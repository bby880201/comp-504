package controller;

import java.awt.EventQueue;
import java.awt.Graphics;

import model.*;
import view.*;
/**
 * The Controller class instantiates our Model and View 
 * in the MVC pattern. This is where the main method is
 * and everything starts here
 * @author bb26,hu3
 */
public class Controller {
	
	private BallModel model;
	private MainView view;
	
	public Controller(){
		view = new MainView(new IView2ModelAdapter(){

			/**
			 * calls the <code>update</update> method of the BallModel
			 * which in turn asks all the balls to paint themselves
			 * @param g: the graphics object that balls will paint themselves on
			 */
			@Override
			public void paint(Graphics g) {
				model.update(g);
			}

			@Override
			public void addBall(String ballType) {
				model.addBall(ballType);
			}

			@Override
			public void clearBall() {
				model.clearFrame();
			}
			
		});
		model = new BallModel(new IModel2ViewAdapter(){
			
			@Override
			public void update(){
				view.update();
			}
		});
	}
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {   // Java specs say that the system must be constructed on the GUI event thread.
			public void run() {
				try {
					Controller controller = new Controller();   // instantiate the system
					controller.start();  // start the system
					} catch (Exception e) {
						e.printStackTrace();
						}
				}
			});
		}

	public void start() {
		model.start();
		view.start();
	}
	
	
}
