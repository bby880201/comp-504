-------------------------------------------------------------------------------
BallWorld Demo Application(HW02) README

-------------------------------------------------------------------------------

QuickStart Guide For Running the BallWorld demo Application 
  by Harsh Upadhyay (Harsh.Upadhyay@rice.edu)

	1.) The BallWorld Demo Application demonstrates an application where a user can create animated balls of different styles
		by providing class names of the desired ball type. There is also a button to clear the screen of all balls
  	2.) This guide assumes the project is opened in eclipse IDE.
  	3.) To run the application, you can right click and select Run As -> Java application 
  		on any of the following in the package explorer pane:
  			- the top folder 'HW02'
  			- the 'HW02/src' folder
  			- the 'HW02/src/controller' package
  			- the class inside controller package 'Controller.java' that contains the main method