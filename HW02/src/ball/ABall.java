package ball;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Point;
import java.util.Observable;
import java.util.Observer;

import util.Randomizer;

/**
 * This is the base superclass for all types of balls
 * @author hu3,bb26
 */
public abstract class ABall implements Observer {

	/* (non-Javadoc)
	 * @see java.util.Observer#update(java.util.Observable, java.lang.Object)
	 */

	final static Randomizer rand = Randomizer.Singleton;
	private Component canvas;
	private Point location;
	private int radius = rand.randomInt(1, 25);
	private Color color = rand.randomColor();

	/**
	 * Creates a randomly located Ball that is always constrained inside the given canvas
	 * @param canvas: ball stays within the bounds of this canvas
	 */
	public ABall(Component canvas) {
		this.canvas = canvas;
		this.location = rand.randomLoc(this.canvas.getBounds());
	}

	/**
	 * set the location of the ball at the given point
	 * @param loc: takes in an object of type Point
	 */
	protected void setLocation(Point loc) {
		this.location = loc;
	}

	/**
	 * return location of the ball's center
	 * @return: Point object
	 */
	public Point getLocation() {
		return this.location;
	}

	/**
	 * Sets the color of the ball
	 * @param col: takes in a Color Object
	 */
	protected void setColor(Color col) {
		this.color = col;
	}

	/**
	 * returns the ball's color
	 * @return: Color object
	 */
	public Color getColor() {
		return this.color;
	}

	/**
	 * sets the ball's radius
	 * @param r: int radius
	 */
	protected void setRadius(int r) {
		this.radius = r;
	}

	/**
	 * returns the ball's radius
	 * @return: int value
	 */
	public int getRadius() {
		return this.radius;
	}

	/**
	 * returns the Canvas the ball is being painted on
	 * @return: Canvas Object
	 */
	public Component getCanvas() {
		return this.canvas;
	}

	/**
	 * makes the ball go into its next state
	 */
	public abstract void setNextState();

	/**
	 * paints the next state of the ball on the canvas using the given Graphics object argument
	 * @param o: The Dispatcher that all balls in the frame watch for update instructions
	 * @param arg: Graphics object that the balls use to paint their next state 
	 */
	public void update(Observable o, Object arg) {
		setNextState();
		Graphics g = ((Graphics) arg);
		g.setColor(color);
		g.fillOval(location.x, location.y, radius * 2, radius * 2);

	}

}
