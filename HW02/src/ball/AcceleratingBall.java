package ball;

import java.awt.Component;
import java.awt.Point;
import java.awt.Rectangle;

/**
 * This is a concrete implementation of the ABall which represents a ball that keeps accelerating
 * @author bb26,hu3
 *
 */
public class AcceleratingBall extends ABall {

	private int vX; //velocity along x-axis
	private int vY; //velocity along y-axis
	private int dX; //direction along x-axis
	private int dY; //direction along y-axis
	private int aX; //acceleration along x-axis
	private int aY; //acceleration along y-axis

	/**
	 * Creates a new accelerating ball
	 * @param canvas: takes in the graphical component that the ball gets painted on
	 */
	public AcceleratingBall(Component canvas) {
		super(canvas);
		this.vX = rand.randomInt(1, 50); 
		this.vY = rand.randomInt(1, 50);
		this.dX = rand.randomInt(0, 1) > 0 ? 1 : -1;
		this.dY = rand.randomInt(0, 1) > 0 ? 1 : -1;
		this.aX = rand.randomInt(-3, 3);
		this.aY = rand.randomInt(-3, 3);
	}

	@Override
	public void setNextState() {
		Point p = this.getLocation();
		Rectangle bounds = this.getCanvas().getBounds();
		int r = this.getRadius();

		int newVX = vX + aX;
		int newVY = vY + aY;

		if (newVX > 100) {
			newVX = 100;
			aX = -aX;
		} else if (newVX < 0) {
			newVX = 0;
			aX = -aX;
		}

		if (newVY > 100) {
			newVY = 100;
			aY = -aY;
		} else if (newVY < 0) {
			newVY = 0;
			aY = -aY;
		}

		int lower = bounds.height - r * 2;
		int right = bounds.width - r * 2;

		int newX = p.x + newVX * dX;
		int newY = p.y + newVY * dY;

		if (newX > right) {
			newX = right - (newX - right);
			dX = -dX;
		}
		if (newX < 0) {
			newX = -newX;
			dX = -dX;
		}
		if (newY > lower) {
			newY = lower - (newY - lower);
			dY = -dY;
		}
		if (newY < 0) {
			newY = -newY;
			dY = -dY;
		}

		this.setLocation(new Point(newX, newY));
		this.vX = newVX;
		this.vY = newVY;
	}

}
