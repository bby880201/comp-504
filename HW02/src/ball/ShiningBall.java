/**
 * 
 */
package ball;

import java.awt.Component;
import java.awt.Point;
import java.awt.Rectangle;

/**
 * This is a concrete implementation of the ABall which represents a ball that rapidly changes color and breathes
 * @author bb26,hu3
 */
public class ShiningBall extends ABall {

	private int vX; //velocity along x-axis
	private int vY; //velocity along y-axis
	private int vR; //rate of change of radius

	/**
	 * Creates a new shining ball
	 * @param canvas: takes in the graphical component that the ball gets painted on
	 */
	public ShiningBall(Component canvas) {
		super(canvas);
		this.vX = rand.randomInt(1, 50);
		this.vY = rand.randomInt(1, 50);
		this.vR = rand.randomInt(1, 10);
	}

	@Override
	public void setNextState() {
		Point p = this.getLocation();
		Rectangle bounds = this.getCanvas().getBounds();

		int r = this.getRadius() + vR;
		if (r >= 100) {
			r = 100 - (r - 100);
			vR = -vR;
		}
		if (r <= 0) {
			r = -r;
			vR = -vR;
		}

		int lower = bounds.height - r * 2;
		int right = bounds.width - r * 2;

		int newX = p.x + vX;
		int newY = p.y + vY;

		if (newX > right) {
			newX = right - (newX - right);
			vX = -vX;
		}
		if (newX < 0) {
			newX = -newX;
			vX = -vX;
		}
		if (newY > lower) {
			newY = lower - (newY - lower);
			vY = -vY;
		}
		if (newY < 0) {
			newY = -newY;
			vY = -vY;
		}

		this.setRadius(r);
		this.setLocation(new Point(newX, newY));
		this.setColor(rand.randomColor());
	}

}
