package model;

import java.awt.Component;
import java.awt.Graphics;
import javax.swing.Timer;

import ball.*;
import util.Dispatcher;

/**
 * Ball Model is the Model class that is responsible for updating the state of all the balls
 * @author hu3,bb26
 */
public class BallModel {
	private int tick = 30; //30 millisecond tick
	private Dispatcher dispatcher = new Dispatcher(); //the observabl that all the balls watch

	private IModel2ViewAdapter m2vAdapter = IModel2ViewAdapter.NULL_OBJECT; //ensures we always have a valid interface

	/**
	 * Creates a new ball Model
	 * @param m2vAdpt: the model to view adapter used to communicate with the view
	 */
	public BallModel(IModel2ViewAdapter m2vAdpt) {
		m2vAdapter = m2vAdpt;
	}

	/**
	 * creating a timer to update the GUI and the balls at regular intervals
	 */
	private Timer timer = new Timer(tick, (e) -> m2vAdapter.update()); //using the Lambda style to create anonymous inner class

	/**
	 * simply tells all the balls to paint their next state
	 * @param g: Graphics object used by the balls to paint themselves
	 */
	public void update(Graphics g) {
		dispatcher.notifyAll(g);
	}

	/**
	 * Adds a new ball to the screen of the specified type
	 * @param ballType: the fully qualified class name of ball to be added
	 * @param canvas: the component that will contain the ball
	 */
	public void addBall(String ballType, Component canvas) {
		try {
			Object[] args = new Object[] { canvas };
			java.lang.reflect.Constructor<?> cs[] = Class.forName(ballType).getConstructors(); // get all the constructors
			java.lang.reflect.Constructor<?> c = null;

			for (int i = 0; i < cs.length; i++) { // find the first constructor with the right number of input parameters
				if (args.length == (cs[i]).getParameterTypes().length) {
					c = cs[i];
					break;
				}
			}
			dispatcher.addObserver((ABall) c.newInstance(args)); // Call the constructor. Will throw a null ptr exception if no constructor with the right number of input parameters was found.
		} catch (Exception ex) {
			System.err.println("Class " + ballType + " failed to load. \nException = \n" + ex);
			ex.printStackTrace(); // print the stack trace to help in debugging.
		}
	}

	/**
	 * clears the canvas of all the balls
	 */
	public void clearFrame() {
		dispatcher.deleteObservers();
	}

	/**
	 * starts up the model
	 */
	public void start() {
		timer.start();
	}
}
