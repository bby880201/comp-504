package model;

/**
 * Interface for the Model to View communication
 * @author hu3,bb26
 */
public interface IModel2ViewAdapter {
	/**
	 * tells View to update itself 
	 */
	public void update();

	/**
	 * this is to create a no-op adapter for Model to View communication
	 */
	public static final IModel2ViewAdapter NULL_OBJECT = new IModel2ViewAdapter() {
		public void update() {
		}
	};
}
