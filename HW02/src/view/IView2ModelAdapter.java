package view;

import java.awt.Component;
import java.awt.Graphics;

/**
 * Interface for the View to Model communication
 * @author hu3,bb26
 */
public interface IView2ModelAdapter {

	/**
	 * tells the model to paint all the balls
	 * @param g: graphics object used by the balls to paint themselves
	 */
	public void paint(Graphics g);

	/**
	 * adds new ball to the canvas by taking in the type of the new ball
	 * @param ballType: fully qualified class name of the ball to be added
	 * @param canvas: canvas on which the new ball will be painted
	 */
	public void addBall(String ballType, Component canvas);

	/**
	 * clears the screen of all the balls
	 */
	public void clearBall();

	/**
	 * this is to create a no-op adapter for Model to View communication
	 */
	public static final IView2ModelAdapter NULL_OBJECT = new IView2ModelAdapter() {

		@Override
		public void paint(Graphics g) {
		}

		@Override
		public void addBall(String ballType, Component canvas) {
		}

		@Override
		public void clearBall() {
		}
	};
}