package view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.BoxLayout;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * This is the main GUI component of the ball world demo
 * @author bb26,hu3
 */

public class MainView extends JFrame {

	/**
	 * contentPane: this is the area where Balls will be displayed
	 */
	private static final long serialVersionUID = 1434216695759527061L;
	private JPanel contentPane;
	private JTextField tfModelInput;
	private JPanel pnlCanvas;
	private IView2ModelAdapter v2mAdapter = IView2ModelAdapter.NULL_OBJECT;

	/**
	 * Launch the application.
	 */
	public void start() {
		this.setVisible(true);
	}

	/**
	 * Create the frame.
	 */
	public MainView() {

		initGUI();
	}
	
	/**
	 * sets up all the properties of the various GUI components and initializes them
	 */
	private void initGUI() {
		setTitle("BallWorld Demo");
		
				setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
				JPanel pnlControl = new JPanel();
		contentPane.add(pnlControl, BorderLayout.NORTH);
		pnlControl.setLayout(new BoxLayout(pnlControl, BoxLayout.X_AXIS));
		
				tfModelInput = new JTextField();
		tfModelInput.setToolTipText("Enter fully qualified name for your ball class. eg. ball.ShiningBall");
		tfModelInput.setText("ball.SimpleBall");
		pnlControl.add(tfModelInput);
		tfModelInput.setColumns(10);
		
				JButton btnMakeball = new JButton("MakeBall");
		btnMakeball.setToolTipText("Adds a new ball to the screen based on the classname entered");
		btnMakeball.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				v2mAdapter.addBall(tfModelInput.getText(), (Component) pnlCanvas);
			}
		});
		pnlControl.add(btnMakeball);
		
				JButton btnClearball = new JButton("ClearBall");
		btnClearball.setToolTipText("Clears away all the balls from the screen");
		btnClearball.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				v2mAdapter.clearBall();
			}
		});
		pnlControl.add(btnClearball);
		
				pnlCanvas = new JPanel() {
					/**
					 * anonymous inner class to facilitate painting of the balls
					 */
					private static final long serialVersionUID = 1L;
		
					public void paintComponent(Graphics g) {
						super.paintComponent(g); // clear the panel and redo the background
						v2mAdapter.paint(g); // call back to the model to paint the sprites
					}
				};
				pnlCanvas.setToolTipText("Balls will be drawing here.");
		contentPane.add(pnlCanvas, BorderLayout.CENTER);
	}

	/**
	 * constructor of the main view that takes in the adapter object for communicating to the model
	 * @param adapter: the adapter it will use to talk to the model
	 */
	public MainView(IView2ModelAdapter adapter) {
		this();
		this.v2mAdapter = adapter;
	}

	/**
	 * repaints the canvas with updated ball states
	 */
	public void update() {
		pnlCanvas.repaint();
	}

}
