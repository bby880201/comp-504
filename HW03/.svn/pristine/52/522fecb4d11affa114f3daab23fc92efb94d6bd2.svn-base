package view;

import java.awt.BorderLayout;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import java.awt.Color;

import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * BallWorldView
 * @author Yi
 *
 */
public class BallWorldView extends JFrame {
	private IModelPaintAdapter _mdlPaintAdpt = IModelPaintAdapter.NULL_OBJECT;
	private IModelCtrlAdpater _mdlCtrlAdpt = IModelCtrlAdpater.NULL_OBJECT;

	/**
	 * 
	 */
	private static final long serialVersionUID = 8104981297940197208L;
	private JPanel contentPane;
	private final JPanel pnlCanvas = new JPanel() {
		private static final long serialVersionUID = -5602025929803047476L;

		/**
		* Overridden paintComponent method to paint a shape in the panel.
		* @param g The Graphics object to paint on.
		**/
		public void paintComponent(Graphics g) {
			super.paintComponent(g); // Do everything normally done first, e.g. clear the screen.
			_mdlPaintAdpt.paintBalls(g); // Call-back to paint all sprite
		}
	};
	private final JPanel pnlControl = new JPanel();
	private final JButton btnMakeBall = new JButton("Make Ball");
	private final JTextField tfClassname = new JTextField();
	private final JButton btnClearBalls = new JButton("Clear All");

	/**
	 * Create the frame.
	 */
	public BallWorldView(IModelCtrlAdpater mdlCtrlAdpt, IModelPaintAdapter mdlPaintAdpt) {
		_mdlCtrlAdpt = mdlCtrlAdpt;
		_mdlPaintAdpt = mdlPaintAdpt;
		tfClassname.setText("ballworld.StraightBall");

		tfClassname.setToolTipText("Type your desired qualified ball type");
		tfClassname.setColumns(10);
		initGUI();
	}

	/**
	* Initialize the GUI */
	private void initGUI() {
		setTitle("Ball World");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 664, 328);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		pnlCanvas.setToolTipText("This the drawing panel that all shapes are drawn upon");
		pnlCanvas.setBackground(new Color(255, 240, 245));

		contentPane.add(pnlCanvas, BorderLayout.CENTER);
		pnlControl.setToolTipText(
				"costumize the title by typing in the textfield and the click on the \"Update Label\" button");
		pnlControl.setBackground(new Color(222, 184, 135));

		contentPane.add(pnlControl, BorderLayout.NORTH);
		btnMakeBall.setToolTipText("Click to create a ball according to the text field");
		btnMakeBall.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				_mdlCtrlAdpt.loadBall(pnlCanvas, tfClassname.getText()); //////
			}
		});

		pnlControl.add(tfClassname);

		pnlControl.add(btnMakeBall);
		btnClearBalls.setToolTipText("Sweep away all the balls on the screen");
		btnClearBalls.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				_mdlCtrlAdpt.clearBalls(); //////
			}
		});

		pnlControl.add(btnClearBalls);
	}

	/**
	* Launch the timer and GUI */
	public void start() {
		setVisible(true);
	}

	/**
	 * Updates the view by repainting the canvas;
	 * This process should be called by IViewAdapter
	 */
	public void update() {
		pnlCanvas.repaint();
	}
}
