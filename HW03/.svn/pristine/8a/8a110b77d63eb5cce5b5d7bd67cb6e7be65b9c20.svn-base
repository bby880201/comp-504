package view;

import java.awt.Component;

/**
 * Control adapter of BallWorldView to BallWorldModel
 * @author Yi
 *
 */
public interface IModelCtrlAdpater {
	/**
	 * The method to tell model to clear all created balls
	 */
	public void clearBalls();

	/**
	 * The method to tell model to create a new ball by the
	 * specifically given class name
	 * @param canvas The reference of the paint panel
	 * @param className The qualified ball class name
	 */
	public void loadBall(Component canvas, String className);

	/**
	 * No-op "null" adapter
	 */
	public static final IModelCtrlAdpater NULL_OBJECT = new IModelCtrlAdpater() {
		public void clearBalls() {

		}

		public void loadBall(Component canvas, String className) {
		}
	};
}
