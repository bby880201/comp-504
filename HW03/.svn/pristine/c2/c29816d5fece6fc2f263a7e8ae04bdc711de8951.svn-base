package ballworld;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Point;
import java.util.Observable;
import java.util.Observer;

public abstract class ABall implements Observer {
	/**
	 * Constructor of ABall class.
	 * @param canvas The panel component where balls are painted
	 * @param loc The location of the upper left corner of a ball
	 * @param dd The diameter of a ball 
	 * @param vel The velocity of a ball
	 * @param cc The color of a ball
	 */
	public ABall(Component canvas, Point loc, int dd, Point vel, Color cc) {
		this.canvas = canvas;
		this.loc = loc;
		this.dia = dd;
		this.vel = vel;
		this.c = cc;
	}

	private Component canvas;

	/**
	 * getter of painting panel component
	 * @return return a Component object
	 */
	public Component getCan() {
		return this.canvas;
	}

	private Point loc;

	/**
	 * Set the location of the ball to value
	 * @param value The position of the upper left corner of the ball
	 */
	public void setLoc(Point value) {
		this.loc = value;
	}

	/**
	 * Return the location of the ball
	 * @return Return a value of Point class which contains the upper left
	 * corner's position of the ball
	 */
	public Point getLoc() {
		return this.loc;
	}

	private int dia;

	/**
	 * Set the diameter of the ball to value
	 * @param value Pass an integer value as the ball's diameter
	 */
	public void setDia(int value) {
		this.dia = value;
	}

	/**
	 * Get the diameter of the ball
	 * @return Return the diameter of the ball
	 */
	public int getDia() {
		return this.dia;
	}

	private Point vel;

	/**
	 * Set the moving velocity of the ball to value
	 * @param value Pass a velocity vector of Point class as the ball's velocity
	 */
	public void setVel(Point value) {
		this.vel = value;
	}

	/**
	 * Get the velocity of the ball
	 * @return Return a velocity vector by a Point object
	 */
	public Point getVel() {
		return this.vel;
	}

	private Color c;

	/**
	 * Configure the color of the ball to be value 
	 * @param value Pass a color value by a Color object
	 */
	public void setC(Color value) {
		this.c = value;
	}

	/**
	 * Get the color of the ball
	 * @return Return the color of the ball by a Color object
	 */
	public Color getC() {
		return this.c;
	}

	/**
	 * Update the state of the ball, such as color, velocity, etc.
	 */
	public abstract void updateState();

	/**
	 * Move the ball to the next location based on the current
	 * location and velocity
	 */
	public void move() {
		getLoc().setLocation(getLoc().x + getVel().x, getLoc().y + getVel().y);
	}

	/**
	 * Correct the location of the ball, if the ball moves beyond the walls. 
	 */
	public void bounce() {
		int width = getCan().getWidth();
		int height = getCan().getHeight();
		int d = getDia();
		int vx = getVel().x;
		int vy = getVel().y;
		int x = getLoc().x;
		int y = getLoc().y;

		if (x < 0) { // the ball is beyond the left
			x *= -1;
			vx *= -1;
		} else if (x > width - d) { // the ball is beyond the right
			x = (width - d) * 2 - x;
			vx *= -1;
		}

		if (y < 0) { // the ball is beyond the top
			y *= -1;
			vy *= -1;
		} else if (y > height - d) { // the ball is beyond the bottom
			y = (height - d) * 2 - y;
			vy *= -1;
		}

		// Update the position of the ball
		getLoc().setLocation(x, y);
		getVel().setLocation(vx, vy);
	}

	/**
	 * Paint the ball on the display screen represented by g
	 * @param g Graphics component representing the display screen; the
	 * parameter g should come from the paintComponent()
	 */
	public void paint(Graphics g) {
		g.setColor(getC());
		g.fillOval(getLoc().x, getLoc().y, getDia(), getDia());
	}

	/**
	 * refresh the state of the ball
	 */
	public void update(Observable o, Object g) {
		updateState();
		move();
		bounce();
		paint((Graphics) g);
	}
}
