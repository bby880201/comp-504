/**
 * 
 */
package ball;

import java.awt.Point;

/**
 * @author bb26
 * Accelerating strategy 
 * ball will continue to accelerate until it reach  a maximum velocity,
 * then the velocity will change to opposite direction
 */
public class Accelerating implements IUpdateStrategy {
	private Point a;

	public Accelerating() {
		a = new Point(rand.randomInt(-5, 5), rand.randomInt(-5, 5));
	}

	@Override
	public void updateState(Ball ball) {
		Point v = ball.getVelocity();
		v.x += a.x;
		v.y += a.y;

		ball.setVelocity(v);

		if (Math.abs(v.x) > 150) {
			v.x *= -1;
		}

		if (Math.abs(v.y) > 150) {
			v.y *= -1;
		}
	}

}
