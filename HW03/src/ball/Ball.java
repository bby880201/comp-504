package ball;

import java.awt.Color;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.Observable;
import java.util.Observer;

import util.Randomizer;

/**
 * This is the base superclass for all types of balls
 * @author hu3,bb26
 */
public class Ball implements Observer {

	/* (non-Javadoc)
	 * @see java.util.Observer#update(java.util.Observable, java.lang.Object)
	 */

	final static Randomizer rand = Randomizer.Singleton;
	private Container canvas;
	private Point location;
	private int radius;
	private Color color;
	private Point velocity;
	private IUpdateStrategy stg = IUpdateStrategy.NULL_OBJECT;

	/**
	 * Creates a randomly located Ball that is always constrained inside the given canvas
	 * @param canvas: ball stays within the bounds of this canvas
	 * @param stg : a concrete strategy for ball
	 */
	public Ball(Container canvas, IUpdateStrategy stg) {
		this.canvas = canvas;
		this.location = rand.randomLoc(this.canvas.getBounds());
		this.radius = rand.randomInt(1, 25);
		this.color = rand.randomColor();
		this.velocity = new Point(rand.randomInt(1, 25), rand.randomInt(1, 25));
		this.stg = stg;
	}

	/**
	 * set the location of the ball at the given point
	 * @param loc: takes in an object of type Point
	 */
	public void setLocation(Point loc) {
		this.location = loc;
	}

	/**
	 * return location of the ball's center
	 * @return: Point object
	 */
	public Point getLocation() {
		return this.location;
	}

	/**
	 * Sets the color of the ball
	 * @param col: takes in a Color Object
	 */
	public void setColor(Color col) {
		this.color = col;
	}

	/**
	 * returns the ball's color
	 * @return: Color object
	 */
	public Color getColor() {
		return this.color;
	}

	/**
	 * sets the ball's radius
	 * @param r: int radius
	 */
	public void setRadius(int r) {
		this.radius = r;
	}

	/**
	 * returns the ball's radius
	 * @return: int value
	 */
	public int getRadius() {
		return this.radius;
	}

	/**
	 * set the ball's velocity
	 * @param v : the velocity
	 */
	public void setVelocity(Point v) {
		this.velocity = v;
	}
	/**
	 * returns the ball's velocity
	 * @return the ball's velocity
	 */
	public Point getVelocity() {
		return this.velocity;
	}

	/**
	 * implement the interface to use a specific strategy
	 * @return a concrete strategy
	 */
	public IUpdateStrategy getStrategy() {
		return this.stg;
	}

	/**
	 * set the ball's strategy
	 * @param stg : ball's strategy
	 */
	public void setStrategy(IUpdateStrategy stg) {
		this.stg = stg;
	}

	/**
	 * Move the ball to the next location based on the current
	 * location and velocity
	 */
	private void move() {
		location.x += velocity.x;
		location.y += velocity.y;
	}

	/**
	 * Correct the location of the ball, if the ball moves beyond the walls. 
	 */	
	private void bounce() {
		Point p = this.location;
		Rectangle bounds = this.canvas.getBounds();
		int r = this.radius;
		Point v = this.velocity;

		int lower = bounds.height - r * 2;
		int right = bounds.width - r * 2;

		if (p.x > right) {
			p.x = right - (p.x - right);
			v.x *= -1;
		}
		if (p.x < 0) {
			p.x = -p.x;
			v.x *= -1;
		}
		if (p.y > lower) {
			p.y = lower - (p.y - lower);
			v.y *= -1;
		}
		if (p.y < 0) {
			p.y = -p.y;
			v.y *= -1;
		}
	}

	/**
	 * paints the next state of the ball on the canvas using the given Graphics object argument
	 * @param o: The Dispatcher that all balls in the frame watch for update instructions
	 * @param arg: Graphics object that the balls use to paint their next state 
	 */
	public void update(Observable o, Object arg) {
		stg.updateState(this);
		this.move();
		this.bounce();
		this.paint(((Graphics) arg));
	}

	/**
	 * Paint the ball on the display screen represented by g
	 * @param g Graphics component representing the display screen; the
	 * parameter g should come from the paintComponent()
	 */	
	public void paint(Graphics g) {
		g.setColor(color);
		g.fillOval(location.x, location.y, radius * 2, radius * 2);
	}

}
