/**
 * 
 */
package ball;

import util.SineMaker;

/**
 * @author bb26
 * Breathing Ball
 * The ball will change its size according to a sine function
 */
public class Breathing implements IUpdateStrategy {

	private double delta;
	private SineMaker sine;

	public Breathing() {
		this.delta = rand.randomDouble(-0.3, 0.3);
		this.sine = new SineMaker(-Math.PI, Math.PI, delta);
	}

	@Override
	public void updateState(Ball ball) {
		int r = ball.getRadius();
		ball.setRadius(r += sine.getIntVal());
	}

}
