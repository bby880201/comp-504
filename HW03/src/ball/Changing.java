/**
 * 
 */
package ball;

/**
 * @author bb26
 * Changing Ball
 * combine the curve and shining strategy
 */
public class Changing implements IUpdateStrategy {
	private int counter;
	private IUpdateStrategy stg1;
	private IUpdateStrategy stg2;

	public Changing() {
		counter = 150;
		stg1 = new Curve();
		stg2 = new Shining();
	}

	@Override
	public void updateState(Ball ball) {
		counter--;
		if (counter == 0) {
			IUpdateStrategy temp = stg1;
			stg1 = stg2;
			stg2 = temp;
			counter = 150;
		}
		stg1.updateState(ball);
	}

}
