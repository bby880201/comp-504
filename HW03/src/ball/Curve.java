/**
 * 
 */
package ball;

import java.awt.Point;

/**
 * @author bb26
 * Curve Ball
 * the ball will go like a spiral
 */
public class Curve implements IUpdateStrategy {

	private double theta;
	private double sin;
	private double cos;

	public Curve() {
		this.theta = rand.randomDouble(-0.5, 0.5);
		this.sin = Math.sin(theta);
		this.cos = Math.cos(theta);
	}

	@Override
	public void updateState(Ball ball) {
		Point v = ball.getVelocity();
		v.setLocation(v.x * cos - v.y * sin, v.y * cos + v.x * sin);
		ball.setVelocity(v);
	}

	public void updateTheta() {
		double newTheta = theta += 0.15;
		theta = newTheta > 2 * Math.PI ? newTheta - 2 * Math.PI : newTheta;
	}
}
