/**
 * 
 */
package ball;

import java.awt.Point;

/**
 * @author bb26
 * Drunken ball
 * The ball will change its color, velocity and size randomly
 */
public class Drunken implements IUpdateStrategy {

	public Drunken() {

	}

	@Override
	public void updateState(Ball ball) {
		int i = rand.randomInt(1, 10);
		Point v = ball.getVelocity();
		int r = ball.getRadius();
		switch (i) {
		case 1:
			ball.setColor(rand.randomColor());
			break;
		case 2:
			v.x += rand.randomInt(-10, 10);
			ball.setVelocity(v);
			break;
		case 3:
			v.y += rand.randomInt(-10, 10);
			ball.setVelocity(v);
			break;
		case 4:
			r += rand.randomInt(-3, 3);
			ball.setRadius(r);
			break;
		default:
			break;
		}
	}

}
