/**
 * 
 */
package ball;

/**
 * A interface for generating ball strategy, make() function
 * must be implemented.
 * @author bb26
 */
public interface IStrategyFac {

	public IUpdateStrategy make();

	public final static IStrategyFac NULL_OBJECT = new IStrategyFac() {

		@Override
		public IUpdateStrategy make() {
			return null;
		}

	};
	public final static IStrategyFac ERROR_OBJECT = new IStrategyFac() {

		@Override
		public IUpdateStrategy make() {
			return null;
		}

	};

}
