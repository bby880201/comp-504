/**
 * 
 */
package ball;

import util.Randomizer;

/**
 * @author bb26
 * Interface that helps to dynamically change ball's strategy
 */
public interface IUpdateStrategy {
	final static Randomizer rand = Randomizer.Singleton;

	public void updateState(Ball ball);

	public final static IUpdateStrategy NULL_OBJECT = new IUpdateStrategy() {

		@Override
		public void updateState(Ball ball) {
		}

	};

	public final static IUpdateStrategy ERROR_OBJECT = new IUpdateStrategy() {

		@Override
		public void updateState(Ball ball) {
		}

	};

}
