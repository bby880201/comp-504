/**
 * 
 */
package ball;

/**
 * @author bb26
 * Shining Ball
 * The ball will change its color randomly
 */
public class Shining implements IUpdateStrategy {

	/**
	 * 
	 */
	public Shining() {
	}

	@Override
	public void updateState(Ball ball) {
		ball.setColor(rand.randomColor());
	}

}
