/**
 *
 */

package ball;

/**
 * @author bb26
 * Let the ball switch its strategy
 */

public class Switcher implements IUpdateStrategy {
	protected static IUpdateStrategy currentStg = new Straight();

	public Switcher() {

	}

	@Override
	public void updateState(Ball ball) {
		currentStg.updateState(ball);
	}

	public void setCurrentStg(IUpdateStrategy stg) {
		currentStg = stg;
	}

}
