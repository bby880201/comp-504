package controller;

import java.awt.Container;
import java.awt.EventQueue;
import java.awt.Graphics;

import ball.IStrategyFac;
import ball.Switcher;
import model.*;
import view.*;

/**
 * The Controller class instantiates our Model and View 
 * in the MVC pattern. This is where the main method is
 * and everything starts here
 * @author bb26
 */
public class Controller {

	private BallModel model;
	private MainView<IStrategyFac> view;

	public Controller() {
		view = new MainView<IStrategyFac>(new IView2ModelAdapter() {

			/**
			 * calls the <code>update</update> method of the BallModel
			 * which in turn asks all the balls to paint themselves
			 * @param g: the graphics object that balls will paint themselves on
			 */
			@Override
			public void paint(Graphics g) {
				model.update(g);
			}
		}, new IView2ModelControlAdapter<IStrategyFac>() {

			@Override
			public IStrategyFac addStrategy(String classname) {
				return model.makeStgFac(classname);
			}

			@Override
			public void makeBall(Container canvas, IStrategyFac selectedItem) {
				if (null != selectedItem) {
					model.loadBall(canvas, selectedItem.make());
				}
			}

			@Override
			public IStrategyFac combineStrategies(IStrategyFac selectedItem1,
					IStrategyFac selectedItem2) {
				return model.combineStg(selectedItem1, selectedItem2);
			}

			@Override
			public void clearBall() {
				model.clearFrame();
			}

			@Override
			public void makeSwitchBall(Container canvas) {
				model.loadBall(canvas, new Switcher());
			}

			@Override
			public void setSwithcerStg(IStrategyFac selectedItem) {
				model.makeSwitch(selectedItem);
			}
		});
		model = new BallModel(new IModel2ViewAdapter() {

			@Override
			public void update() {
				view.update();
			}
		});
	}

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() { // Java specs say that the system must be constructed on the GUI event thread.
					public void run() {
						try {
							Controller controller = new Controller(); // instantiate the system
							controller.start(); // start the system
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
	}

	/**
	 * starts up the GUI and the back end model
	 */
	public void start() {
		model.start();
		view.start();
	}

}
