package model;

import java.awt.Container;
import java.awt.Graphics;

import javax.swing.Timer;

import ball.Ball;
import ball.IStrategyFac;
import ball.IUpdateStrategy;
import ball.Switcher;
import util.Dispatcher;

/**
 * Ball Model is the Model class that is responsible for updating the state of all the balls
 * @author bb26
 */
public class BallModel {
	private Switcher switcher = new Switcher();
	private int tick = 40; //40 millisecond tick
	private Dispatcher dispatcher = new Dispatcher(); //the observabl that all the balls watch

	private IModel2ViewAdapter m2vAdapter = IModel2ViewAdapter.NULL_OBJECT; //ensures we always have a valid interface

	/**
	 * Creates a new ball Model
	 * @param m2vAdpt: the model to view adapter used to communicate with the view
	 */
	public BallModel(IModel2ViewAdapter m2vAdpt) {
		m2vAdapter = m2vAdpt;
	}

	/**
	 * creating a timer to update the GUI and the balls at regular intervals
	 */
	private Timer timer = new Timer(tick, (e) -> m2vAdapter.update()); //using the Lambda style to create anonymous inner class

	/**
	 * simply tells all the balls to paint their next state
	 * @param g: Graphics object used by the balls to paint themselves
	 */
	public void update(Graphics g) {
		dispatcher.notifyAll(g);
	}

	/**
	 * clears the canvas of all the balls
	 */
	public void clearFrame() {
		dispatcher.deleteObservers();
	}

	/**
	 * starts up the model
	 */
	public void start() {
		timer.start();
	}

	public IStrategyFac makeStgFac(final String ballType) {
		return new IStrategyFac() {
			@Override
			public IUpdateStrategy make() {
				return loadStrategy(ballType);
			}

			@Override
			public String toString() {
				return ballType;
			}
		};
	}

	private IUpdateStrategy loadStrategy(String ballType) {
		try {
			Object[] args = new Object[] {};
			java.lang.reflect.Constructor<?> cs[] = Class.forName(
					"ball." + ballType).getConstructors(); // get all the constructors
			java.lang.reflect.Constructor<?> c = null;

			for (int i = 0; i < cs.length; i++) { // find the first constructor with the right number of input parameters
				if (args.length == (cs[i]).getParameterTypes().length) {
					c = cs[i];
					break;
				}
			}
			return (IUpdateStrategy) c.newInstance(args);
		} catch (Exception ex) {
			System.err.println("Class " + ballType
					+ " failed to load. \nException = \n" + ex);
			ex.printStackTrace(); // print the stack trace to help in debugging.
		}
		return IUpdateStrategy.ERROR_OBJECT;
	}

	/**
	 * Adds a new ball to the screen of the specified type
	 * @param canvas: the container that will contain the ball
	 * @param stg: a strategy of ball to be added
	 */

	public void loadBall(Container canvas, IUpdateStrategy stg) {
		if (stg == IUpdateStrategy.ERROR_OBJECT) {
			System.out.println("Wrong Strategy!");
		} else {
			dispatcher.addObserver(new Ball(canvas, stg));
		}
	}

	public IStrategyFac combineStg(final IStrategyFac selectedItem1,
			final IStrategyFac selectedItem2) {
		if (null == selectedItem1 || null == selectedItem2)
			return null;
		return new IStrategyFac() {

			@Override
			public IUpdateStrategy make() {
				IUpdateStrategy stg1 = selectedItem1.make();
				IUpdateStrategy stg2 = selectedItem2.make();
				return new IUpdateStrategy() {

					@Override
					public void updateState(Ball ball) {
						stg1.updateState(ball);
						stg2.updateState(ball);
					}

				};
			}

			@Override
			public String toString() {
				return selectedItem1.toString() + " - "
						+ selectedItem2.toString();
			}

		};
	}

	public void makeSwitch(IStrategyFac selectedItem) {
		switcher.setCurrentStg(selectedItem.make());
	}
}
