package view;

import java.awt.Graphics;

/**
 * Interface for the View to Model communication
 * @author bb26
 */
public interface IView2ModelAdapter {

	/**
	 * tells the model to paint all the balls
	 * @param g: graphics object used by the balls to paint themselves
	 */
	public void paint(Graphics g);

	/**
	 * this is to create a no-op adapter for Model to View communication
	 */
	public static final IView2ModelAdapter NULL_OBJECT = new IView2ModelAdapter() {

		@Override
		public void paint(Graphics g) {
		}

	};
}