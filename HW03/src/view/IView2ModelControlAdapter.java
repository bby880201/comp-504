package view;

import java.awt.Container;

/**
 * Interface for the View to Model communication, focusing on strategy control
 * @author bb26
 */

public interface IView2ModelControlAdapter<T> {

	public T addStrategy(String classname);

	public T combineStrategies(T selectedItem1, T selectedItem2);

	public void clearBall();

	public void makeBall(Container canvas, T stgFac);

	public void makeSwitchBall(Container canvas);

	public void setSwithcerStg(T selectedItem);

}
