package view;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

import javax.swing.JComboBox;

import java.awt.Container;
import java.awt.Graphics;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

/**
 * This is the main GUI component of the ball world demo
 * @author bb26
 */

public class MainView<TDlItem> extends JFrame {

	/**
	 * contentPane: this is the area where Balls will be displayed
	 */
	private static final long serialVersionUID = 1434216695759527061L;
	private JPanel contentPane;
	private JPanel ctlPnl;
	private JButton addStgBtn;
	private JTextField stgTf;
	private JButton addBallBtn;
	private JComboBox<TDlItem> upperDl;
	private JComboBox<TDlItem> lowerDl;
	private JButton cbBtn;
	private JButton mkSwcBtn;
	private JButton switchBtn;
	private JButton clrBtn;
	private JPanel cvsPnl;
	private IView2ModelControlAdapter<TDlItem> v2mCtlAdapter;
	private IView2ModelAdapter v2mUpdAdapter = IView2ModelAdapter.NULL_OBJECT;

	/**
	 * Launch the application.
	 */
	public void start() {
		this.setVisible(true);
	}

	/**
	 * Create the frame.
	 */
	public MainView() {

		initGUI();
	}

	/**
	 * sets up all the properties of the various GUI components and initializes them
	 */
	private void initGUI() {
		setTitle("BallWorld Demo");

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		ctlPnl = new JPanel();
		ctlPnl.setBackground(Color.ORANGE);
		contentPane.add(ctlPnl, BorderLayout.NORTH);
		GridBagLayout gbl_ctlPnl = new GridBagLayout();
		gbl_ctlPnl.columnWidths = new int[] { 100, 160, 170, 170, 100, 100, 0 };
		gbl_ctlPnl.rowHeights = new int[] { 25, 25, 0, 0, 0 };
		gbl_ctlPnl.columnWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
				Double.MIN_VALUE };
		gbl_ctlPnl.rowWeights = new double[] { 1.0, 1.0, 1.0, 1.0,
				Double.MIN_VALUE };
		ctlPnl.setLayout(gbl_ctlPnl);

		addBallBtn = new JButton("Make A Ball");
		addBallBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				v2mCtlAdapter.makeBall((Container) cvsPnl,
						upperDl.getItemAt(upperDl.getSelectedIndex()));
			}
		});
		addBallBtn
				.setToolTipText("Make a ball with strategy selected in upper droplist.");
		GridBagConstraints gbc_addBallBtn = new GridBagConstraints();
		gbc_addBallBtn.fill = GridBagConstraints.BOTH;
		gbc_addBallBtn.insets = new Insets(0, 0, 5, 5);
		gbc_addBallBtn.gridx = 2;
		gbc_addBallBtn.gridy = 0;
		ctlPnl.add(addBallBtn, gbc_addBallBtn);

		stgTf = new JTextField();
		stgTf.setToolTipText("Enter a ball strategy here.");
		GridBagConstraints gbc_stgTf = new GridBagConstraints();
		gbc_stgTf.insets = new Insets(0, 0, 5, 5);
		gbc_stgTf.fill = GridBagConstraints.BOTH;
		gbc_stgTf.gridx = 1;
		gbc_stgTf.gridy = 1;
		ctlPnl.add(stgTf, gbc_stgTf);
		stgTf.setColumns(10);

		upperDl = new JComboBox<TDlItem>();
		upperDl.setToolTipText("Select a strategy here for either making a ball or generating a combined strategy.\n");
		GridBagConstraints gbc_upperDl = new GridBagConstraints();
		gbc_upperDl.insets = new Insets(0, 0, 5, 5);
		gbc_upperDl.fill = GridBagConstraints.BOTH;
		gbc_upperDl.gridx = 2;
		gbc_upperDl.gridy = 1;
		ctlPnl.add(upperDl, gbc_upperDl);

		mkSwcBtn = new JButton("Make A Switcher");
		mkSwcBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				v2mCtlAdapter.makeSwitchBall((Container) cvsPnl);
			}
		});
		mkSwcBtn.setToolTipText("Make a ball that can switch strategy.");
		GridBagConstraints gbc_mkSwcBtn = new GridBagConstraints();
		gbc_mkSwcBtn.fill = GridBagConstraints.BOTH;
		gbc_mkSwcBtn.insets = new Insets(0, 0, 5, 5);
		gbc_mkSwcBtn.gridx = 3;
		gbc_mkSwcBtn.gridy = 1;
		ctlPnl.add(mkSwcBtn, gbc_mkSwcBtn);

		clrBtn = new JButton("Clear All");
		clrBtn.setToolTipText("Clear all balls.");
		clrBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				v2mCtlAdapter.clearBall();
			}
		});
		GridBagConstraints gbc_clrBtn = new GridBagConstraints();
		gbc_clrBtn.fill = GridBagConstraints.HORIZONTAL;
		gbc_clrBtn.gridheight = 2;
		gbc_clrBtn.insets = new Insets(0, 0, 5, 5);
		gbc_clrBtn.gridx = 4;
		gbc_clrBtn.gridy = 1;
		ctlPnl.add(clrBtn, gbc_clrBtn);

		addStgBtn = new JButton("Add Strategy");
		addStgBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TDlItem stg = v2mCtlAdapter.addStrategy(stgTf.getText());
				upperDl.addItem(stg);
				lowerDl.addItem(stg);
			}
		});
		addStgBtn.setToolTipText("Add strategy to both droplists.");
		GridBagConstraints gbc_addStgBtn = new GridBagConstraints();
		gbc_addStgBtn.fill = GridBagConstraints.BOTH;
		gbc_addStgBtn.insets = new Insets(0, 0, 5, 5);
		gbc_addStgBtn.gridx = 1;
		gbc_addStgBtn.gridy = 2;
		ctlPnl.add(addStgBtn, gbc_addStgBtn);

		lowerDl = new JComboBox<TDlItem>();
		lowerDl.setToolTipText("Select another strategy for generating combined strategy.");
		GridBagConstraints gbc_lowerDl = new GridBagConstraints();
		gbc_lowerDl.insets = new Insets(0, 0, 5, 5);
		gbc_lowerDl.fill = GridBagConstraints.BOTH;
		gbc_lowerDl.gridx = 2;
		gbc_lowerDl.gridy = 2;
		ctlPnl.add(lowerDl, gbc_lowerDl);

		switchBtn = new JButton("Switch");
		switchBtn
				.setToolTipText("Switch the strategy on all switcher balls to the strategy selected in upper droplist.");
		switchBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				v2mCtlAdapter.setSwithcerStg(upperDl.getItemAt(upperDl
						.getSelectedIndex()));
			}
		});
		GridBagConstraints gbc_switchBtn = new GridBagConstraints();
		gbc_switchBtn.fill = GridBagConstraints.BOTH;
		gbc_switchBtn.insets = new Insets(0, 0, 5, 5);
		gbc_switchBtn.gridx = 3;
		gbc_switchBtn.gridy = 2;
		ctlPnl.add(switchBtn, gbc_switchBtn);

		cbBtn = new JButton("Combine!");
		cbBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TDlItem cbStg = v2mCtlAdapter.combineStrategies(
						upperDl.getItemAt(upperDl.getSelectedIndex()),
						lowerDl.getItemAt(lowerDl.getSelectedIndex()));
				upperDl.addItem(cbStg);
				lowerDl.addItem(cbStg);
			}
		});
		cbBtn.setToolTipText("Combine strategy selected from both droplist into a new strategy and add it to the droplists.");
		GridBagConstraints gbc_cbBtn = new GridBagConstraints();
		gbc_cbBtn.insets = new Insets(0, 0, 0, 5);
		gbc_cbBtn.fill = GridBagConstraints.BOTH;
		gbc_cbBtn.gridx = 2;
		gbc_cbBtn.gridy = 3;
		ctlPnl.add(cbBtn, gbc_cbBtn);

		cvsPnl = new JPanel() {
			/**
			 * 
			 */
			private static final long serialVersionUID = -5622828270408106935L;

			/**
			 * anonymous inner class to facilitate painting of the balls
			 */

			public void paintComponent(Graphics g) {
				super.paintComponent(g); // clear the panel and redo the background
				v2mUpdAdapter.paint(g); // call back to the model to paint the sprites
			}
		};
		cvsPnl.setToolTipText("Balls will be drawn here.");
		contentPane.add(cvsPnl, BorderLayout.CENTER);
	}

	/**
	 * constructor of the main view that takes in the adapter object for communicating to the model
	 * @param adapter: the adapter it will use to talk to the model
	 * @param ctlAdapter : the control adapter
	 */
	public MainView(IView2ModelAdapter adapter,
			IView2ModelControlAdapter<TDlItem> ctlAdapter) {
		this();
		this.v2mUpdAdapter = adapter;
		this.v2mCtlAdapter = ctlAdapter;
	}

	/**
	 * repaints the canvas with updated ball states
	 */
	public void update() {
		cvsPnl.repaint();
	}
}
