package paint.shapefactory;

import java.awt.Point;
import java.awt.Polygon;
import java.awt.Shape;
import java.awt.geom.AffineTransform;

/**
 * a superclass that defines a factory that instantiates 
 * a abstract polygon shape painting model
 */
public class PolygonFactory implements IShapeFactory {
	
	protected AffineTransform at = new AffineTransform();
	private Polygon poly = new Polygon();
	private double scaleFactor = 1.0;
	
	/**
	 * constructor, populate/draw a concrete polygon
	 * @param at
	 * @param scaleFactor
	 * @param points
	 */
	public PolygonFactory(AffineTransform at, double scaleFactor, Point... points){
		this.at = at;
		this.scaleFactor = scaleFactor;
		for ( Point pt : points) {
			poly.addPoint(pt.x, pt.y);
		}
	}

	@Override
	public Shape makeShape(double x, double y, double xScale, double yScale) {
		at.setToTranslation(x, y);
		at.scale(xScale*scaleFactor, yScale*scaleFactor);
		return at.createTransformedShape(poly);
	}

}
