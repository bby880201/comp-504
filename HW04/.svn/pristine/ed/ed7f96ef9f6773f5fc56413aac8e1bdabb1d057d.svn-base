package model;

import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;

import ball.*;
import strategy.IStrategyFac;
import strategy.IUpdateStrategy;
import strategy.MultiStrategy;
import util.*;

public class BallWorldModel {

	private IModel2ViewUpdateAdapter _model2ViewUpdateAdapter = IModel2ViewUpdateAdapter.NULL_OBJECT;
	private Dispatcher myDispatcher = new Dispatcher();

	/**
     * A factory for a beeping error strategy that beeps the speaker every 25 updates.
     * Either use the _errorStrategyFac variable directly if you need a factory that makes an error strategy,
     * or call _errorStrategyFac.make() to create an instance of a beeping error strategy.
     */
    private IStrategyFac _errorStrategyFac = new IStrategyFac(){
    	 @Override
         /**
          * Make the beeping error strategy
          * @return  An instance of a beeping error strategy
          */
         public IUpdateStrategy make() {
             return new IUpdateStrategy() {
                 private int count = 0; // update counter
                 @Override
                 /**
                  * Beep the speaker every 25 updates
                  */
                 public void updateState(Ball context) {
                     if(25 < count++){
                         java.awt.Toolkit.getDefaultToolkit().beep(); 
                         count = 0;
                     }
                 }
             };
         }
    };

	private int _timeSlice = 50; // update every 50 milliseconds
	private Timer _timer = new Timer(_timeSlice, new ActionListener() {
		/**
		 * The timer "ticks" by calling this method every _timeslice milliseconds
		 */
		public void actionPerformed(ActionEvent e) {
			_model2ViewUpdateAdapter.update();
		}
	});

	public BallWorldModel(IModel2ViewUpdateAdapter viewUpdateAdapter) {
		_model2ViewUpdateAdapter = viewUpdateAdapter;
	}

	public void loadBall(IUpdateStrategy strat){
		myDispatcher.addObserver(new Ball(strat));
	}

	//exception throw
	public void deleteBalls() {
		myDispatcher.deleteObservers();
	}

	//dynamic ball loading
	public IUpdateStrategy loadStrategy(String fullClassName){
		try {
			Object[] args = new Object[] {}; // YOUR CONSTRUCTOR MAY BE DIFFERENT!!   The supplied values here may be fields, input parameters, random values, etc.
			java.lang.reflect.Constructor<?> cs[] = Class.forName(fullClassName).getConstructors(); // get all the constructors
			java.lang.reflect.Constructor<?> c = null;
			for (int i = 0; i < cs.length; i++) { // find the first constructor with the right number of input parameters
				if (args.length == (cs[i]).getParameterTypes().length) {
					c = cs[i];
					break;
				}
			}
			return (IUpdateStrategy) c.newInstance(args); // Call the constructor.   Will throw a null ptr exception if no constructor with the right number of input parameters was found.
		} catch (Exception ex) {
			System.err.println("Strategy " + fullClassName + " failed to load. \nException = \n" + ex);
//			ex.printStackTrace(); // print the stack trace to help in debugging.
			return (IUpdateStrategy) _errorStrategyFac; // Is this really a useful thing to return here?  Is there something better that could be returned? 
		}
	}

	private String fixName(String classname){
		return "strategy." + classname;
	}

	public void start() {
		_timer.start();
	}

	public void update(Graphics g) {
		myDispatcher.x = _model2ViewUpdateAdapter.getXBoarder();
		myDispatcher.y = _model2ViewUpdateAdapter.getYBoarder();
		myDispatcher.notifyAll(g);
	}

	/**
	 * Returns an IStrategyFac that can instantiate the strategy specified by
	 * classname. Returns a factory for a beeping error strategy if classname is null. 
	 * The toString() of the returned factory is the classname.
	 * 
	 * @param classname  Shortened name of desired strategy
	 * @return A factory to make that strategy
	 */
	public IStrategyFac makeStrategyFac(final String classname) {
		if (null == classname) return _errorStrategyFac;
		return new IStrategyFac() {
			/**
			 * Instantiate a strategy corresponding to the given class name.
			 * @return An IUpdateStrategy instance
			 */
			public IUpdateStrategy make() {
				return loadStrategy(fixName(classname));
			}

			/**
			 * Return the given class name string
			 */
			public String toString() {
				return classname;
			}
		};
	}

	/**
	 * Returns an IStrategyFac that can instantiate a MultiStrategy with the two
	 * strategies made by the two given IStrategyFac objects. Returns null if
	 * either supplied factory is null. The toString() of the returned factory
	 * is the toString()'s of the two given factories, concatenated with "-". 
	 * If either factory is null, then a factory for a beeping error strategy is returned.
	 * 
	 * @param stratFac1 An IStrategyFac for a strategy
	 * @param stratFac2 An IStrategyFac for a strategy
	 * @return An IStrategyFac for the composition of the two strategies
	 */
	public IStrategyFac combineStrategyFacs(final IStrategyFac stratFac1, final IStrategyFac stratFac2) {
		if (null == stratFac1 || null == stratFac2) return _errorStrategyFac;
		return new IStrategyFac() {
			/**
			 * Instantiate a new MultiStrategy with the strategies from the given strategy factories
			 * @return A MultiStrategy instance
			 */
			public IUpdateStrategy make() {
				return new MultiStrategy(stratFac1.make(), stratFac2.make());
			}

			/**
			 * Return a string that is the toString()'s of the given strategy factories concatenated with a "-"
			 */
			public String toString() {
				return stratFac1.toString() + "-" + stratFac2.toString();
			}
		};
	}

}
