package ball;

import java.awt.Color;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Point;
import java.util.*;

import model.IBallCmd;
import paint.strategy.IPaintStrategy;
import strategy.IUpdateStrategy;
import util.Dispatcher;
import util.Randomizer;
import util.SineMaker;

/**
 * the prototype of the object getting painted and moving on the canvas.
 */
public class Ball implements Observer {

	protected Point location;
	protected Point velocity;
	protected int radius;
	protected Color color;
	protected SineMaker sine;
	protected Container centerPanel;
	protected IUpdateStrategy strategy;
	protected IPaintStrategy paintStrategy;

	/**
	 * Initiate the ball at the given location, velocity, radius, color and sine value.
	 */
	public Ball(IUpdateStrategy strat, IPaintStrategy paintStrat,
			Container canvas) {
		this.strategy = strat;
		this.setPaintStrategy(paintStrat);
		this.setLocation(new Point(Randomizer.Singleton.randomInt(30, 600),
				Randomizer.Singleton.randomInt(50, 300)));
		this.setVelocity(new Point(Randomizer.Singleton.randomInt(-10, 10),
				Randomizer.Singleton.randomInt(-10, 10)));
		this.setRadius(Randomizer.Singleton.randomInt(5, 20));
		this.setColor(Randomizer.Singleton.randomColor());
		this.setSine(new SineMaker(5, 30, 0.2));
		this.setCanvas(canvas);
		this.paintStrategy.init(this);
	}

	public void setCanvas(Container canvas) {
		this.centerPanel = canvas;
	}

	public Container getCanvas() {
		return this.centerPanel;
	}

	/**
	 * getters and setters
	 */
	public Point getLocation() {
		return this.location;
	}

	public void setLocation(Point point) {
		this.location = point;
	}

	public Point getVelocity() {
		return this.velocity;
	}

	public void setVelocity(Point point) {
		this.velocity = point;
	}

	public int getRadius() {
		return this.radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

	public Color getColor() {
		return this.color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public void setSine(SineMaker sine) {
		this.sine = sine;
	}

	public IPaintStrategy getPaintStrategy() {
		return this.paintStrategy;
	}

	/**
	 * This function set ball's paint strategy
	 * @param paintStrat: paint strategy to be updated
	 */
	private void setPaintStrategy(IPaintStrategy paintStrat) {
		this.paintStrategy = paintStrat;
		this.paintStrategy.init(this);
	}

	/**
	 * Updating the strategy of this ball class
	 */
	public void updateStrategy() {
		this.strategy.updateState(this);
	}

	/**
	 * This function does the command sent from the observable object
	 * @param o observable object
	 * @param cmd the command sent from the observable object
	 */
	public void update(Observable o, Object cmd) {
		((IBallCmd) cmd).apply(this, (util.Dispatcher) o);
	}

	/**
	 * Updating the location, also handle the bounce situation.
	 * @param o
	 */
	public void updatePosition(Observable o) {
		Dispatcher dispatcher = Dispatcher.class.cast(o);
		this.move();
		this.bounce(dispatcher);
	}

	/**
	 * Move the ball in straight way
	 */
	public void move() {
		this.location.x += this.velocity.x;
		this.location.y += this.velocity.y;
	}

	/**
	 * Bouncing Balls based on location
	 * @param dispatcher
	 */
	public void bounce(Dispatcher dispatcher) {
		if (this.location.x <= this.radius) {
			this.location.x = this.radius;
			this.velocity.x = -this.velocity.x;
		}
		if (this.location.y <= this.radius) {
			this.location.y = this.radius;
			this.velocity.y = -this.velocity.y;
		}
		if (this.location.x >= dispatcher.x - this.radius) {
			this.location.x = dispatcher.x - this.radius;
			this.velocity.x = -this.velocity.x;
		}
		if (this.location.y >= dispatcher.y - this.radius) {
			this.location.y = dispatcher.y - this.radius;
			this.velocity.y = -this.velocity.y;
		}
	}

	/**
	 * paint this ball on the canvas using its painting strategy.
	 * @param g
	 */
	public void paint(Object g) {
		this.paintStrategy.paint((Graphics) g, this);
	}

}
