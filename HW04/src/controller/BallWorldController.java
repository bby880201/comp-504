package controller;

import java.awt.Container;
import java.awt.EventQueue;
import java.awt.Graphics;

import paint.strategy.IPaintStrategy;
import paint.strategy.IPaintStrategyFac;
import model.*;
import strategy.IStrategyFac;
import strategy.IUpdateStrategy;
import strategy.SwitcherStrategy;
import view.*;

/**
 * this is the major controller in this MVC project.
 */
public class BallWorldController {

	private BallWorldModel model;
	private BallWorldGUI<IStrategyFac, IPaintStrategyFac> view;
	private SwitcherStrategy _switcherStrategy;

	/**
	 * constructor
	 * instantiate a model and a view
	 * implementing adapters
	 */
	public BallWorldController() {

		_switcherStrategy = new SwitcherStrategy();

		model = new BallWorldModel(new IModel2ViewUpdateAdapter() {
			@Override
			public void update() {
				view.update();
			}

			@Override
			public int getXBoarder() {
				return view.getXX();
			}

			@Override
			public int getYBoarder() {
				return view.getYY();
			}
		});

		view = new BallWorldGUI<IStrategyFac, IPaintStrategyFac>(
				new IModelCtrlAdapter<IStrategyFac, IPaintStrategyFac>() {

					@Override
					public IStrategyFac addStrategy(String classname) {
						return model.makeStrategyFac(classname);
					}

					@Override
					public IPaintStrategyFac addPaintStrategy(String classname) {
						return model.makePaintingStrategyFac(classname);
					}

					@Override
					public IStrategyFac combineStrategies(
							IStrategyFac selectedItem1,
							IStrategyFac selectedItem2) {
						return model.combineStrategyFacs(selectedItem1,
								selectedItem2);
					}

					@Override
					public void setSwitcherStrategy(IStrategyFac selectedItem) {
						if (null != selectedItem)
							_switcherStrategy.setStrategy(selectedItem.make());
					}

					@Override
					public void makeStrategySwitchableBall(
							IPaintStrategyFac selectedPaintingStrategy,
							Container canvas) {
						model.loadBall(_switcherStrategy,
								selectedPaintingStrategy.make(), canvas);
					}

					@Override
					public void makeBall(IStrategyFac selectedStrategy,
							IPaintStrategyFac selectedPaintingStrategy,
							Container canvas) {
						if (null != selectedStrategy) {
							System.out.println("Ballmaker Called");
							model.loadBall(selectedStrategy.make(),
									selectedPaintingStrategy.make(), canvas);

						}
					}
				},

				new IView2ModelUpdateAdapter() {
					@Override
					public void paint(Graphics g) {
						model.update(g);
					}

					@Override
					public void loadBall(IUpdateStrategy strat,
							IPaintStrategy paintStrat, Container canvas) {
						model.loadBall(strat, paintStrat, canvas);
					}

					@Override
					public void clearBalls() {
						model.deleteBalls();
					}
				});
	}

	/**
	 * controller starting function
	 * separate construction from operation
	 */
	public void start() {
		model.start();
		view.start();
	}

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BallWorldController controller = new BallWorldController();
					controller.start();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}
