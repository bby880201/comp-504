package model;

import java.awt.Container;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;

import paint.strategy.IPaintStrategy;
import paint.strategy.IPaintStrategyFac;
import ball.*;
import strategy.IStrategyFac;
import strategy.IUpdateStrategy;
import strategy.MultiStrategy;
import util.*;

/**
 * the main Model of this MVC painting project
 */
public class BallWorldModel {

	private IModel2ViewUpdateAdapter _model2ViewUpdateAdapter = IModel2ViewUpdateAdapter.NULL_OBJECT;
	private Dispatcher myDispatcher = new Dispatcher();

	/**
	 * A factory for a beeping error strategy that beeps the speaker every 25 updates.
	 * Either use the _errorStrategyFac variable directly if you need a factory that makes an error strategy,
	 * or call _errorStrategyFac.make() to create an instance of a beeping error strategy.
	 */
	private IStrategyFac _errorStrategyFac = new IStrategyFac() {
		@Override
		/**
		 * Make the beeping error strategy
		 * @return  An instance of a beeping error strategy
		 */
		public IUpdateStrategy make() {
			return new IUpdateStrategy() {
				private int count = 0; // update counter

				@Override
				/**
				 * Beep the speaker every 25 updates
				 */
				public void updateState(Ball context) {
					if (25 < count++) {
						java.awt.Toolkit.getDefaultToolkit().beep();
						count = 0;
					}
				}
			};
		}
	};

	/**
	 * A factory similar to _errorStrategyFac for error painting strategy
	 */
	private IPaintStrategyFac _errorPaintStrategyFac = new IPaintStrategyFac() {
		@Override
		public IPaintStrategy make() {
			return new IPaintStrategy() {
				private int count = 0;

				@Override
				public void init(Ball host) {
				}

				public void paint(Graphics g, Ball host) {
					if (25 < count++) {
						java.awt.Toolkit.getDefaultToolkit().beep();
						count = 0;
					}
				}
			};
		}
	};

	private int _timeSlice = 50; // update every 50 milliseconds
	private Timer _timer = new Timer(_timeSlice, new ActionListener() {
		/**
		 * The timer "ticks" by calling this method every _timeslice milliseconds
		 */
		public void actionPerformed(ActionEvent e) {
			_model2ViewUpdateAdapter.update();
		}
	});

	/**
	 * model constructor
	 * @param viewUpdateAdapter
	 */
	public BallWorldModel(IModel2ViewUpdateAdapter viewUpdateAdapter) {
		_model2ViewUpdateAdapter = viewUpdateAdapter;
	}

	/**
	 * adding the new created ball in Dispatcher observer
	 */
	public void loadBall(IUpdateStrategy strat, IPaintStrategy paintStrat,
			Container canvas) {
		myDispatcher.addObserver(new Ball(strat, paintStrat, canvas));
	}

	/**
	 * delete all Balls in the observer
	 */
	public void deleteBalls() {
		myDispatcher.deleteObservers();
	}

	/**
	 * dynamic ball loading based on the class name given
	 * @param fullClassName
	 * @return
	 */
	public IUpdateStrategy loadStrategy(String fullClassName) {
		try {
			Object[] args = new Object[] {}; // YOUR CONSTRUCTOR MAY BE DIFFERENT!!   The supplied values here may be fields, input parameters, random values, etc.
			java.lang.reflect.Constructor<?> cs[] = Class
					.forName(fullClassName).getConstructors(); // get all the constructors
			java.lang.reflect.Constructor<?> c = null;
			for (int i = 0; i < cs.length; i++) { // find the first constructor with the right number of input parameters
				if (args.length == (cs[i]).getParameterTypes().length) {
					c = cs[i];
					break;
				}
			}
			return (IUpdateStrategy) c.newInstance(args); // Call the constructor.   Will throw a null ptr exception if no constructor with the right number of input parameters was found.
		} catch (Exception ex) {
			System.err.println("Strategy " + fullClassName
					+ " failed to load. \nException = \n" + ex);
			//ex.printStackTrace(); // print the stack trace to help in debugging.
			return (IUpdateStrategy) _errorStrategyFac; // Is this really a useful thing to return here?  Is there something better that could be returned? 
		}
	}

	/**
	 * dynamically load the paint strategy
	 * @param className name of the paint strategy to load
	 * @return the new IPaintStrategy object
	 */
	private IPaintStrategy loadPaintStrategy(String fullClassName) {
		try {
			java.lang.reflect.Constructor<?> cs = Class.forName(fullClassName)
					.getConstructor(new Class<?>[0]);
			return (IPaintStrategy) cs.newInstance();
		} catch (Exception ex) {
			System.err.println("Paint strategy " + fullClassName
					+ " failed to load. \nException = \n" + ex);
			//ex.printStackTrace(); // print the stack trace to help in debugging.
			return (IPaintStrategy) _errorPaintStrategyFac;
		}
	}

	/**
	 * create a full classname based on path and postfix
	 * @param folder
	 * @param classname
	 * @param postfix
	 * @return
	 */
	private String fixName(String folder, String classname, String postfix) {
		return folder + classname + postfix;
	}

	/**
	 * start the timer
	 */
	public void start() {
		_timer.start();
	}

	/**
	 * update the location and paint
	 * @param g
	 */
	public void update(final Graphics g) {
		myDispatcher.x = _model2ViewUpdateAdapter.getXBoarder();
		myDispatcher.y = _model2ViewUpdateAdapter.getYBoarder();
		myDispatcher.notifyAll(new IBallCmd() {
			@Override
			/**
			 * Do stuff with the ball
			 */
			public void apply(Ball host, Dispatcher disp) {
				host.paint(g);
				host.updateStrategy();
				host.updatePosition(myDispatcher);
			}
		});
	}

	/**
	 * Returns an IStrategyFac that can instantiate the strategy specified by
	 * The toString() of the returned factory is the classname.
	 * @param classname  Shortened name of desired strategy
	 * @return A factory to make that strategy
	 */
	public IStrategyFac makeStrategyFac(final String classname) {
		if (null == classname)
			return _errorStrategyFac;
		return new IStrategyFac() {
			/**
			 * Instantiate a strategy corresponding to the given class name.
			 * @return An IUpdateStrategy instance
			 */
			public IUpdateStrategy make() {
				return loadStrategy(fixName("strategy.", classname, ""));
			}

			/**
			 * Return the given class name string
			 */
			public String toString() {
				return classname;
			}
		};
	}

	/**
	 * Returns an IPaintStrategy that can instantiate a painting strategy specified byclassname.
	 * The toString() of the returned factory is the classname.
	 * @param classname  Shortened name of desired strategy
	 * @return A factory to make that strategy
	 */
	public IPaintStrategyFac makePaintingStrategyFac(final String classname) {
		if (null == classname)
			return _errorPaintStrategyFac;
		return new IPaintStrategyFac() {

			@Override
			public IPaintStrategy make() {
				return loadPaintStrategy(fixName(
						"paint.strategy.concreteStrategy.", classname,
						"PaintStrategy"));
			}

			public String toString() {
				return classname;
			}
		};
	}

	/**
	 * Returns an IStrategyFac that can instantiate a MultiStrategy with the two
	 * strategies made by the two given IStrategyFac objects. Returns null if
	 * either supplied factory is null. The toString() of the returned factory
	 * is the toString()'s of the two given factories, concatenated with "-". 
	 * If either factory is null, then a factory for a beeping error strategy is returned.
	 * 
	 * @param stratFac1 An IStrategyFac for a strategy
	 * @param stratFac2 An IStrategyFac for a strategy
	 * @return An IStrategyFac for the composition of the two strategies
	 */
	public IStrategyFac combineStrategyFacs(final IStrategyFac stratFac1,
			final IStrategyFac stratFac2) {
		if (null == stratFac1 || null == stratFac2)
			return _errorStrategyFac;
		return new IStrategyFac() {
			/**
			 * Instantiate a new MultiStrategy with the strategies from the given strategy factories
			 * @return A MultiStrategy instance
			 */
			public IUpdateStrategy make() {
				return new MultiStrategy(stratFac1.make(), stratFac2.make());
			}

			/**
			 * Return a string that is the toString()'s of the given strategy factories concatenated with a "-"
			 */
			public String toString() {
				return stratFac1.toString() + "-" + stratFac2.toString();
			}
		};
	}

}
