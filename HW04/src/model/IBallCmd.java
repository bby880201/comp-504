package model;

import ball.Ball;
import util.Dispatcher;

/**
 * Interface that represents commands sent through the dispatcher to process the balls
 */
public interface IBallCmd {
	/**
	 * The method run by the ball's update method which is called when the ball is updated by the dispatcher.
	 * @param context The ball that is calling this method.   The context under which the command is to be run.
	 * @param disp The Dispatcher that sent the command out.
	 */
	void apply(Ball host, Dispatcher disp);
}
