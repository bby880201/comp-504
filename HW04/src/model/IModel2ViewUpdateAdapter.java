package model;

/**
 * the adapter object that translate the communication between model and view
 */
public interface IModel2ViewUpdateAdapter {

	/**
	 * the update function of this model adapter
	 */
	public void update();

	/**
	 * The method to get the current width of painting field of view.
	 */
	public int getXBoarder();

	/**
	 * The method to get the current height of painting field of view.
	 */
	public int getYBoarder();

	/**
	 * an Null object of this interface
	 */
	public static final IModel2ViewUpdateAdapter NULL_OBJECT = new IModel2ViewUpdateAdapter() {

		// repaint the center panel
		public void update() {
		}

		public int getXBoarder() {
			return 0;
		}

		public int getYBoarder() {
			return 0;
		}

	};
}
