package model;

import java.awt.Container;

/**
 * the major model controller adapter,
 * helps the communication between model and controller.
 * @param <TDropListItem>
 * @param <TPaintDropListItem>
 */
public interface IModelCtrlAdapter<TDropListItem, TPaintDropListItem> {

	/**
	 * Take the given short strategy name and return a corresponding something to put onto both drop lists.
	 * @param classname  The shortened class name of the desired strategy
	 * @return Something to put onto both the drop lists.
	 */
	public TDropListItem addStrategy(String classname);

	/**
	 * Take the given painting strategy name and return a corresponding object to the drop lists.
	 * @param classname  The shortened class name of the desired painting strategy
	 * @return the strategy object to the paint drop lists.
	 */
	public TPaintDropListItem addPaintStrategy(String classname);

	/**
	 * Make a ball with the selected short strategy name.
	 * @param tDropListItem  A shorten class name for the desired strategy
	 */
	public void makeBall(TDropListItem tDropListItem,
			TPaintDropListItem tPaintDropListItem, Container canvas);

	/**
	 * Return a new object to put on both lists, given two items from the lists.
	 * @param selectedItem1  An object from one drop list
	 * @param selectedItem2 An object from the other drop list
	 * @return An object to put back on both lists.
	 */
	public TDropListItem combineStrategies(TDropListItem selectedItem1,
			TDropListItem selectedItem2);

	/**
	 * Set the strategy for all SwitcherStrategy balls.
	 */
	public void setSwitcherStrategy(TDropListItem selectedItem);

	/**
	 * Make a ball with the Singleton switcherStrategy as its strategy.
	 */
	void makeStrategySwitchableBall(
			TPaintDropListItem selectedPaintingStrategy, Container cavas);
}
