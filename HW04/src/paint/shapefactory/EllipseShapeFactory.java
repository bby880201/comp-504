package paint.shapefactory;

import java.awt.Shape;
import java.awt.geom.Ellipse2D;

/**
 * a class that defines a factory that instantiates 
 * a Ellipse shape painting model
 */
public class EllipseShapeFactory implements IShapeFactory {
	public static EllipseShapeFactory Singleton = new EllipseShapeFactory();

	/**
	 * constructor, no param needed
	 */
	private EllipseShapeFactory() {
	}

	@Override
	public Shape makeShape(double x, double y, double xScale, double yScale) {
		Shape ellipse = new Ellipse2D.Double(x, y, xScale, yScale);
		return ellipse;
	}

}
