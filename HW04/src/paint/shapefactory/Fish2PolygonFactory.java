package paint.shapefactory;

import java.awt.Point;
import java.awt.geom.AffineTransform;

/**
 * a class that defines a factory that instantiates 
 * a concrete polygon shape painting model Fish2
 */
public class Fish2PolygonFactory extends PolygonFactory {

	private static Point[] pointsArray = { new Point(-4, -3),
			new Point(-3, -2), new Point(-2, -1), new Point(3, -3),
			new Point(7, 0), new Point(3, 3), new Point(-2, 1),
			new Point(-3, 2), new Point(-4, 3) };

	/**
	 * constructor, constructing the superclass based on parameters
	 * @param at: AffineTransform
	 * @param scaleFactor
	 * @param points: polygon points
	 */
	public Fish2PolygonFactory() {
		super(new AffineTransform(), 0.5, pointsArray);
	}

}
