package paint.shapefactory;

import java.awt.Shape;

/**
 * An interface that defines a factory that instantiates a specific IShapeFactory
 * Abstract factory that creates a Shape for use as prototype shapes in IPaintStrategies. 
 * The location of the center of the shape and the x and y scales can be specified.
 */
public interface IShapeFactory {
	/**
	 * make a shape
	 * @param x-coordinate of the center of the shape
	 * @param y-coordinate of the center of the shape
	 * @param xScale the x-dimension of the shape, usually the x-radius.
	 * @param yScale the y-dimension of the shape, usually the y-radius.
	 * @return
	 */
	public Shape makeShape(double x, double y, double xScale, double yScale);
}
