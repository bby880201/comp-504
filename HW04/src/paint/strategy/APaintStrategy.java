package paint.strategy;

import java.awt.Graphics;
import java.awt.geom.AffineTransform;

import ball.Ball;

/**
 * This class provides the basic affine transform services that its 
 * subclasses will use to resize, translate and rotate their prototype
 * images into their proper current locations and orientations on the screen.   
 * This class is designed to be the root class for all strategies that use
 * affine transforms to create their visual representations.
 */
public abstract class APaintStrategy implements IPaintStrategy {

	protected AffineTransform at;

	@Override
	public void init(Ball host) {

	}

	/**
	 * constructors
	 */
	public APaintStrategy() {
		this.at = new AffineTransform();
	}

	public APaintStrategy(AffineTransform at) {
		this.at = at;
	}

	/**
	 * getter and setter
	 * @return
	 */
	public AffineTransform getAt() {
		return this.at;
	}

	protected void setAt(AffineTransform at) {
		this.at = at;
	}

	/**
	 * implement painting with affineTransform
	 */
	@Override
	public void paint(Graphics g, Ball host) {
		double scale = host.getRadius();
		at.setToTranslation(host.getLocation().x, host.getLocation().y);
		at.scale(scale, scale);
		at.rotate(Math.atan2(host.getVelocity().y, host.getVelocity().x));
		g.setColor(host.getColor());
		paintCfg(g, host);
		paintXfrm(g, host, at);
	}

	/**
	 * The paintCfg method is set to be a concrete no-op that the subclasses may
	 * or may not override.   This method allows the subclass to inject additional 
	 * processing into the paint method process before the final transformations are performed.
	 * @param g
	 * @param host
	 */
	protected void paintCfg(Graphics g, Ball host) {

	}

	/**
	 * A secondary paint operation that is the last step of the above paint method, 
	 * which does not calculate its own affine transform, but instead, uses a supplied affine transform.  
	 * Notice that the translation, rotation and scaling have already been added to 
	 * the affine transform before it gets to paintXfrm.  This allows the same affine 
	 * transform to be shared amongst paint strategies, reducing the number of times that 
	 * it has to be calculated
	 * @param g
	 * @param host
	 * @param at
	 */
	public abstract void paintXfrm(Graphics g, Ball host, AffineTransform at);
}
