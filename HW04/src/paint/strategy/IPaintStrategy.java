package paint.strategy;

import java.awt.Graphics;

import ball.Ball;

/**
 * The top-level superclass (interface, actually) that fundamentally defines what
 * a painting strategy can do. This is the view of the painting strategy from
 * the perspective of the ball.
 */
public interface IPaintStrategy {
	/**
	 * used to initialize the strategy and host ball. This method must be run whenever 
	 * the ball gets a new strategy, such as in a setPaintStrategy method or even in 
	 * the constructor of the ball.   
	 */
	public void init(Ball host);

	/**
	 * The actual paint operation that is called during the (re)painting process.  
	 * Paints the host onto the given Graphics context. 
	 * @param g
	 * @param host
	 */
	public void paint(Graphics g, Ball host);

	/**
	 * the null object of this strategy.
	 */
	public static final IPaintStrategy NULL_OBJECT = new IPaintStrategy() {

		@Override
		public void paint(Graphics g, Ball host) {
		}

		@Override
		public void init(Ball host) {
		}
	};
}
