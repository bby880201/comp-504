package paint.strategy;

/**
 * An interface that defines a factory that instantiates 
 * a specific IPaintStrategy
 */
public interface IPaintStrategyFac {
	/**
	 * Instantiate the specific IUpdateStrategy for which this factory is defined.
	 * @return An IUpdateStrategy instance.
	 */
	public IPaintStrategy make();
}