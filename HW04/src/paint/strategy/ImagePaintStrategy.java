/**
 * 
 */
package paint.strategy;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Toolkit;
import java.awt.geom.AffineTransform;
import java.awt.image.ImageObserver;

import ball.Ball;

/**
 *a super class for image painting strategy,
 *every image should extend this class
 */
public class ImagePaintStrategy extends APaintStrategy {

	private ImageObserver imageObs;
	private Image image;
	private double scaleFactor = 1.0;
	private double fillFactor = 1.0;
	protected AffineTransform localAT = new AffineTransform();
	protected AffineTransform tempAT = new AffineTransform();

	/**
	 * constructor, cannot be called by GUI directly.
	 * @param String fileName file name plus its path relative to the location of the class that is doing the loading
	 * @param double fillFactor fill factor
	 */
	public ImagePaintStrategy(String fileName, double fillFactor) {
		try {
			image = Toolkit.getDefaultToolkit().getImage(
					this.getClass().getResource(fileName));
		} catch (Exception e) {
			System.err.println("ImagePaintStrategy: Error reading file: "
					+ fileName + "\n" + e);
		}

	}

	/**
	 * Initializes the internal ImageObserver reference from the host Ball Also calculates the net scale factor for the image.
	 * @param Ball host host object
	 */
	public void init(Ball host) {
		imageObs = host.getCanvas();
		MediaTracker mt = new MediaTracker(host.getCanvas());
		mt.addImage(image, 1);
		try {
			mt.waitForAll();
		} catch (Exception e) {
			System.out
					.println("ImagePaintStrategy.init(): Error waiting for image.  Exception = "
							+ e);
		}
		scaleFactor = 2.0 / (fillFactor
				* (image.getWidth(imageObs) + image.getHeight(imageObs)) / 2.0);
	}

	/**
	 * Draws the image on the given Graphics context using the given affine transform in combination with the local affine transform.	
	 * @param Ball host host object
	 * @param Graphics g Graphics object painting strategy is painting on
	 * @param AffineTransform at object doing affine transform
	 */
	@Override
	public void paintXfrm(Graphics g, Ball host, AffineTransform at) {
		localAT.setToScale(scaleFactor, scaleFactor);
		localAT.translate(-image.getWidth(imageObs) / 2.0,
				-image.getHeight(imageObs) / 2.0);
		localAT.preConcatenate(at);
		((Graphics2D) g).drawImage(image, localAT, imageObs);
	}
}
