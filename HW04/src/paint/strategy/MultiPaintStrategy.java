package paint.strategy;

import java.awt.Graphics;
import java.awt.geom.AffineTransform;

import ball.Ball;

/**
 * This Composite Design Pattern paint strategy composes affine transform-based paint strategies,
 * i.e. APaintStrategies.It will not compose general IPaintStrategy's. 
 * IPaintStrategies can be composed just like was done with the IUpdateStrategies before.  
 */
public class MultiPaintStrategy extends APaintStrategy {
	/**
	 * maintain all needed aPaintStrategy instances
	 */
	protected APaintStrategy[] aPaintStrategies;

	/**
	 * constructors
	 * @param strategies
	 */
	public MultiPaintStrategy(APaintStrategy... strategies) {
		super(new AffineTransform());
		this.aPaintStrategies = strategies;
	}

	public MultiPaintStrategy(AffineTransform at, APaintStrategy... strategies) {
		super(at);
		this.aPaintStrategies = strategies;
	}

	@Override
	public void init(Ball host) {
		for (APaintStrategy aPaintEntry : this.aPaintStrategies) {
			aPaintEntry.init(host);
		}
	}

	@Override
	public void paintXfrm(Graphics g, Ball host, AffineTransform at) {
		for (APaintStrategy aPaintEntry : this.aPaintStrategies) {
			aPaintEntry.paintXfrm(g, host, at);
		}
	}
}
