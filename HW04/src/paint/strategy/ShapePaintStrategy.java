package paint.strategy;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.AffineTransform;

import ball.Ball;

/**
 * This class is very similar the SquarePaintStrategy, 
 * but abstracts out the shape aspects into an java.awt.Shape 
 * object transformed by an affineTransform.It does this by leveraging 
 * off of the affineTransform services provided by APaintStrategy,
 */
public class ShapePaintStrategy extends APaintStrategy {
	private Shape shape;

	/**
	 * constructors
	 */
	public ShapePaintStrategy() {

	}

	public ShapePaintStrategy(Shape shape) {
		this.shape = shape;
	}

	public ShapePaintStrategy(AffineTransform at, Shape shape) {
		super(at);
		this.shape = shape;
	}

	@Override
	public void paintXfrm(Graphics g, Ball host, AffineTransform at) {
		//		double scale = host.getRadius();
		//		at.setToTranslation(host.getLocation().x, host.getLocation().y);
		//		at.scale(scale, scale);
		//		at.rotate(host.getVelocity().x, host.getVelocity().y);
		//		g.setColor(host.getColor());   
		Shape newShape = at.createTransformedShape(shape);
		((Graphics2D) g).fill(newShape);
	}

}
