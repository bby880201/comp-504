package paint.strategy;

import java.awt.Graphics;

import ball.Ball;

/**
 *a super class for upright image painting strategy, which always keeps image be upright 
 *every image should extend this class
 */

public class UprightImagePaintStrategy extends ImagePaintStrategy {

	/**
	 * constructors, cannot be called by GUI, need to be extended
	 * @param String fileName file name plus its path relative to the location of the class that is doing the loading
	 * @param double fillFactor fill factor
	 */
	public UprightImagePaintStrategy(String fileName, double fillFactor) {
		super(fileName, fillFactor);
	}

	/**
	 * configure painting strategy before painting
	 * @param Graphics g 
	 * @param Ball host
	 */
	@Override
	public void paintCfg(Graphics g, Ball host) {
		super.paintCfg(g, host);
		if (Math.abs(Math.atan2(host.getVelocity().y, host.getVelocity().x)) > Math.PI / 2.0) {
			at.scale(1.0, -1.0);
		}
	}

}
