package paint.strategy.concreteStrategy;

import java.awt.Graphics;

import ball.Ball;
import paint.strategy.IPaintStrategy;

/**
 * concrete simple ball painting strategy
 */
public class BallPaintStrategy implements IPaintStrategy {
	/**
	 * constructor
	 */
	public BallPaintStrategy() {
	}

	@Override
	public void init(Ball host) {
	}

	@Override
	public void paint(Graphics g, Ball host) {
		int radius = host.getRadius();
		g.setColor(host.getColor());
		g.fillOval(host.getLocation().x - radius,
				host.getLocation().y - radius, 2 * radius, 2 * radius);
	}

}
