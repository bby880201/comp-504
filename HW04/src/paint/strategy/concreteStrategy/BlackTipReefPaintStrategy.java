package paint.strategy.concreteStrategy;

import paint.strategy.UprightImagePaintStrategy;

/** 
 * Paint strategy to paint black tip reef, which always keep upright
 */

public class BlackTipReefPaintStrategy extends UprightImagePaintStrategy {

	public BlackTipReefPaintStrategy() {
		super("images/BlackTipReef.gif", 0.8);
	}

}
