package paint.strategy.concreteStrategy;

import java.awt.geom.AffineTransform;

import paint.shapefactory.Fish1PolygonFactory;
import paint.strategy.ShapePaintStrategy;

/**
 * concrete fish1 painting strategy
 */
public class Fish1PaintStrategy extends ShapePaintStrategy {
	/**
	 * constructors
	 */
	public Fish1PaintStrategy() {
		super(new Fish1PolygonFactory().makeShape(0, 0, 1, 1));
	}

	public Fish1PaintStrategy(AffineTransform at) {
		super(at, new Fish1PolygonFactory().makeShape(0, 0, 1, 1));
	}

}
