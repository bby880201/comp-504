package paint.strategy.concreteStrategy;

import java.awt.geom.AffineTransform;

import paint.shapefactory.Fish2PolygonFactory;
import paint.strategy.ShapePaintStrategy;

/**
 * concrete fish2 painting strategy
 */
public class Fish2PaintStrategy extends ShapePaintStrategy {
	/**
	 * constructors
	 */
	public Fish2PaintStrategy() {
		super(new Fish2PolygonFactory().makeShape(0, 0, 1, 1));
	}

	public Fish2PaintStrategy(AffineTransform at) {
		super(at, new Fish2PolygonFactory().makeShape(0, 0, 1, 1));
	}

}
