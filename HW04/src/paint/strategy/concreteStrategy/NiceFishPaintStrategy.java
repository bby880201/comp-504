package paint.strategy.concreteStrategy;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.geom.AffineTransform;

import ball.Ball;
import paint.strategy.APaintStrategy;
import paint.strategy.MultiPaintStrategy;

/**
 * concrete nice fish painting strategy, composite strategy which should contain a polygon fish and a circle eye
 */

public class NiceFishPaintStrategy extends MultiPaintStrategy {

	public NiceFishPaintStrategy() {
		super((APaintStrategy) new Fish1PaintStrategy(),
				(APaintStrategy) new EllipsePaintStrategy(
						new AffineTransform(), 1, -1, 0.9, 0.9) {
					@Override
					public void paintCfg(Graphics g, Ball host) {
						super.paintCfg(g, host);
						g.setColor(getContrastColor(g.getColor()));
						if (Math.abs(Math.atan2(host.getVelocity().y,
								host.getVelocity().x)) > Math.PI / 2.0) {
							at.scale(1.0, -1.0);
						}
					}

					private Color getContrastColor(final Color col) {
						return new Color(255 - col.getRed(),
								255 - col.getGreen(), 255 - col.getBlue());
					}

				});
	}

	@Override
	public void paint(Graphics g, Ball host) {
		for (APaintStrategy aPaintEntry : this.aPaintStrategies) {
			aPaintEntry.paint(g, host);
		}
	}

}
