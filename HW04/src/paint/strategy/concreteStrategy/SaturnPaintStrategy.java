package paint.strategy.concreteStrategy;

import paint.strategy.UprightImagePaintStrategy;

/** 
 * Paint strategy to paint an Saturn that keep upright
 */

public class SaturnPaintStrategy extends UprightImagePaintStrategy {

	public SaturnPaintStrategy() {
		super("images/Saturn.png", 0.8);
	}

}
