package paint.strategy.concreteStrategy;

import java.awt.Graphics;

import paint.strategy.IPaintStrategy;
import ball.Ball;

/**
 * concrete simple square painting strategy
 */
public class SquarePaintStrategy implements IPaintStrategy {
	/** 
	 * No parameter constructor for the class
	 */
	public SquarePaintStrategy() {
	}

	/**
	 * By default, do nothing for initialization.
	 */
	@Override
	public void init(Ball host) {
	}

	/**
	 * Paints a square on the given graphics context using the color and radius 
	 * provided by the host. 
	 * param g The Graphics context that will be paint on
	 * param host The host Ball that the required information will be pulled from.
	 */
	@Override
	public void paint(Graphics g, Ball host) {
		int halfSide = host.getRadius();
		g.setColor(host.getColor());
		g.fillRect(host.getLocation().x - halfSide, host.getLocation().y
				- halfSide, 2 * halfSide, 2 * halfSide);
	}

}
