package strategy;

import util.SineMaker;
import ball.Ball;

/**
 * breathing behavior strategy
 * changes the size of the object when moving
 */
public class Breathing implements IUpdateStrategy {

	private SineMaker sine = new SineMaker(5, 30, 0.2);

	@Override
	public void updateState(Ball ball) {
		ball.setRadius(this.sine.getIntVal());
	}

}
