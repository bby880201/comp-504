package strategy;

import util.Randomizer;
import ball.Ball;

/**
 * color changing behavior strategy
 * changes the color of the object when moving
 */
public class Color implements IUpdateStrategy {

	@Override
	public void updateState(Ball ball) {
		ball.setColor(Randomizer.Singleton.randomColor());
	}

}
