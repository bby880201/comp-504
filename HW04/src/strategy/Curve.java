package strategy;

import java.awt.Point;

import ball.Ball;

/**
 * curving behavior strategy
 * circling movement
 */
public class Curve implements IUpdateStrategy {

	@Override
	public void updateState(Ball ball) {
		// TODO Auto-generated method stub
		Point currVel = ball.getVelocity();
		double vx_old = currVel.getX();
		double vy_old = currVel.getY();
		Point newVel = new Point((int) Math.round(vx_old
				* Math.cos(2 * Math.PI / 13) - vy_old
				* Math.sin(2 * Math.PI / 13)), (int) Math.round(vy_old
				* Math.cos(2 * Math.PI / 13) + vx_old
				* Math.sin(2 * Math.PI / 13)));
		ball.setVelocity(newVel);
	}
}
