package strategy;

import ball.Ball;

/**
 * an interface that can be implemented by different strategies
 */
public interface IUpdateStrategy {
	/**
	 * update the state of the object
	 * @param ball
	 */
	void updateState(Ball ball);

}
