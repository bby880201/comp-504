package strategy;

import ball.Ball;

/**
 * a strategy capable of composing arbitrarily complex behaviors
 */
public class MultiStrategy implements IUpdateStrategy {
	private IUpdateStrategy _s1;
	private IUpdateStrategy _s2;

	/**
	 * construct that takes in two individual strategies
	 */
	public MultiStrategy(IUpdateStrategy s1, IUpdateStrategy s2) {
		_s1 = s1;
		_s2 = s2;
	}

	/** 
	 * composing the two behavior and change the ball state accordingly 
	 */
	public void updateState(Ball context) {
		_s1.updateState(context);
		_s2.updateState(context);
	}
}