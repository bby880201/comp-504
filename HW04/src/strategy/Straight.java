package strategy;

import ball.Ball;

/**
 * basic straight moving strategy
 */
public class Straight implements IUpdateStrategy {

	@Override
	public void updateState(Ball ball) {
		// do nothing
	}
}
