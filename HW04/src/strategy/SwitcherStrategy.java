package strategy;

import ball.Ball;

/**
 * strategy that creates a ball which can change its behavior strategy upon button clicking
 */
public class SwitcherStrategy implements IUpdateStrategy {
	/** 
	 * create a straight strategy 
	 */
	private IUpdateStrategy _strategy = new Straight();

	@Override
	public void updateState(Ball ball) {
		_strategy.updateState(ball);
	}

	/** 
	 * set to a new strategy 
	 */
	public void setStrategy(IUpdateStrategy newStrategy) {
		_strategy = newStrategy;
	}
}