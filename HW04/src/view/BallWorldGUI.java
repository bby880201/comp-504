package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.BoxLayout;
import javax.swing.JComboBox;

import model.IModelCtrlAdapter;

import javax.swing.border.BevelBorder;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;

public class BallWorldGUI<TDropListItem, TPaintDropListItem> extends JFrame {

	private IModelCtrlAdapter<TDropListItem, TPaintDropListItem> _modelCtrlAdapter;
	private IView2ModelUpdateAdapter _view2ModelUpdateAdapter = IView2ModelUpdateAdapter.NULL_OBJECT;

	private static final long serialVersionUID = 5545354999257563085L;

	private final JPanel contentPane = new JPanel();
	private final JPanel controlPanel = new JPanel();
	private final JPanel behaviorStrategyPanel = new JPanel();
	private final JPanel addedStrategyPanel = new JPanel();
	private final JPanel switcherPanel = new JPanel();
	private final JPanel paintingStrategyPanel = new JPanel();

	private final JTextField textField = new JTextField();
	private final JTextField paintingTextField = new JTextField();

	private final JButton btnAddBehabior = new JButton("Add to lists");
	private final JButton btnMake = new JButton("Make selected ball!");
	private final JButton btnCombine = new JButton("Combine");
	private final JButton btnMakeSwicher = new JButton("Make Switcher!");
	private final JButton btnSwitch = new JButton("Switch");
	private final JButton btnClearAll = new JButton("Clear All");
	private final JButton btnAddPainting = new JButton("Add Style");

	/**
	 * Top drop list, used for save all added behavior strategy
	 */
	private JComboBox<TDropListItem> _addedStrategyList;
	/**
	 * Bottom drop list, used for combining with the top list selection.
	 */
	private JComboBox<TDropListItem> _combineStrategyList;

	/**
	 * paintingStrategy list, used for save all added painting strategy
	 */
	private JComboBox<TPaintDropListItem> _addedpaintingStrategyList;

	/** 
	 * painting field
	 */
	private JPanel centerPanel = new JPanel() {
		private static final long serialVersionUID = 1L;

		/**
		 * Overridden paintComponent method to paint a shape in the panel.
		 * @param g The Graphics object to paint on.
		 **/
		public void paintComponent(Graphics g) {
			super.paintComponent(g); // Do everything normally done first, e.g. clear the screen.
			_view2ModelUpdateAdapter.paint(g);
		}
	};

	//constructor
	public BallWorldGUI(
			IModelCtrlAdapter<TDropListItem, TPaintDropListItem> _modelCtrlAdapter,
			IView2ModelUpdateAdapter _view2ModelUpdateAdapter) {
		this._modelCtrlAdapter = _modelCtrlAdapter;
		this._view2ModelUpdateAdapter = _view2ModelUpdateAdapter;
		initGUI();
	}

	private void initGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1200, 800);

		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		controlPanel.setBackground(Color.GREEN);
		contentPane.add(controlPanel, BorderLayout.NORTH);
		controlPanel.setToolTipText("control panel");

		behaviorStrategyPanel.setBorder(new TitledBorder(null,
				"BehaviorStrategy", TitledBorder.LEADING, TitledBorder.TOP,
				null, null));
		controlPanel.add(behaviorStrategyPanel);
		behaviorStrategyPanel.setLayout(new BoxLayout(behaviorStrategyPanel,
				BoxLayout.Y_AXIS));
		behaviorStrategyPanel.setToolTipText("left control panel");

		behaviorStrategyPanel.add(textField);
		textField.setColumns(10);
		textField.setText("Straight");
		textField.setToolTipText("enter a valid strategy name.");

		btnAddBehabior
				.setToolTipText("Add strategy above to the strategy lists on right");
		btnAddBehabior.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//return a strategyFac object
				TDropListItem o = _modelCtrlAdapter.addStrategy(textField
						.getText());
				if (null == o)
					return; // just in case
				_addedStrategyList.insertItemAt(o, 0);
				_addedStrategyList.setSelectedIndex(0);
				_combineStrategyList.insertItemAt(o, 0);
				_combineStrategyList.setSelectedIndex(0);
			}
		});
		btnAddBehabior.setAlignmentX(Component.CENTER_ALIGNMENT);
		behaviorStrategyPanel.add(btnAddBehabior);

		controlPanel.add(addedStrategyPanel);
		addedStrategyPanel.setLayout(new BoxLayout(addedStrategyPanel,
				BoxLayout.Y_AXIS));
		addedStrategyPanel.setToolTipText("Existing Strategy control panel");

		btnMake.setToolTipText("Make a ball with the selected strategy in the list.");
		btnMake.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				_modelCtrlAdapter.makeBall(_addedStrategyList
						.getItemAt(_addedStrategyList.getSelectedIndex()),
						_addedpaintingStrategyList
								.getItemAt(_addedpaintingStrategyList
										.getSelectedIndex()),
						(Container) contentPane);
			}
		});
		btnMake.setAlignmentX(Component.CENTER_ALIGNMENT);
		addedStrategyPanel.add(btnMake);

		_addedStrategyList = new JComboBox<TDropListItem>();
		_addedStrategyList
				.setToolTipText("The strategy to use for making new balls.");
		addedStrategyPanel.add(_addedStrategyList);

		_combineStrategyList = new JComboBox<TDropListItem>();
		_combineStrategyList
				.setToolTipText("The chosen strategy that can be combined with the strategy above it.");
		addedStrategyPanel.add(_combineStrategyList);

		btnCombine
				.setToolTipText("Combines the two strategies selected above into a new strategy.");
		btnCombine.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//combine the two strategies in the comboBox 
				TDropListItem o = _modelCtrlAdapter.combineStrategies(
						_addedStrategyList.getItemAt(_addedStrategyList
								.getSelectedIndex()), _combineStrategyList
								.getItemAt(_combineStrategyList
										.getSelectedIndex()));
				if (null == o)
					return; // just in case
				_addedStrategyList.insertItemAt(o, 0);
				_combineStrategyList.insertItemAt(o, 0);
			}
		});
		btnCombine.setAlignmentX(Component.CENTER_ALIGNMENT);
		addedStrategyPanel.add(btnCombine);

		switcherPanel.setLayout(new BoxLayout(switcherPanel, BoxLayout.Y_AXIS));
		switcherPanel.setBorder(new TitledBorder(null, "SwitcherControl",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		controlPanel.add(switcherPanel);
		switcherPanel.setToolTipText("switcher control panel");

		btnMakeSwicher.setToolTipText("Makes a strategy switchable ball.");
		btnMakeSwicher.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				_modelCtrlAdapter.makeStrategySwitchableBall(
						_addedpaintingStrategyList
								.getItemAt(_addedpaintingStrategyList
										.getSelectedIndex()),
						(Container) contentPane);
			}
		});

		btnMakeSwicher.setAlignmentX(Component.CENTER_ALIGNMENT);
		switcherPanel.add(btnMakeSwicher);

		btnSwitch
				.setToolTipText("Switches all strategy switchable Balls to the strategy selected in the top list on left.");
		btnSwitch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				_modelCtrlAdapter.setSwitcherStrategy(_addedStrategyList
						.getItemAt(_addedStrategyList.getSelectedIndex()));
			}
		});
		btnSwitch.setAlignmentX(Component.CENTER_ALIGNMENT);
		switcherPanel.add(btnSwitch);

		btnClearAll.setToolTipText("Remove everything from the canvas.");
		btnClearAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				_view2ModelUpdateAdapter.clearBalls();
			}
		});

		controlPanel.add(btnClearAll);

		/**
		 * Painting strategy relevant UI
		 */
		paintingStrategyPanel.setBorder(new TitledBorder(null,
				"PaintingStrategy", TitledBorder.LEADING, TitledBorder.TOP,
				null, null));
		paintingStrategyPanel.setToolTipText("painting strategy control panel");
		controlPanel.add(paintingStrategyPanel);
		paintingStrategyPanel.setSize(100, 50);

		paintingTextField.setText("Ball");
		paintingStrategyPanel.add(paintingTextField);
		paintingTextField.setToolTipText("please input a painting strategy");
		paintingTextField.setColumns(10);

		btnAddPainting.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TPaintDropListItem o = _modelCtrlAdapter
						.addPaintStrategy(paintingTextField.getText());
				if (null == o)
					return;
				_addedpaintingStrategyList.insertItemAt(o, 0);
				_addedpaintingStrategyList.setSelectedIndex(0);
			}
		});
		btnAddPainting.setHorizontalAlignment(SwingConstants.LEFT);
		btnAddPainting.setToolTipText("adding painting strategy to the list");
		btnAddPainting.setAlignmentX(Component.CENTER_ALIGNMENT);

		paintingStrategyPanel.add(btnAddPainting);
		paintingStrategyPanel.setLayout(new BoxLayout(paintingStrategyPanel,
				BoxLayout.Y_AXIS));

		_addedpaintingStrategyList = new JComboBox<TPaintDropListItem>();
		_addedpaintingStrategyList.setEditable(true);
		_addedpaintingStrategyList
				.setToolTipText("The chosen painting strategy here can be added to the object.");

		paintingStrategyPanel.add(_addedpaintingStrategyList);
		centerPanel.setBorder(new BevelBorder(BevelBorder.RAISED, null, null,
				null, null));

		contentPane.add(centerPanel, BorderLayout.CENTER);
		centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.X_AXIS));
	}

	public void start() {
		setVisible(true);
	}

	/**
	 * Updates the view by repainting the canvas
	 */
	public void update() {
		centerPanel.repaint();
	}

	public JPanel getCenterPanel() {
		return centerPanel;
	}

	/** @return  the current height of painting field */
	public int getYY() {
		return centerPanel.getHeight();
	}

	/** @return  the current width of painting field */
	public int getXX() {
		return centerPanel.getWidth();
	}

}
