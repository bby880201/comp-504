package view;

import java.awt.Container;
import java.awt.Graphics;

import paint.strategy.IPaintStrategy;
import strategy.IUpdateStrategy;

/**
 * the adapter object that translate the communication between view and model
 */
public interface IView2ModelUpdateAdapter {

	/**
	 * the paints function of this view adapter
	 * @param g
	 */
	public void paint(Graphics g);

	/**
	 * load balls to view
	 * @param ballName the name of a concrete class of ABall
	 */
	void loadBall(IUpdateStrategy strat, IPaintStrategy paintStrat,
			Container canvas);

	/**
	 * clear all balls on the view
	 */
	public void clearBalls();

	/**
	 * an Null object of this interface
	 */
	public static final IView2ModelUpdateAdapter NULL_OBJECT = new IView2ModelUpdateAdapter() {
		public void paint(Graphics g) {
		}

		@Override
		public void loadBall(IUpdateStrategy strat, IPaintStrategy paintStrat,
				Container canvas) {
		}

		@Override
		public void clearBalls() {
		}
	};

}
