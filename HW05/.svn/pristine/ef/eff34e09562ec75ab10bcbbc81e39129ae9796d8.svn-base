package edu.rice.comp504.ballworld.ball.paint;

import java.awt.Graphics;
import java.awt.geom.AffineTransform;

import edu.rice.comp504.ballworld.ball.Ball;

/**
 * The strategy that would paint the image and always keep the image upright.
 * @author Li
 *
 */
public class UprightImagePaintStrategy extends ImagePaintStrategy {

	/**
	 * The constructor that takes in the image's file name and fill factor
	 * @param filename the image's filename
	 * @param fillFactor the fill factor to set to
	 */
	public UprightImagePaintStrategy(String filename, double fillFactor) {
		super(filename, fillFactor);
	}

	/**
	 * The constructor that takes in the image's file name, fill factor and AffineTransform object to use
	 * @param transform
	 * @param filename the image's filename
	 * @param fillFactor the fill factor to set to
	 */
	public UprightImagePaintStrategy(AffineTransform transform, String filename, double fillFactor) {
		super(transform, filename, fillFactor);
	}

	@Override
	protected void paintCfg(Graphics g, Ball host) {
		super.paintCfg(g, host);
		if (Math.abs(Math.atan2(host.getVelocityY(), host.getVelocityX())) > Math.PI / 2.0) {
			transform.scale(1.0, -1.0);
		}
	}
}
