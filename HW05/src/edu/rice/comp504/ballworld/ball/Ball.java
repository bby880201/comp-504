package edu.rice.comp504.ballworld.ball;

import java.awt.Color;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.geom.Point2D;

import edu.rice.comp504.ballworld.ball.paint.IPaintStrategy;
import edu.rice.comp504.ballworld.ball.update.IUpdateStrategy;
import edu.rice.comp504.ballworld.model.IBallCmd;
import edu.rice.comp504.ballworld.util.IDispatcher;
import edu.rice.comp504.ballworld.util.IObserver;

/**
 * Ball Class for all balls in ball world.
 * 
 * @author Li Ding
 */
public class Ball implements IObserver<IBallCmd> {
	
	private static final int DEFAULT_MAX_VELOCITY = 8;
	private static final int DEFAULT_RADIUS = 16;
	
	/**
	 * The canvas of the ball being painted on.
	 */
	private Container canvas;
	
	/**
	 * x and y velocity of the ball.
	 */
	private double velocityX;
	private double velocityY;
	
	/**
	 * Color of the Ball.
	 */
	Color color;
	
	/**
	 * Location of the Ball's center point.
	 */
	private Point2D location;

	/**
	 * The width of the ball.
	 */
	private double width;
	
	
	/**
	 * The ball's state change strategy after each update.
	 */
	private IUpdateStrategy<IBallCmd> updateStrategy;
	
	/**
	 * The paint strategy for the ball.
	 */
	private IPaintStrategy paintStrategy;
	
	/**
	 * Whether the ball have just bounced.
	 */
	boolean justBounced = false;
	
	/**
	 * Used when the ball need to change the update strategy 
	 */
	private int updateCount = -1;

	/**
	 * Create a ball with default size, random color and random velocity at left up corner of the GUI frame.
	 * 
	 * @param updateStrategy The strategy to change the ball state after each move
	 * @param paintStrategy The strategy to paint the ball on screen.
	 * @param canvas The canvas of the ball being painted on.
	 */
	public Ball(IUpdateStrategy<IBallCmd> updateStrategy, IPaintStrategy paintStrategy, Container canvas) {
		init(new Point2D.Double(Math.random() * 1200, Math.random() * 660), DEFAULT_RADIUS, getRandomColor(),
				getRandomSpeed(), getRandomSpeed(), updateStrategy, paintStrategy, canvas);
	}

	/**
	 * Create a ball with random velocity and set up fields with given parameters.
	 * 
	 * @param location The location center point.
	 * @param r The radius of the Ball.
	 * @param color The color of the Ball.
	 * @param updateStrategy The strategy to change the ball state after each move
	 * @param paintStrategy The strategy to paint the ball on screen.
	 * @param canvas The canvas of the ball being painted on.
	 */
	public Ball(Point2D location, double r, Color color, 
			IUpdateStrategy<IBallCmd> updateStrategy, IPaintStrategy paintStrategy, Container canvas) {
		init(location, r, color, getRandomSpeed(), getRandomSpeed(), updateStrategy, paintStrategy, canvas);
	}
	
	/**
	 * Create a ball and set up all fields with given parameters.
	 * 
	 * @param location The location center point.
	 * @param r The radius of the ball.
	 * @param color The color of the ball.
	 * @param velocityX X velocity of the ball.
	 * @param velocityY Y velocity of the ball.
	 * @param updateStrategy The strategy to change the ball state after each move
	 * @param paintStrategy The strategy to paint the ball on screen.
	 * @param canvas The canvas of the ball being painted on.
	 */
	public Ball(Point2D location, double r, Color color,
			double velocityX, double velocityY, IUpdateStrategy<IBallCmd> updateStrategy,
			IPaintStrategy paintStrategy, Container canvas) {
		init(location, r, color, velocityX, velocityY, updateStrategy, paintStrategy, canvas);
	}
	
	/**
	 * Create a ball and set up all fields with given parameters.
	 * 
	 * @param location The location center point.
	 * @param r The radius of the ball.
	 * @param color The color of the ball.
	 * @param velocityX X velocity of the ball.
	 * @param velocityY Y velocity of the ball.
	 * @param updateStrategy The strategy to change the ball state after each move
	 * @param paintStrategy The strategy to paint the ball on screen.
	 * @param canvas The canvas of the ball being painted on.
	 */
	private  void init(Point2D location, double r, Color color,
			double velocityX, double velocityY, IUpdateStrategy<IBallCmd> updateStrategy,
			IPaintStrategy paintStrategy, Container canvas) {
		this.color = color;
		this.location = location;
		this.width = r * 2.0;
		this.velocityX = velocityX;
		this.velocityY = velocityY;
		this.updateStrategy = updateStrategy;
		this.paintStrategy = paintStrategy;
		this.canvas = canvas;
		paintStrategy.init(this);
	}
	
	/**
	 * Paint the Ball with the given paint strategy.
	 * @param g The graphic object used to paint.
	 */
	public void paint(Graphics g) {
		paintStrategy.paint(g, this);
	}
	
	@Override
	public void update(IDispatcher<IBallCmd> dispatcher, IBallCmd msg) {
		msg.apply(this, dispatcher);
	}
	
	/**
	 * Move the ball to next position.
	 */
	public void move() {
		location.setLocation(location.getX() + velocityX, location.getY() + velocityY);
	}
	
	/**
	 * Bounce the ball if needed.
	 * @param bound The rectangle bound.
	 */
	public void bounce() {
		Rectangle bound = canvas.getBounds();
		justBounced = false;
		if(isXOutOfBound(bound.width)) {
			bounceX(bound.width);
			justBounced = true;
		}
		if(isYOutOfBound(bound.height)) {
			bounceY(bound.height);
			justBounced = true;
		}
	}
	/**
	 * using strategy to update ball state
	 */
	public void updateState(IDispatcher<IBallCmd> disp) {
		updateStrategy.updateState(this, disp);
	}
	
	/**
	 * Bounce the ball when reaching the horizontal bound.
	 * @param bound The width of horizontal bound.
	 */
	protected void bounceX(int bound) {
		if(location.getX() + getRadius() > bound) location.setLocation(bound - getRadius(), location.getY());
		if(location.getX() - getRadius() < 0) location.setLocation(getRadius(), location.getY());
		velocityX = 0 - velocityX;
	}
	
	/**
	 * Bounce the ball when reaching the vertical bound.
	 * @param bound The height of vertical bound.
	 */
	protected void bounceY(int bound) {
		if(location.getY() + getRadius() > bound) location.setLocation(location.getX(), bound - getRadius());
		if(location.getY() - getRadius() < 0) location.setLocation(location.getX(), getRadius());
		velocityY = 0 - velocityY;
	}
	
	/**
	 * Check if the ball is out of horizontal bound.
	 * @param bound The width of horizontal bound.
	 * @return whether the ball is out of horizontal bound.
	 */
	protected boolean isXOutOfBound(int bound) {
		if(location.getX() + getRadius() > bound || location.getX() - getRadius() < 0) return true;
		return false;
	}
	
	/**
	 * Check if the ball is out of vertical bound.
	 * @param bound The height of vertical bound.
	 * @return whether the ball is out of vertical bound.
	 */
	protected boolean isYOutOfBound(int bound) {
		if(location.getY() + getRadius() > bound || location.getY() - getRadius() < 0) return true;
		return false;
	}
	
	/**
	 * Getter of radius.
	 * @return Radius of the ball.
	 */
	public double getRadius() {
		return this.width / 2.0;
	}
	
	/**
	 * Setter of radius.
	 * @param r Radius value to set to.
	 */
	public void setRadius(double r) {
		this.width = r * 2.0;
	}
	
	/**
	 * @param updateStrategy the updateStrategy to set
	 */
	public void setUpdateStrategy(IUpdateStrategy<IBallCmd> updateStrategy) {
		this.updateStrategy = updateStrategy;
	}
	
	/**
	 * Generate a random color.
	 * @return Random Color object
	 */
	public static Color getRandomColor() {
		return new Color((int)(Math.random()*256), (int)(Math.random()*256), (int)(Math.random()*256));
	}
	
	/**
	 * Generate a random speed.
	 * @return random speed
	 */
	public static double getRandomSpeed() {
		return Math.random() * DEFAULT_MAX_VELOCITY + 1;
	}

	/**
	 * @return the velocityX
	 */
	public double getVelocityX() {
		return velocityX;
	}

	/**
	 * @param velocityX the velocityX to set
	 */
	public void setVelocityX(double velocityX) {
		this.velocityX = velocityX;
	}

	/**
	 * @return the velocityY
	 */
	public double getVelocityY() {
		return velocityY;
	}

	/**
	 * @param velocityY the velocityY to set
	 */
	public void setVelocityY(double velocityY) {
		this.velocityY = velocityY;
	}

	/**
	 * @return the justBounced
	 */
	public boolean isJustBounced() {
		return justBounced;
	}

	
	/**
	 * @return the color
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * @param color the color to set
	 */
	public void setColor(Color color) {
		this.color = color;
	}

	public Container getCanvas() {
		return canvas;
	}

	/**
	 * @return the location
	 */
	public Point2D getLocation() {
		return location;
	}

	/**
	 * @return the paintStrategy
	 */
	public IPaintStrategy getPaintStrategy() {
		return paintStrategy;
	}
	
	/**
	 * Get update count
	 * @return update count
	 */
	public int getUpdateCount(){return updateCount;}
	
	/**
	 * Set update count
	 * @param count the count want to be set
	 */
	public void setUpdateCount(int count){updateCount = count;}
}
