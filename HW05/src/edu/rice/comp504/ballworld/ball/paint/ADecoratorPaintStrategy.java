package edu.rice.comp504.ballworld.ball.paint;

import java.awt.Graphics;
import java.awt.geom.AffineTransform;

import edu.rice.comp504.ballworld.ball.Ball;

/**
 * A Decorator Strategy that allows the changing of default decoree paint behaviors
 * @author Li
 *
 */
public abstract class ADecoratorPaintStrategy extends APaintStrategy {

	/**
	 * The PaintStrategy to be decorated.
	 */
	private APaintStrategy decoree;

	/**
	 * The constructor that takes in a decoree.
	 * @param decoree The strategy to be decorated.
	 */
	public ADecoratorPaintStrategy(APaintStrategy decoree) {
		super(new AffineTransform());
		this.decoree = decoree;
	}

	@Override
	public void init(Ball host) {
		decoree.init(host);
	}

	@Override
	public void paint(Graphics g, Ball host) {
		decoree.paint(g, host);
	}

	@Override
	public void paintXfrm(Graphics g, Ball host, AffineTransform at) {
		decoree.paintXfrm(g, host, at);
	}

}
