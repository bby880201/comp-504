package edu.rice.comp504.ballworld.ball.paint;

import java.awt.Graphics;
import java.awt.geom.AffineTransform;

import edu.rice.comp504.ballworld.ball.Ball;

/**
 * The abstract paint strategy that will do all the affine transforms.
 * @author Li
 *
 */
public abstract class APaintStrategy implements IPaintStrategy {

	/**
	 * The AffineTransform object to be used for this strategy.
	 */
	protected AffineTransform transform;

	/**
	 * The constructor that takes in a AffineTransform object.
	 * @param transform The AffineTransform object to use.
	 */
	public APaintStrategy(AffineTransform transform) {
		this.transform = transform;
	}

	@Override
	public void paint(Graphics g, Ball host) {
		g.setColor(host.getColor());
		transform.setToTranslation(host.getLocation().getX(), host.getLocation().getY());
		transform.scale(host.getRadius(), host.getRadius());
		transform.rotate(host.getVelocityX(), host.getVelocityY());
		paintCfg(g, host);
		paintXfrm(g, host, transform);
	}

	/**
	 * Injecting additional processing inside this method into the paint method
	 * @param g The graphics object to paint on.
	 * @param host The ball with information needed for painting.
	 */
	protected void paintCfg(Graphics g, Ball host) {
	}

	/**
	 * Uses supplied AffineTransform object to do the final opeartions of the entire painting.
	 * @param g The graphics object to paint on.
	 * @param host The ball to be painted.
	 * @param at The AffineTransform object to use for transformations.
	 */
	public abstract void paintXfrm(Graphics g, Ball host, AffineTransform at);

	@Override
	public void init(Ball host) {
	}

}
