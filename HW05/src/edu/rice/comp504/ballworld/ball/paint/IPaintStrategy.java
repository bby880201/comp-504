package edu.rice.comp504.ballworld.ball.paint;

import java.awt.Graphics;

import edu.rice.comp504.ballworld.ball.Ball;

/**
 * Interface for strategies to paint the ball.
 * @author Li
 */
public interface IPaintStrategy {
	/**
	 * The paint operation that is called during the repainting process. 
	 * Paint the ball at given with given Graphic object.
	 * @param g The Graphic object used for painting.
	 * @param host The ball to paint.
	 */
	void paint(Graphics g, Ball host);

	/**
	 *Initialize the strategy and host ball.
	 * @param host The ball to paint.
	 */
	void init(Ball host);
}
