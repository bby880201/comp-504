package edu.rice.comp504.ballworld.ball.paint;

/**
 * A factory interface to make IPaintStrategy.
 *
 */
public interface IPaintStrategyFac {
	/**
	 * Make a paint strategy
	 * @return The generated paint strategy
	 */
	public IPaintStrategy make();
}
