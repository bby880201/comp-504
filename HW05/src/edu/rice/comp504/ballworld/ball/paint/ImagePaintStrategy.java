package edu.rice.comp504.ballworld.ball.paint;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Toolkit;
import java.awt.geom.AffineTransform;
import java.awt.image.ImageObserver;

import edu.rice.comp504.ballworld.ball.Ball;

/**
 * A concrete strategy that will paint image at given location.
 * @author Li
 *
 */
public class ImagePaintStrategy extends APaintStrategy {

	/**
	 * The Image object to be painted.
	 */
	private Image image;

	/**
	 * The factor to set the given image from it's default size to a unit size
	 */
	private double fillFactor;

	/**
	 * The ratio of unit size to actual size
	 */

	private double scaleFactor;
	/**
	 * The Image Observer for the image to paint.
	 */
	private ImageObserver imageObs;

	/**
	 * The AffineTransform object use for the image to do all the transform.
	 */
	protected AffineTransform localTransform = new AffineTransform();

	/**
	 * The constructor that takes in the image's file name and fill factor
	 * @param filename the image's filename
	 * @param fillFactor the fill factor to set to
	 */
	public ImagePaintStrategy(String filename, double fillFactor) {
		this(new AffineTransform(), filename, fillFactor);
	}

	/**
	 * The constructor that takes in the image's file name, fill factor and AffineTransform object to use
	 * @param transform
	 * @param filename the image's filename
	 * @param fillFactor the fill factor to set to
	 */
	public ImagePaintStrategy(AffineTransform transform, String filename, double fillFactor) {
		super(transform);
		this.fillFactor = fillFactor;
		try {
			image = Toolkit.getDefaultToolkit().getImage(this.getClass().getResource(filename));
		} catch (Exception e) {
			System.out.println("ImagePaintStrategy: Error reading file: " + filename + "\n" + e);
		}
	}

	@Override
	public void init(Ball host) {
		super.init(host);
		imageObs = host.getCanvas();
		MediaTracker mt = new MediaTracker(host.getCanvas());
		mt.addImage(image, 1);
		try {
			mt.waitForAll();
		} catch (Exception e) {
			System.out.println("ImagePaintStrategy.init(): Error waiting for image.  Exception = " + e);
		}
		scaleFactor = 2.0 / (fillFactor * (image.getWidth(imageObs) + image.getHeight(imageObs)) / 2.0);
	}

	@Override
	public void paintXfrm(Graphics g, Ball host, AffineTransform transform) {
		localTransform.setToScale(scaleFactor, scaleFactor);
		localTransform.translate(-image.getWidth(imageObs) / 2.0, -image.getHeight(imageObs) / 2.0);
		localTransform.preConcatenate(transform);
		((Graphics2D) g).drawImage(image, localTransform, imageObs);
	}
}
