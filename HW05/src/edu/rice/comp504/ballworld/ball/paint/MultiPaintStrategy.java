package edu.rice.comp504.ballworld.ball.paint;

import java.awt.Graphics;
import java.awt.geom.AffineTransform;

import edu.rice.comp504.ballworld.ball.Ball;

/**
 * The Paint Strategy that consists of multiple different paint strategies.s
 * @author Li
 *
 */
public class MultiPaintStrategy extends APaintStrategy {

	/**
	 * An array of strategies that this strategy consist of.s
	 */
	private APaintStrategy[] strategies;

	/**
	 * A constructor that takes in all the strategies that itself should consist of
	 * @param strategies some strategies to add to this strategy
	 */
	public MultiPaintStrategy(APaintStrategy... strategies) {
		this(new AffineTransform(), strategies);
	}

	/**
	 * A constructor that takes in a AffineTransform object that it will use 
	 * and all the strategies that itself should consist of.
	 * @param transform The object to use for the transformation.
	 * @param strategies some strategies to add to this strategy
	 */
	public MultiPaintStrategy(AffineTransform transform, APaintStrategy... strategies) {
		super(transform);
		this.strategies = strategies;
	}

	@Override
	public void paintXfrm(Graphics g, Ball host, AffineTransform transform) {
		for (APaintStrategy strategy : strategies) {
			strategy.paintXfrm(g, host, transform);
		}
	}

	@Override
	public void init(Ball host) {
		for (APaintStrategy strategy : strategies) {
			strategy.init(host);
		}
	}

}
