package edu.rice.comp504.ballworld.ball.paint;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.geom.AffineTransform;

import edu.rice.comp504.ballworld.ball.Ball;

/**
 * Paint the decorated strategies with given color, regardless of the original color to use.
 * @author Li
 *
 */
public class OverrideColorPaintStrategy extends ADecoratorPaintStrategy {
	/**
	 * The color to use for painting.
	 */
	private Color color;

	/**
	 * The constructor that takes in strategy to be added and the desired color.
	 * @param decoree The strategy to be modified.
	 * @param color The desired Color object.
	 */
	public OverrideColorPaintStrategy(APaintStrategy decoree, Color color) {
		super(decoree);
		this.color = color;
	}

	@Override
	public void paintXfrm(Graphics g, Ball host, AffineTransform at) {
		g.setColor(color);
		super.paintXfrm(g, host, at);
	}

}
