package edu.rice.comp504.ballworld.ball.paint;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.AffineTransform;

import edu.rice.comp504.ballworld.ball.Ball;


/**
 * The strategy to paint any awt shape.
 * @author Li
 *
 */
public class ShapePaintStrategy extends APaintStrategy {

	/**
	 * The shape to be painted.
	 */
	private Shape shape;

	/**
	 * The constructor that takes in shape to paint
	 * @param shape The Shape object to paint.
	 */
	public ShapePaintStrategy(Shape shape) {
		super(new AffineTransform());
		this.shape = shape;
	}

	/**
	 * The constructor that takes in a shape to paint and AffineTransform object to use
	 * @param shape The Shape object to paint.
	 * @param transform The object to be used for transformation.
	 */
	public ShapePaintStrategy(Shape shape, AffineTransform transform) {
		super(transform);
		this.shape = shape;
	}

	@Override
	public void paintXfrm(Graphics g, Ball host, AffineTransform transform) {
		Shape newShape = transform.createTransformedShape(shape);
		((Graphics2D) g).fill(newShape);
	}

}
