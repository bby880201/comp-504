package edu.rice.comp504.ballworld.ball.paint;

import java.awt.Graphics;
import java.awt.Shape;
import java.awt.geom.AffineTransform;

import edu.rice.comp504.ballworld.ball.Ball;

/**
 * The strategy that will paint a awt shape and always keep the shape upright.
 * @author Li
 *
 */
public class UprightShapePaintStrategy extends ShapePaintStrategy {

	/**
	 * The constructor that takes in shape to paint
	 * @param shape The Shape object to paint.
	 */
	public UprightShapePaintStrategy(Shape shape) {
		super(shape);
	}

	/**
	 * The constructor that takes in a shape to paint and AffineTransform object to use
	 * @param shape The Shape object to paint.
	 * @param transform The object to be used for transformation.
	 */
	public UprightShapePaintStrategy(AffineTransform transform, Shape shape) {
		super(shape, transform);
	}

	@Override
	protected void paintCfg(Graphics g, Ball host) {
		super.paintCfg(g, host);
		if (Math.abs(Math.atan2(host.getVelocityY(), host.getVelocityX())) > Math.PI / 2.0) {
			transform.scale(1.0, -1.0);
		}
	}
}
