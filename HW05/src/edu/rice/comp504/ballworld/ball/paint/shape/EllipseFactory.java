package edu.rice.comp504.ballworld.ball.paint.shape;

import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;

/**
 * The factory to generate ellipse
 * @author Li
 *
 */
public class EllipseFactory implements IShapeFactory {

	/**
	 * Singleton for this factory.
	 */
	public static final EllipseFactory Singleton = new EllipseFactory();
	
	/**
	 * The AffineTransform object to use for the transformation.
	 */
	AffineTransform transform = new AffineTransform();
	
	/**
	 * A united sized ellipse
	 */
	Shape shape = new Ellipse2D.Double(-1.0, -1.0, 2.0, 2.0);

	@Override
	public Shape makeShape(double x, double y, double xScale, double yScale) {
		transform.setToTranslation(x, y);
		transform.scale(xScale, yScale);
		return transform.createTransformedShape(shape);
	}

}
