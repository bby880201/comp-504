package edu.rice.comp504.ballworld.ball.paint.shape;

import java.awt.Point;
import java.awt.geom.AffineTransform;

/**
 * The factory that makes polygon which looks like an ugly fish with an opening mouth.
 * @author Li
 *
 */
public class Fish1Factory extends PolygonFactory {

	/**
	 * The singleton for the factory
	 */
	public static final Fish1Factory Singleton = new Fish1Factory();

	/**
	 * Constructor of the factory
	 */
	public Fish1Factory() {
		super(new AffineTransform(), 0.04, new Point(16, 0), new Point(25, 6), new Point(15, 12), new Point(0, 12),
				new Point(-30, 5), new Point(-34, 20), new Point(-34, -20), new Point(-30, -5), new Point(0, -12),
				new Point(15, -12), new Point(25, -6));
	}

}
