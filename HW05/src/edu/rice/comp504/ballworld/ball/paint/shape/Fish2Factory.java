package edu.rice.comp504.ballworld.ball.paint.shape;

import java.awt.Point;
import java.awt.geom.AffineTransform;

/**
 * The factory that makes polygon which looks like an ugly fish with an closing mouth.
 * @author Li
 *
 */
public class Fish2Factory extends PolygonFactory {

	/**
	 * The singleton for the factory
	 */
	public static final Fish2Factory Singleton = new Fish2Factory();

	/**
	 * Constructor of the factory
	 */
	public Fish2Factory() {
		super(new AffineTransform(), 0.04, new Point(25, 6), new Point(15, 12), new Point(0, 12), new Point(-30, 5),
				new Point(-34, 20), new Point(-34, -20), new Point(-30, -5), new Point(0, -12), new Point(15, -12),
				new Point(25, -6));
	}

}
