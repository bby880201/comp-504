package edu.rice.comp504.ballworld.ball.paint.shape;

import java.awt.Shape;

/**
 * The factory interface to make awt Shape
 * @author Li
 *
 */
public interface IShapeFactory {
	/**
	 * Make a Shape that is translated and scaled from a unit sized shape.
	 * @param x The x coordinate to translate the shape.
	 * @param y The x coordinate to translate the shape.
	 * @param xScale The x coordinate to scale the shape.
	 * @param yScale The y coordinate to scale the shape.
	 * @return
	 */
	public Shape makeShape(double x, double y, double xScale, double yScale);
}
