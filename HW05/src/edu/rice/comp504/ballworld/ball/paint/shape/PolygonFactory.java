package edu.rice.comp504.ballworld.ball.paint.shape;

import java.awt.Point;
import java.awt.Polygon;
import java.awt.Shape;
import java.awt.geom.AffineTransform;

/**
 * The factory to generate polygon
 * @author Li
 *
 */
public class PolygonFactory implements IShapeFactory {

	/**
	 * The prototype polygon which is not yet unit size
	 */
	private Polygon polygon;
	
	/**
	 * The factor to scale the polygon to a unit sized shape.
	 */
	private double scaleFactor;
	
	/**
	 * The AffineTransform object to use.
	 */
	private AffineTransform transform;

	/**
	 * The constructor of the factory which takes in AffineTransform object, the scale factor to use, 
	 * and all points which the polygon consist of
	 * @param transform
	 * @param scaleFactor
	 * @param points
	 */
	public PolygonFactory(AffineTransform transform, double scaleFactor, Point... points) {
		this.scaleFactor = scaleFactor;
		polygon = new Polygon();
		for (Point p : points) {
			polygon.addPoint((int) p.getX(), (int) p.getY());
		}
		this.transform = transform;
	}

	@Override
	public Shape makeShape(double x, double y, double xScale, double yScale) {
		transform.setToTranslation(x, y);
		transform.scale(xScale * scaleFactor, yScale * scaleFactor);
		return transform.createTransformedShape(polygon);
	}

}
