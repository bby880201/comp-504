package edu.rice.comp504.ballworld.ball.paint.strategy;

import edu.rice.comp504.ballworld.ball.paint.ShapePaintStrategy;
import edu.rice.comp504.ballworld.ball.paint.shape.EllipseFactory;


/**
 * The strategy to paint a ball.
 * @author Li
 *
 */
public class BallPaintStrategy extends ShapePaintStrategy {
	
	/**
	 * The constructor which requires no parameters.
	 */
	public BallPaintStrategy() {
		super(EllipseFactory.Singleton.makeShape(0, 0, 1.0, 1.0));
	}
}
