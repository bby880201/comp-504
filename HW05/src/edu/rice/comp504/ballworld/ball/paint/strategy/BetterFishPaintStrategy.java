package edu.rice.comp504.ballworld.ball.paint.strategy;

import java.awt.Color;
import java.awt.Graphics;

import edu.rice.comp504.ballworld.ball.Ball;
import edu.rice.comp504.ballworld.ball.paint.MultiPaintStrategy;
import edu.rice.comp504.ballworld.ball.paint.OverrideColorPaintStrategy;
import edu.rice.comp504.ballworld.ball.paint.ShapePaintStrategy;
import edu.rice.comp504.ballworld.ball.paint.shape.EllipseFactory;

/**
 * A strategy to paint a Fish1 fish with an eye and keep upright while moving.
 * @author Li
 *
 */
public class BetterFishPaintStrategy extends MultiPaintStrategy {
	
	/**
	 * The constructor which requires no parameters.
	 */
	public BetterFishPaintStrategy() {
		super(new Fish1PaintStrategy(), new OverrideColorPaintStrategy(
				new ShapePaintStrategy(EllipseFactory.Singleton.makeShape(0.4, -0.3, 0.15, 0.15)), Color.WHITE));
	}

	@Override
	protected void paintCfg(Graphics g, Ball host) {
		super.paintCfg(g, host);
		if (Math.abs(Math.atan2(host.getVelocityY(), host.getVelocityX())) > Math.PI / 2.0) {
			transform.scale(1.0, -1.0);
		}
	}
}
