package edu.rice.comp504.ballworld.ball.paint.strategy;

import edu.rice.comp504.ballworld.ball.paint.UprightImagePaintStrategy;


/**
 * The strategy to paint a bird GIF and keep the bird upright.
 * @author Li
 *
 */
public class BirdPaintStrategy extends UprightImagePaintStrategy {
	private static final String FILE_NAME = "./humbird.gif";

	/**
	 * The constructor which requires no parameters.
	 */
	public BirdPaintStrategy() {
		super(FILE_NAME, 0.8);
	}

}
