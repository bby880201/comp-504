package edu.rice.comp504.ballworld.ball.paint.strategy;

import edu.rice.comp504.ballworld.ball.paint.ShapePaintStrategy;
import edu.rice.comp504.ballworld.ball.paint.shape.Fish1Factory;

/**
 * A strategy to paint the Fish1 fish.
 * @author Li
 *
 */
public class Fish1PaintStrategy extends ShapePaintStrategy {

	/**
	 * The constructor which requires no parameters.
	 */
	public Fish1PaintStrategy() {
		super(Fish1Factory.Singleton.makeShape(0, 0, 1.0, 1.0));
	}

}
