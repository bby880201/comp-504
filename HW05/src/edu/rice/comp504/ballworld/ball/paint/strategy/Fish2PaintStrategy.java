package edu.rice.comp504.ballworld.ball.paint.strategy;

import edu.rice.comp504.ballworld.ball.paint.ShapePaintStrategy;
import edu.rice.comp504.ballworld.ball.paint.shape.Fish2Factory;

/**
 * A strategy to paint a Fish2 fish.
 * @author Li
 *
 */
public class Fish2PaintStrategy extends ShapePaintStrategy {

	/**
	 * The constructor which requires no parameters.
	 */
	public Fish2PaintStrategy() {
		super(Fish2Factory.Singleton.makeShape(0, 0, 1.0, 1.0));
	}

}
