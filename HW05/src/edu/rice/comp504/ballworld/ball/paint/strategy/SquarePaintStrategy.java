package edu.rice.comp504.ballworld.ball.paint.strategy;

import java.awt.Graphics;

import edu.rice.comp504.ballworld.ball.Ball;
import edu.rice.comp504.ballworld.ball.paint.IPaintStrategy;

/**
 * The strategy to paint a square directly.
 * @author Li
 *
 */
public class SquarePaintStrategy implements IPaintStrategy {

	@Override
	public void paint(Graphics g, Ball host) {
		g.setColor(host.getColor());
		g.fillRect((int) (host.getLocation().getX() - host.getRadius()), (int) (host.getLocation().getY() - host.getRadius()),
				(int)host.getRadius() * 2, (int)host.getRadius() * 2);
	}

	@Override
	public void init(Ball host) {
	}

}
