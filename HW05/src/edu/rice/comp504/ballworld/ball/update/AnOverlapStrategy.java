package edu.rice.comp504.ballworld.ball.update;

import edu.rice.comp504.ballworld.ball.Ball;
import edu.rice.comp504.ballworld.model.IBallCmd;

/**
 * An abstract strategy for all interact update 
 * strategies which need to detect overlap between
 * context ball and target ball
 * @author bb26
 *
 */

public abstract class AnOverlapStrategy implements IUpdateStrategy<IBallCmd> {
	
	/**
	 * detect if context ball overlap with target ball, if so return true, otherwise return false
	 * @param context the ball with interact strategy
	 * @param target the target ball
	 * @return 
	 */
	protected boolean isOverlaped(Ball context, Ball target){
		if (context.getLocation().distance(target.getLocation()) < (context.getRadius()+target.getRadius())) return true;
		return false;
	}

}
