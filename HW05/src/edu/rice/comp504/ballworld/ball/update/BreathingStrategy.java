package edu.rice.comp504.ballworld.ball.update;

import edu.rice.comp504.ballworld.ball.Ball;
import edu.rice.comp504.ballworld.util.IDispatcher;

/**
 * Strategy for breathing ball
 * @author Li Ding
 */
public class BreathingStrategy<Msg> implements IUpdateStrategy<Msg> {
	
	/**
	 * How much the radius should change when breathing.
	 */
	private double increment = 1;
	
	/**
	 * The radius of the ball when this strategy first runs.
	 */
	private double initialRadius = 0;
	
	/**
	 * The radius to set to for the ball using this strategy.
	 */
	private double currentRadius;

	@Override
	public void updateState(Ball context, IDispatcher<Msg> dispacher) {
		if(initialRadius == 0) {
			initialRadius = context.getRadius();
			currentRadius = initialRadius;
		}
		currentRadius = currentRadius + increment;
		context.setRadius(currentRadius);
		if(context.getRadius() >= initialRadius * 2 || 
				context.getRadius() <= initialRadius / 2) increment = 0 - increment;
	}

}
