package edu.rice.comp504.ballworld.ball.update;

import java.awt.geom.Point2D;

import edu.rice.comp504.ballworld.ball.Ball;
import edu.rice.comp504.ballworld.model.IBallCmd;
import edu.rice.comp504.ballworld.util.IDispatcher;

/**
 * The collide strategy. The ball with this strategy will detect if overlap with other 
 * balls and if so, do the elastic collision behavior based on the mass (radius^2).
 * @author bb26
 *
 */
public class CollideStrategy extends AnOverlapStrategy {
	
	/**
	 * The amount to add to the separation distance to insure that the two balls
	 * are beyond collision distance
	 */
	private double nudge = 1.1;


	@Override
	public void updateState(final Ball context, IDispatcher<IBallCmd> disp) {
		disp.dispatch(new IBallCmd() {
			@Override
			public void apply(Ball other, IDispatcher<IBallCmd> disp) {
				if (context != other) {
					if (isOverlaped(context, other)){
						Point2D imp = impulse(context, other);
						updateCollision(context, imp.getX(), imp.getY());
						updateCollision(other, -imp.getX(), -imp.getY());
					}
				}
			}
		});
	}

	/**
	 * Returns the reduced mass of the two balls (m1*m2)/(m1+m2) Gives correct
	 * result if one of the balls has infinite mass.
	 * 
	 * @param mSource
	 *            Mass of the source ball
	 * @param mTarget
	 *            Mass of the target ball
	 */
	protected double reducedMass(double mSource, double mTarget) {
		if (mSource == Double.POSITIVE_INFINITY)
			return mTarget;
		if (mTarget == Double.POSITIVE_INFINITY)
			return mSource;
		else
			return (mSource * mTarget) / (mSource + mTarget);
	}

	/**
	 * Calculates the impulse (change in momentum) of the collision in the
	 * direction from the source to the target This method calculates the
	 * impulse on the source ball. The impulse on the target ball is the
	 * negative of the result. Also moves source ball out of collision range
	 * along normal direction. The change in velocity of the source ball is the
	 * impulse divided by the source's mass The change in velocity of the target
	 * ball is the negative of the impulse divided by the target's mass
	 * 
	 * Operational note: Even though theoretically, the difference in velocities
	 * of two balls should be co-linear with the normal line between them, the
	 * discrete nature of animations means that the point where collision is
	 * detected may not be at the same point as the theoretical contact point.
	 * This method calculates the rebound directions as if the two balls were
	 * the appropriate radii such that they had just contacted
	 * _at_the_point_of_collision_detection_. This may give slightly different
	 * rebound direction than one would calculate if they contacted at the
	 * theoretical point given by their actual radii.
	 * 
	 * @param lSource
	 *            Location of the source ball
	 * @param vSource
	 *            Velocity of the source ball
	 * @param lTarget
	 *            Location of the target ball
	 * @param vTarget
	 *            Velocity of the target ball
	 * @param reducedMass
	 *            Reduced mass of the two balls
	 * @param distance
	 *            Distance between the two balls.
	 * @param deltaR
	 *            The minimum allowed separation(sum of the ball radii) minus the actual separation(distance between ball centers). Should be a
	 *            positive value.  This is the amount of overlap of the balls as measured along the line between their centers.
	 * @return
	 */
	protected Point2D.Double impulse(Ball source, Ball target) {
		// Calculate the normal vector, from source to target
		Point2D lSource = source.getLocation();
		Point2D lTarget = target.getLocation();
		double distance = lSource.distance(lTarget);
		double rSource = source.getRadius();
		double rTarget = target.getRadius();
		double deltaR = rSource + rTarget - distance;
		double reducedMass = reducedMass(Math.pow(rSource, 2), Math.pow(rTarget, 2));
		
		double nx = (lTarget.getX() - lSource.getX()) / distance;
		double ny = (lTarget.getY() - lSource.getY()) / distance;

		// delta velocity (speed, actually) in normal direction, source to
		// target
		double dvn = (target.getVelocityX() - source.getVelocityX()) * nx + (target.getVelocityY() - source.getVelocityY()) * ny;

		// move the source ball beyond collision range of the target ball, along
		// the normal direction.
		lSource.setLocation(lSource.getX() + Math.ceil(-nx * (nudge * deltaR)), lSource.getY() + Math.ceil(-ny * (nudge * deltaR)));


		return new Point2D.Double(2.0 * reducedMass * dvn * nx, 2.0
				* reducedMass * dvn * ny);
	}

	/**
	 * Updates the velocity of the source ball, given an impulse, then uses the
	 * context's interactWith method to determine the post collision behavior, from the context
	 * ball's perspective. The change in velocity is the impulse divided by the (source) ball's mass. To change
	 * the velocity of the target ball, switch the source and target input
	 * parameters and negate the impulse values.   This will also run the post collision behavior from 
	 * the other perspective.
	 * 
	 * @param context
	 *            The ball to update
	 * @param target
	 *            The ball being collided with
	 * @param impX
	 *            x-coordinate of the impulse
	 * @param impY
	 *            y-coordinate of the impulse
	 */
	protected void updateCollision(Ball context, double impX, double impY) {
		double mContext = Math.pow(context.getRadius(), 2);

		context.setVelocityX(context.getVelocityX() + Math.round(impX / mContext));
		context.setVelocityY(context.getVelocityY() + Math.round(impY / mContext));		
	}
}
