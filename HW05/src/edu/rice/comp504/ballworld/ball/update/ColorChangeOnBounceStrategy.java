package edu.rice.comp504.ballworld.ball.update;

import edu.rice.comp504.ballworld.ball.Ball;
import edu.rice.comp504.ballworld.util.IDispatcher;

/**
 * Strategy for balls that change color when bouncing
 * @author Ding Li
 */
public class ColorChangeOnBounceStrategy<Msg> implements IUpdateStrategy<Msg> {

	@Override
	public void updateState(Ball context, IDispatcher<Msg> dispacher) {
		if(context.isJustBounced()) {
			context.setColor(Ball.getRandomColor());
		}
	}

}
