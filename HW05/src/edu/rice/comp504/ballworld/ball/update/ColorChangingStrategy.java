package edu.rice.comp504.ballworld.ball.update;

import edu.rice.comp504.ballworld.ball.Ball;
import edu.rice.comp504.ballworld.util.IDispatcher;

/**
 * Strategy for balls change color periodically
 * @author Ding Li
 */
public class ColorChangingStrategy<Msg> implements IUpdateStrategy<Msg> {
	
	/**
	 * a counter that triggers ball color changing
	 */
	private int count=0;
	@Override
	public void updateState(Ball context, IDispatcher<Msg> dispacher) {
		count++;
		if (count % 20 == 0) context.setColor(Ball.getRandomColor());
	}
}
