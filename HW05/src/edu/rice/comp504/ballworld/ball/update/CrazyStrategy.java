package edu.rice.comp504.ballworld.ball.update;

import edu.rice.comp504.ballworld.ball.Ball;
import edu.rice.comp504.ballworld.util.IDispatcher;

/**
 * Strategy for crazy balls
 * @author Li
 */
public class CrazyStrategy<Msg> implements IUpdateStrategy<Msg> {
	
	private static final double SHAKE_OFFSET = 3;

	@Override
	public void updateState(Ball context, IDispatcher<Msg> dispacher) {
		context.setVelocityX(context.getVelocityX() > 0 ? Ball.getRandomSpeed() * 2 - SHAKE_OFFSET : SHAKE_OFFSET - Ball.getRandomSpeed() * 2);
		context.setVelocityY(context.getVelocityY() > 0 ? Ball.getRandomSpeed() * 2 - SHAKE_OFFSET : SHAKE_OFFSET - Ball.getRandomSpeed() * 2);
	}

}
