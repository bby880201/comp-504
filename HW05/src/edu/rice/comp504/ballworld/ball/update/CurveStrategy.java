package edu.rice.comp504.ballworld.ball.update;

import edu.rice.comp504.ballworld.ball.Ball;
import edu.rice.comp504.ballworld.util.IDispatcher;

/**
 * Strategy for curve ball
 * @author Deyu Fu
 * @author Ding Li
 */
public class CurveStrategy<Msg> implements IUpdateStrategy<Msg> {
	/**
	 * angle to turn each circle
	 */
	private static double _theta = -(Math.PI) / 16;

	@Override
	public void updateState(Ball context, IDispatcher<Msg> dispacher) {
		double sin_val = Math.sin(_theta);
		double cos_val = Math.cos(_theta);

		double xVelocityOld = context.getVelocityX();
		double yVelocityOld = context.getVelocityY();

		context.setVelocityX(Math.round((xVelocityOld * cos_val - yVelocityOld * sin_val)));
		context.setVelocityY(Math.round((yVelocityOld * cos_val + xVelocityOld * sin_val)));
	}

}
