package edu.rice.comp504.ballworld.ball.update;

import edu.rice.comp504.ballworld.ball.Ball;
import edu.rice.comp504.ballworld.model.IBallCmd;
import edu.rice.comp504.ballworld.util.IDispatcher;

/**
 * The kill strategy. The ball with this strategy will detect if overlap with other 
 * balls and if so, kill the contact ball and enlarge itself.
 * @author bb26
 *
 */

public class EatStrategy extends AnOverlapStrategy {
	/**
	 * The maximum radius of a eating ball 
	 */
	final int MAX_R = 100;

	@Override
	public void updateState(Ball context, IDispatcher<IBallCmd> disp) {
		disp.dispatch(new IBallCmd() {
			@Override
			public void apply(Ball other, IDispatcher<IBallCmd> disp) {
				if (context != other && isOverlaped(context, other)){
					disp.deleteObserver(other);
					double r = context.getRadius();
					context.setRadius(r >= MAX_R ? r : r+2 );
				}
			}
		});		
	}

}
