package edu.rice.comp504.ballworld.ball.update;

import edu.rice.comp504.ballworld.ball.Ball;
import edu.rice.comp504.ballworld.util.IDispatcher;

/**
 * Strategy to update ball state
 * @author Ding Li
 */
public interface IUpdateStrategy<Msg> {
	void updateState(Ball context, IDispatcher<Msg> disp);
}
