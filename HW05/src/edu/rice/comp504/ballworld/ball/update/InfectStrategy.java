package edu.rice.comp504.ballworld.ball.update;

import java.awt.Color;

import edu.rice.comp504.ballworld.ball.Ball;
import edu.rice.comp504.ballworld.model.IBallCmd;
import edu.rice.comp504.ballworld.util.IDispatcher;

/**
 * The collide strategy. The ball with this strategy will detect if overlap with other 
 * balls and if so, infect the target ball to a black ball for 50 updates. Afterward, 
 * the target ball will become to a straight ball.
 * @author bb26
 *
 */

public class InfectStrategy extends AnOverlapStrategy {

	@Override
	public void updateState(Ball context, IDispatcher<IBallCmd> disp) {
		disp.dispatch(new IBallCmd() {
			@Override
			public void apply(Ball other, IDispatcher<IBallCmd> disp) {
				if (other.getUpdateCount() == 0) {
					other.setUpdateStrategy(new StraightStrategy<IBallCmd>());
					other.setColor(Ball.getRandomColor());
					other.setUpdateCount(-1);
				}
				else if (other.getUpdateCount() > 0){
					other.setUpdateCount(other.getUpdateCount() - 1);
				}
				if (other != context && isOverlaped(other, context)){
					other.setColor(Color.BLACK);
					other.setUpdateCount(50);
				}
			}
		});		
	}

}
