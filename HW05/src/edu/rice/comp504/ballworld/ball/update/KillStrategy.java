package edu.rice.comp504.ballworld.ball.update;

import edu.rice.comp504.ballworld.ball.Ball;
import edu.rice.comp504.ballworld.model.IBallCmd;
import edu.rice.comp504.ballworld.util.IDispatcher;

/**
 * The kill strategy. The ball with this strategy will detect if overlap with other 
 * balls and if so, kill the contact ball.
 * @author bb26
 *
 */

public class KillStrategy extends AnOverlapStrategy {
	/**
	 * This integer will control the flash time after killing other balls
	 */
	int count = 0;
	
	@Override
	public void updateState(Ball context, IDispatcher<IBallCmd> disp) {
		disp.dispatch(new IBallCmd() {
			@Override
			public void apply(Ball other, IDispatcher<IBallCmd> disp) {
				if (0 < count--) context.setColor(Ball.getRandomColor());
				if (context != other && isOverlaped(context, other)){
					disp.deleteObserver(other);
					count = 1000;
				}
			}
		});		
	}

}
