package edu.rice.comp504.ballworld.ball.update;

import edu.rice.comp504.ballworld.ball.Ball;
import edu.rice.comp504.ballworld.util.IDispatcher;

/**
 * Strategy for composition of two strategies
 * @author Ding Li
 */
public class MultipleStrategy<Msg> implements IUpdateStrategy<Msg> {
	private IUpdateStrategy<Msg> strategyOne;
	private IUpdateStrategy<Msg> strategyTwo;
	
	public MultipleStrategy(IUpdateStrategy<Msg> strategyOne, IUpdateStrategy<Msg> strategyTwo) {
		this.strategyOne = strategyOne;
		this.strategyTwo = strategyTwo;
	}
	
	@Override
	public void updateState(Ball context, IDispatcher<Msg> dispacher) {
		strategyOne.updateState(context, dispacher);
		strategyTwo.updateState(context, dispacher);
	}

}
