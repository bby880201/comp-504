package edu.rice.comp504.ballworld.ball.update;

import java.awt.geom.Point2D;

import edu.rice.comp504.ballworld.ball.Ball;
import edu.rice.comp504.ballworld.model.IBallCmd;
import edu.rice.comp504.ballworld.util.IDispatcher;

/**
 * The collide strategy. The ball with this strategy will detect if overlap with other 
 * balls and if so, spawn a new ball on the contact location with a opposite velocity.
 * @author Li
 * @author bb26
 *
 */

public class SpawnStrategy extends AnOverlapStrategy {

	/**
	 * Count for delay.
	 */
	private int count = 0;
	/**
	 * The spawn delay in order to control speed of spawn.
	 */
	private int delay = 100;

	@Override
	public void updateState(Ball context, IDispatcher<IBallCmd> dispatcher) {
		if (delay < count++) {
			dispatcher.dispatch(new IBallCmd() {
				@Override
				public void apply(Ball other, IDispatcher<IBallCmd> disp) {
					if (count != 0 && context != other && isOverlaped(context, other)) {
						disp.addObserver(new Ball(new Point2D.Double(context.getLocation().getX(), context.getLocation().getY()),
								context.getRadius(), context.getColor(), -context.getVelocityX() + 1, -context.getVelocityY() + 1,
								new SpawnStrategy(), context.getPaintStrategy(), context.getCanvas()));
						count = 0;
						delay *= 5;
					}
				}
			});
		}
	}
}
