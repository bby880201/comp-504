package edu.rice.comp504.ballworld.ball.update;

import edu.rice.comp504.ballworld.ball.Ball;
import edu.rice.comp504.ballworld.util.IDispatcher;

/**
 * Strategy for balls go straightly and bounce on bounds
 * @author Ding Li
 */
public class StraightStrategy<Msg> implements IUpdateStrategy<Msg> {
	
	@Override
	public void updateState(Ball context, IDispatcher<Msg> dispacher) {}
	
}
