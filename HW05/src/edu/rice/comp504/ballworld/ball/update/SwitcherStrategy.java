package edu.rice.comp504.ballworld.ball.update;

import edu.rice.comp504.ballworld.ball.Ball;
import edu.rice.comp504.ballworld.util.IDispatcher;

/**
 * Changeable strategy for all switcher ball
 * @author Ding Li
 */
public class SwitcherStrategy<Msg> implements IUpdateStrategy<Msg> {
	
	private IUpdateStrategy<Msg> strategy = new StraightStrategy<Msg>();	

	@Override
	public void updateState(Ball context, IDispatcher<Msg> dispacher) {
		strategy.updateState(context, dispacher);
	}
	
	/**
	 * @param strategy the strategy to set
	 */
	public void setStrategy(IUpdateStrategy<Msg> strategy) {
		this.strategy = strategy;
	}
	

}
