package edu.rice.comp504.ballworld.controller;

import java.awt.Container;
import java.awt.EventQueue;
import java.awt.Graphics;

import edu.rice.comp504.ballworld.ball.Ball;
import edu.rice.comp504.ballworld.ball.paint.IPaintStrategyFac;
import edu.rice.comp504.ballworld.ball.update.IUpdateStrategyFac;
import edu.rice.comp504.ballworld.model.BallModel;
import edu.rice.comp504.ballworld.model.IBallCmd;
import edu.rice.comp504.ballworld.model.IModel2ViewAdapter;
import edu.rice.comp504.ballworld.view.BallGUI;
import edu.rice.comp504.ballworld.view.IView2ModelAdapter;

/**
 * Main Controller of the Ball World, which initialize and set up relationships between 
 * Model and View.
 * 
 * @author Li
 *
 */
public class MyBallWorldController {
	private static final int DELAY_IN_MS = 10;
	
	/**
	 * Model and View object.
	 */
	private BallGUI<IUpdateStrategyFac<IBallCmd>, IPaintStrategyFac> view;
	private BallModel model;

	/**
	 * Controller constructor builds the system. 
	 * Initialize model, view and adapters.
	 */
	public MyBallWorldController() {
		view = new BallGUI<IUpdateStrategyFac<IBallCmd>, IPaintStrategyFac>(new IView2ModelAdapter<IUpdateStrategyFac<IBallCmd>, IPaintStrategyFac>() {
			@Override
			public void update(Graphics g) {
				model.update(g);
			}
			
			@Override
			public void clearBalls() {
				model.clearBalls();
			}

			@Override
			public IUpdateStrategyFac<IBallCmd> addUpdateStrategy(String classname) {
				return model.makeUpdateStrategyFac(classname);
			}

			@Override
			public void makeBall(IUpdateStrategyFac<IBallCmd> updateFactory, IPaintStrategyFac paintFactory, Container canvas) {
				model.addBall(new Ball(updateFactory.make(), paintFactory.make(), canvas));
			}

			@Override
			public IUpdateStrategyFac<IBallCmd> combineUpdateStrategies(IUpdateStrategyFac<IBallCmd> stratFacOne,
					IUpdateStrategyFac<IBallCmd> stratFacTwo) {
				return model.combineUpdateStrategyFacs(stratFacOne, stratFacTwo);
			}

			@Override
			public void makeSwitcherBall(IPaintStrategyFac paintFactory, Container canvas) {
				model.addBall(new Ball(model.getSwitcherStrategy(), paintFactory.make(), canvas));
				
			}

			@Override
			public void switchSwitcherBall(IUpdateStrategyFac<IBallCmd> selectItem) {
				model.switchSwitcherStrategy(selectItem.make());
			}

			@Override
			public IPaintStrategyFac addPaintStrategy(String classname) {
				return model.makePaintStrategyFac(classname);
			}
		});

		model = new BallModel(DELAY_IN_MS, new IModel2ViewAdapter() {
			@Override
			public void update() {
				view.update();
			}
		});
	}

	/**
	 * Start both view and model.
	 */
	void startTheWorld() {
		model.start();
		view.start();
	}

	/**
	 * Launch the application.
	 * @param args Arguments given by the system or command line.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MyBallWorldController controller = new MyBallWorldController();
					controller.startTheWorld();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
