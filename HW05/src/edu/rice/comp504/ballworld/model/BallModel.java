package edu.rice.comp504.ballworld.model;

import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;

import edu.rice.comp504.ballworld.ball.Ball;
import edu.rice.comp504.ballworld.ball.paint.IPaintStrategy;
import edu.rice.comp504.ballworld.ball.paint.IPaintStrategyFac;
import edu.rice.comp504.ballworld.ball.paint.strategy.BallPaintStrategy;
import edu.rice.comp504.ballworld.ball.update.IUpdateStrategyFac;
import edu.rice.comp504.ballworld.ball.update.IUpdateStrategy;
import edu.rice.comp504.ballworld.ball.update.MultipleStrategy;
import edu.rice.comp504.ballworld.ball.update.SwitcherStrategy;
import edu.rice.comp504.ballworld.util.IDispatcher;
import edu.rice.comp504.ballworld.util.SetDispatcherSequential;

/**
 * Model Class of Ball World. It notifies view to update on timer change and
 * dispatch to all balls when getting information from view.
 * 
 * @author Li
 */
public class BallModel {

	/**
	 * Timer for the system to refresh.
	 */
	private Timer timer;

	/**
	 * The model to view adapter, which specifies how the model should talk to
	 * view.
	 */
	private IModel2ViewAdapter model2ViewAdapter = IModel2ViewAdapter.NULL_OBJECT;

	/**
	 * The dispatcher object that dispatch actions to all balls when getting
	 * information from view.
	 */
	private IDispatcher<IBallCmd> dispatcher = new SetDispatcherSequential<IBallCmd>();
	
	/**
	 * The singleton switch strategy of the model.
	 */
	private static final SwitcherStrategy<IBallCmd> switcherStrategy = new SwitcherStrategy<IBallCmd>();

    /**
     * A factory for a beeping error strategy that beeps the speaker every 25 updates.
     * Either use the _errorStrategyFac variable directly if you need a factory that makes an error strategy,
     * or call _errorStrategyFac.make() to create an instance of a beeping error strategy.
     */
    private IUpdateStrategyFac<IBallCmd> errorPaintStrategyFac = new IUpdateStrategyFac<IBallCmd>() {
        @Override
        /**
         * Make the beeping error strategy
         * @return  An instance of a beeping error strategy
         */
        public IUpdateStrategy<IBallCmd> make() {
            return new IUpdateStrategy<IBallCmd>() {
                private int count = 0; // update counter
                @Override
                /**
                 * Beep the speaker every 25 updates
                 */
                public void updateState(Ball context, IDispatcher<IBallCmd> disp) {
                    if(25 < count++){
                        java.awt.Toolkit.getDefaultToolkit().beep(); 
                        count = 0;
                    }
                }
            };
        }
    };
    
	/**
	 * Model Constructor.
	 * 
	 * @param delay
	 *            Milliseconds for the initial and between-event delay.
	 * @param adapter
	 *            Model to view adapter object.
	 */
	public BallModel(int delay, IModel2ViewAdapter adapter) {
		model2ViewAdapter = adapter;
		this.timer = new Timer(delay, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				model2ViewAdapter.update();
			}
		});
	}
	
	/**
	 * Returns an IPaintStrategyFac that can instantiate the strategy specified by
	 * class name.
	 *
	 * @param classname Name of desired strategy
	 * @return Factory to make strategy
	 */
	public IPaintStrategyFac makePaintStrategyFac(final String classname) {
		if (null == classname || classname.length() == 0 || loadPaintStrategy(getFullPaintName(classname)) == null)
			return null;
		return new IPaintStrategyFac() {
			@Override
			public IPaintStrategy make() {
				return loadPaintStrategy(getFullPaintName(classname));
			}
			
			@Override
			public String toString() {
				return classname;
			}
		};
	}

	/**
	 * Returns an IStrategyFac that can instantiate the strategy specified by
	 * class name.
	 *
	 * @param classname Name of desired strategy
	 * @return Factory to make strategy
	 */
	public IUpdateStrategyFac<IBallCmd> makeUpdateStrategyFac(final String classname) {
		if (null == classname || classname.length() == 0 || loadUpdateStrategy(getFullUpdateName(classname)) == null)
			return null;
		return new IUpdateStrategyFac<IBallCmd>() {
			@Override
			public IUpdateStrategy<IBallCmd> make() {
				return loadUpdateStrategy(getFullUpdateName(classname));
			}
			
			@Override
			public String toString() {
				return classname;
			}
		};
	}
	
	/**
	 * A method to get IStrategyFac that can instantiate a MultiStrategy with the two
	 * strategies made by the two given IStrategyFac objects.
	 *
	 * @param stratFacOne An IStrategyFac for a strategy
	 * @param stratFacTwo An IStrategyFac for a strategy
	 * @return An IStrategyFac for the composition of the two strategies
	 */
	public IUpdateStrategyFac<IBallCmd> combineUpdateStrategyFacs(final IUpdateStrategyFac<IBallCmd> stratFacOne, final IUpdateStrategyFac<IBallCmd> stratFacTwo) {
	    if (null == stratFacOne || null == stratFacTwo) return null;
	    return new IUpdateStrategyFac<IBallCmd>() {
	        @Override
	        public IUpdateStrategy<IBallCmd> make() {
	            return new MultipleStrategy<IBallCmd>(stratFacOne.make(), stratFacTwo.make());
	        }
	        
	        @Override
	        public String toString() {
	            return stratFacOne.toString() + "-" + stratFacTwo.toString();
	        }
	    };
	}

	/**
	 * Get a strategy object given the class name.
	 * @param className The name of the class
	 * @return An update strategy object
	 */
	@SuppressWarnings("unchecked")
	private IUpdateStrategy<IBallCmd> loadUpdateStrategy(String className) {
		try {
			java.lang.reflect.Constructor<?> cs[] = 
					Class.forName(className).getConstructors(); //reflection
			for (int i = 0; i < cs.length; i++) { 
				if (0 == (cs[i]).getParameterTypes().length) {
					return (IUpdateStrategy<IBallCmd>)cs[i].newInstance();
				}
			}
		} catch (Exception ex) {
			System.err.println("Class " + className
					+ " failed to load. \nException = \n" + ex);
			ex.printStackTrace();
		}
		return errorPaintStrategyFac.make();
	}
	
	/**
	 * Get a strategy object given the class name.
	 * @param className The name of the class
	 * @return A paint strategy object
	 */
	private IPaintStrategy loadPaintStrategy(String className) {
		try {
			java.lang.reflect.Constructor<?> cs[] = 
					Class.forName(className).getConstructors(); //reflection
			for (int i = 0; i < cs.length; i++) { 
				if (0 == (cs[i]).getParameterTypes().length) {
					return (IPaintStrategy)cs[i].newInstance();
				}
			}
		} catch (Exception ex) {
			System.err.println("Class " + className
					+ " failed to load. \nException = \n" + ex);
			ex.printStackTrace();
		}
		return new BallPaintStrategy();
	}

	/**
	 * Clears all the balls in the model.
	 */
	public void clearBalls() {
		dispatcher.deleteObservers();
	}

	/**
	 * Add a ball to the model
	 * 
	 * @param ball
	 *            The ball object that needs to add.
	 */
	public void addBall(Ball ball) {
		dispatcher.addObserver(ball);
	}

	/**
	 * Update the state of all balls on screen.
	 * 
	 * @param g
	 *            The GUI component which all balls paint on.
	 */
	public void update(Graphics g) {
		dispatcher.dispatch(new IBallCmd() {
			@Override
			public void apply(Ball context, IDispatcher<IBallCmd> disp) {
				context.move();
				context.bounce();
				context.paint(g);
				context.updateState(disp);
			}
		});
	}

	/**
	 * Start the model.
	 */
	public void start() {
		timer.start();
	}

	/**
	 * Stop the model.
	 */
	public void stop() {
		timer.stop();
	}
	/**
	 * 
	 * @return the current switcher Strategy
	 */
	public SwitcherStrategy<IBallCmd> getSwitcherStrategy() {
		return switcherStrategy;
	}
	/**
	 * set switcher strategy
	 * @param strategy new strategy to set to
	 */
	public void switchSwitcherStrategy(IUpdateStrategy<IBallCmd> strategy) {
		switcherStrategy.setStrategy(strategy);
	}

	/**
	 * Convert the simple class name to full class name of update strategy
	 * @param className Simple class name of update strategy
	 * @return Full class name of the update strategy
	 */
	private String getFullUpdateName(String className) {
		return "edu.rice.comp504.ballworld.ball.update." + className + "Strategy";
	}
	
	/**
	 * Convert the simple class name to full class name of paint strategy
	 * @param className Simple class name of paint strategy
	 * @return Full class name of the paint strategy
	 */
	private String getFullPaintName(String className) {
		return "edu.rice.comp504.ballworld.ball.paint.strategy." + className + "PaintStrategy";
	}
}
