package edu.rice.comp504.ballworld.model;

import java.util.Observable;

/**
 * The Dispatcher that inform all the objects that listen to it on state change.
 * @author Li
 *
 */
public class Dispatcher extends Observable {

	/**
	 * Notify all registered listeners when there is state change.
	 * @param param Context that need to be passed to listeners.
	 */
	public void notifyAll(Object param) {
		setChanged();
		notifyObservers(param);
	}
}
