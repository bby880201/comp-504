package edu.rice.comp504.ballworld.model;

/**
 * Model to View Adapter. It specifics connection from the model to the view.
 * @author Li
 */
public interface IModel2ViewAdapter {
	
	/**
	 * Inform the view to update it's graph.
	 */
	public void update();
	
	/**
	 * default no-op object.
	 */
	public static final IModel2ViewAdapter NULL_OBJECT = new IModel2ViewAdapter() {
		public void update() {}
	};
}
