package edu.rice.comp504.ballworld.util;

import java.util.Collection;

/**
 * An abstract Collection-based IDispatcher.
 *
 * @author Stephen Wong
 * @author Derek Peirce
 *
 * @param <Msg> The type of message sent to the registered IObservers
 */

public abstract class ACollectionDispatcher<Msg> implements IDispatcher<Msg> {
	
	    private final Collection<IObserver<Msg>> observers; 
	     
	    /**
	     * Constructor for the class.   The Collection that is used needs to be supplied,
	     * generally by the implementing subclass.   This allows for different types of
	     * Collections to be used for different purposes.  It is highly recommended that the
	     * supplied Collection be completely thread-safe to enable the use of
	     * multiple dispatching threads.
	     * @param observers  The Collection of IObserver<Msg> to use.
	     */
	    public ACollectionDispatcher(Collection<IObserver<Msg>> observers) {
	        this.observers = observers;
	    }
	    /**
	     * Accessory method for the internal Collection for use by implementing subclasses.
	     * @return The internal Collection of IObservers<Msg>
	     */
	    protected Collection<IObserver<Msg>> getCollection() {
	        return observers;
	    }
	    /**
	     * {@inheritDoc}<br/>
	     * Implementation: Add the given observer to the internal Collection.
	     */
	    @Override
	    public void addObserver(IObserver<Msg> obs) {
	        observers.add(obs);
	    }
	    /**
	     * {@inheritDoc}<br/>
	     * Implementation: Delete the given observer from the internal Collection.
	     */
	    @Override
	    public void deleteObserver(IObserver<Msg> obs) {
	        observers.remove(obs);
	    }
	    /**
	     * {@inheritDoc}<br/>
	     * Implementation: Delete all the observers from the internal Collection.
	     */
	    @Override
	    public void deleteObservers() {
	        observers.clear();
	    }
}
