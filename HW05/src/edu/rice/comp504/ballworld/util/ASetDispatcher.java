package edu.rice.comp504.ballworld.util;

import java.util.concurrent.CopyOnWriteArraySet;

/**
 * A Collection-based Dispatcher that uses a CopyOnWriteArraySet.
 *
 * @author Stephen Wong
 * @author Derek Peirce
 *
 * @param <Msg> The type of message sent to the registered IObservers
 */

public abstract class ASetDispatcher<Msg> extends ACollectionDispatcher<Msg> {
	/**
	     * The constructor for the class that supplies a CopyOnWriteArraySet instance to the superclass constructor.
	     */
	    public ASetDispatcher() {
	        super(new CopyOnWriteArraySet<>());
	    }


}
