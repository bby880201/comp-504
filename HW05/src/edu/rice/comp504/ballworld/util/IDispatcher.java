package edu.rice.comp504.ballworld.util;

public interface IDispatcher<Msg> {
	public void dispatch(Msg msg);
	public void addObserver(IObserver<Msg> obs);
	public void deleteObserver(IObserver<Msg> obs);
	public void deleteObservers();

}
