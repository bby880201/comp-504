package edu.rice.comp504.ballworld.util;

public interface IObserver<Msg> {
	public void update(IDispatcher<Msg> dispatcher, Msg msg);
}
