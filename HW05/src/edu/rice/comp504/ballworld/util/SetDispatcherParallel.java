package edu.rice.comp504.ballworld.util;

/**
 * A CopyOnWriteArraySet-based IDispatcher that dispatches to its IObservers in parallel.
 *
 * @author Stephen Wong
 * @author Derek Peirce
 *
 * @param <Msg> The type of message sent to the registered IObservers
 */

public class SetDispatcherParallel<Msg> extends ASetDispatcher<Msg> {
	/**
	     * {@inheritDoc}<br/>
	     * Implementation: Attempts to perform parallel dispatching of the message to the collection of IObservers. 
	     * Note that parallel execution is not guaranteed.
	     */
	    @Override
	    public void dispatch(Msg msg) {
	        getCollection().parallelStream().forEach(o -> {
	            o.update(this, msg);
	        });
	    }
}
