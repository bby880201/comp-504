package edu.rice.comp504.ballworld.util;

public class SetDispatcherSequential<Msg> extends ASetDispatcher<Msg> {
	
	/**
	     * {@inheritDoc}<br/>
	     * Implementation: Sequential iteration through the collection of IObservers.
	     */
	    @Override
	    public void dispatch(Msg msg) {
	        getCollection().forEach(o -> {
	            o.update(this, msg);
	        });
	    }
}
