package edu.rice.comp504.ballworld.view;

import java.awt.BorderLayout;
import java.awt.Graphics;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Color;

import javax.swing.JLabel;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JTextField;
import javax.swing.BoxLayout;
import java.awt.Component;

/**
 * View Class of the Ball World. It contains all GUI components.
 * @author Li
 */
public class BallGUI<TDropListItem1, TDropListItem2> extends JFrame {

	/**
	 * Auto-generated fields for main frame
	 */
	private static final long serialVersionUID = -4255830396470732847L;
	private JPanel contentPane;
	private JPanel pnlDisplay;
	private JPanel pnlControl;
	private JTextField txtInfoInput;
	private JButton btnClearBall;
	
	/**
	 * The view to model adapter, which specifies how this view component should interact with model.
	 */
	@SuppressWarnings("unchecked")
	private IView2ModelAdapter<TDropListItem1, TDropListItem2> view2ModelAdapter = IView2ModelAdapter.NULL_OBJECT;
	
	/**
	     * The top drop list, used to select what strategy to use in a new ball and
	     * to switch the switcher to.
	     */
	    private JComboBox<TDropListItem1> cboxFirst;
	    /**
	     * Bottom drop list, used for combining with the top list selection.
	     */
	    private JComboBox<TDropListItem1> cboxSecond;
	    
	    private JPanel pnlTextInput;
	    private JPanel pnlCombination;
	    private JButton btnAddStrategy;
	    private JButton btnCombineStrategy;
	    private JPanel pnlSwitchAndClear;
	    private JButton btnMkSwitcherBall;
	    private JButton btnSwitchStrategy;
	    private JPanel pnlPaint;
	    private JTextField txtpaint;
	    private JComboBox<TDropListItem2> cboxPaint;
	    private JButton btnAddPaint;



	/**
	 * Constructor of the frame with no adapter, 
	 * which can not immediately interact with model when constructed.
	 */
	public BallGUI() {
		initGUI();
	}
	
	/**
	 * Constructor of the frame with a view to model adapt, which
	 * makes it being able to interact with Model with behaviors specified by adapter.
	 * @param adapter The view to model adapter.
	 */
	public BallGUI(IView2ModelAdapter<TDropListItem1, TDropListItem2> adapter) {
		this.view2ModelAdapter = adapter;
		initGUI();
	}
	
	/**
	 * update the graphic display.
	 */
	public void update() {
		pnlDisplay.repaint();
	}
	
	/**
	 * Initialize GUI components but do not start the frame.
	 */
	private void initGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(50, 50, 1200, 660);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		pnlDisplay = new JPanel() {
			
			private static final long serialVersionUID = 8672209849103068554L;

			/**
			* Overridden paintComponent method to paint a shape in the panel.
			* @param g The Graphics object to paint on.
			**/
			public void paintComponent(Graphics g) {
			    super.paintComponent(g);   // Do everything normally done first, e.g. clear the screen.
			    view2ModelAdapter.update(g);
			}
		};
		pnlDisplay.setToolTipText("Graph Display");
		pnlDisplay.setBackground(Color.WHITE);
		contentPane.add(pnlDisplay, BorderLayout.CENTER);
		
		pnlControl = new JPanel();
		pnlControl.setToolTipText("Information Board ");
		pnlControl.setBackground(new Color(Integer.valueOf("4169aa", 16)));
		contentPane.add(pnlControl, BorderLayout.NORTH);
		
		pnlTextInput = new JPanel();
		pnlTextInput.setBorder(null);
		pnlControl.add(pnlTextInput);
		pnlTextInput.setBackground(new Color(Integer.valueOf("4169aa", 16)));
		pnlTextInput.setLayout(new BoxLayout(pnlTextInput, BoxLayout.Y_AXIS));
		
		JLabel lblInfo = new JLabel("Type in strategy here");
		lblInfo.setAlignmentX(Component.CENTER_ALIGNMENT);
		lblInfo.setToolTipText("Showing the information.");
		lblInfo.setForeground(Color.WHITE);
		pnlTextInput.add(lblInfo);
		
		txtInfoInput = new JTextField();
		txtInfoInput.setToolTipText("Type in name of the strategy.");
		pnlTextInput.add(txtInfoInput);
		txtInfoInput.setColumns(10);
		
		btnAddStrategy = new JButton("Add Strategy");
		btnAddStrategy.setToolTipText("Click to add input strategy.");
		btnAddStrategy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TDropListItem1 item = view2ModelAdapter.addUpdateStrategy(txtInfoInput.getText());
				if(item != null) {
					cboxFirst.addItem(item);
					cboxSecond.addItem(item);
				}
			}
		});
		btnAddStrategy.setAlignmentX(Component.CENTER_ALIGNMENT);
		pnlTextInput.add(btnAddStrategy);
		
		pnlCombination = new JPanel();
		pnlCombination.setBorder(null);
		pnlControl.add(pnlCombination);
		pnlCombination.setBackground(new Color(Integer.valueOf("4169aa", 16)));
		pnlCombination.setLayout(new BoxLayout(pnlCombination, BoxLayout.Y_AXIS));
		
		JButton btnAddBall = new JButton("Make Ball");
		btnAddBall.setAlignmentX(Component.CENTER_ALIGNMENT);
		pnlCombination.add(btnAddBall);
		cboxFirst = new JComboBox<TDropListItem1>();
		cboxFirst.setToolTipText("First Strategy ");
		pnlCombination.add(cboxFirst);
		
		btnAddBall.setToolTipText("Click to add a ball of strategy in the first list.");
		btnAddBall.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				view2ModelAdapter.makeBall(
						cboxFirst.getItemAt(cboxFirst.getSelectedIndex()), 
						cboxPaint.getItemAt(cboxPaint.getSelectedIndex()), pnlDisplay);
			}
		});
		cboxSecond = new JComboBox<TDropListItem1>();
		cboxSecond.setToolTipText("Second strategy");
		pnlCombination.add(cboxSecond);
		
		btnCombineStrategy = new JButton("Combine Strategy");
		btnCombineStrategy.setToolTipText("Make a new strategy which combines both two strategies in two lists.");
		btnCombineStrategy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TDropListItem1 item = 
						view2ModelAdapter.combineUpdateStrategies(
								cboxFirst.getItemAt(cboxFirst.getSelectedIndex()), cboxSecond.getItemAt(cboxSecond.getSelectedIndex()));
				cboxFirst.addItem(item);
				cboxSecond.addItem(item);
			}
		});
		btnCombineStrategy.setAlignmentX(Component.CENTER_ALIGNMENT);
		pnlCombination.add(btnCombineStrategy);
		
		pnlSwitchAndClear = new JPanel();
		pnlSwitchAndClear.setBorder(null);
		pnlControl.add(pnlSwitchAndClear);
		pnlSwitchAndClear.setBackground(new Color(Integer.valueOf("4169aa", 16)));
		pnlSwitchAndClear.setLayout(new BoxLayout(pnlSwitchAndClear, BoxLayout.Y_AXIS));
		
		btnMkSwitcherBall = new JButton("Make Switcher Ball");
		btnMkSwitcherBall.setToolTipText("Click to create a ball that can switch it's strategy.");
		btnMkSwitcherBall.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				view2ModelAdapter.makeSwitcherBall(cboxPaint.getItemAt(cboxPaint.getSelectedIndex()), pnlDisplay);
			}
		});
		btnMkSwitcherBall.setAlignmentX(Component.CENTER_ALIGNMENT);
		pnlSwitchAndClear.add(btnMkSwitcherBall);
		
		btnSwitchStrategy = new JButton("Switch Strategy");
		btnSwitchStrategy.setToolTipText("Switch all Switcher Ball's strategy to the strategy in first list.");
		btnSwitchStrategy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				view2ModelAdapter.switchSwitcherBall(cboxFirst.getItemAt(cboxFirst.getSelectedIndex()));
			}
		});
		btnSwitchStrategy.setAlignmentX(Component.CENTER_ALIGNMENT);
		pnlSwitchAndClear.add(btnSwitchStrategy);
		
		btnClearBall = new JButton("Clear Ball");
		btnClearBall.setAlignmentX(Component.CENTER_ALIGNMENT);
		btnClearBall.setToolTipText("Click to clear all balls");
		btnClearBall.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				view2ModelAdapter.clearBalls();
			}
		});
		
		pnlSwitchAndClear.add(btnClearBall);
		
		pnlPaint = new JPanel();
		pnlPaint.setBorder(null);
		pnlControl.add(pnlPaint);
		pnlPaint.setBackground(new Color(Integer.valueOf("4169aa", 16)));
		pnlPaint.setLayout(new BoxLayout(pnlPaint, BoxLayout.Y_AXIS));
		
		txtpaint = new JTextField();
		txtpaint.setToolTipText("Type in the name of paint strategy");
		pnlPaint.add(txtpaint);
		txtpaint.setColumns(10);
		
		cboxPaint = new JComboBox<TDropListItem2>();
		cboxPaint.setToolTipText("Select a paint strategy");
		pnlPaint.add(cboxPaint);
		
		btnAddPaint = new JButton("Add Paint");
		btnAddPaint.setToolTipText("Click to choose the paint strategy in the list");
		btnAddPaint.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TDropListItem2 item = view2ModelAdapter.addPaintStrategy(txtpaint.getText());
				if(item != null) {
					cboxPaint.addItem(item);
				}
			}
		});
		pnlPaint.add(btnAddPaint);
		
	}
	
	/**
	 * Start the initialized frame. Making it visible 
	 * and ready to interact with user. 
	 */
	public void start() {
		setVisible(true);
	}

}
