package edu.rice.comp504.ballworld.view;

import java.awt.Container;
import java.awt.Graphics;

/**
 * View to Model Adapter. It specifics connection from view to the model.
 * 
 * @author Li
 */
public interface IView2ModelAdapter<TDropListItem1, TDropListItem2> {

	/**
	 * Inform the model to update it's property.
	 * 
	 * @param g
	 *            The Graphics object to paint on.
	 */
	public void update(Graphics g);

	/**
	 * Inform the model to dump all balls currently exit.
	 */
	public void clearBalls();

	/**
	 * Take the given short strategy name and return a corresponding something
	 * to put onto both drop lists.
	 * 
	 * @param classname
	 *            The shortened class name of the desired strategy
	 * @return Something to put onto both the drop lists.
	 */
	public TDropListItem1 addUpdateStrategy(String classname);
	
	/**
	 * Take the given short strategy name and return a corresponding something
	 * to put onto both drop lists.
	 * 
	 * @param classname
	 *            The shortened class name of the desired strategy
	 * @return Something to put onto the drop lists.
	 */
	public TDropListItem2 addPaintStrategy(String classname);

	/**
	 * Make a ball with the selected items.
	 * 
	 * @param selectedItem1 one selected item
	 * @param selectedItem2 another selected item
	 * @param canvas The canvas of the ball being painted on.
	 */
	public void makeBall(TDropListItem1 selectedItem1, TDropListItem2 selectedItem2, Container canvas);

	/**
	 * Return a new object to put on both lists, given two items from the lists.
	 * 
	 * @param selectedItem1
	 *            An object from one drop list
	 * @param selectedItem2
	 *            An object from the other drop list
	 * @return An object to put back on both lists.
	 */
	public TDropListItem1 combineUpdateStrategies(TDropListItem1 selectedItem1,
			TDropListItem1 selectedItem2);
	
	/**
	 * Make a switch strategy ball given the input and canvas
	 * @param selectedItem The selected item which decide the ball's strategy.
	 * @param canvas The container object which represent the canvas to paint on.
	 */
	public void makeSwitcherBall(TDropListItem2 selectedItem, Container canvas);
	
	/**
	 * Switch all switcher ball's strategy
	 * @param selectItem The selected object which determines the switcher ball's strategy
	 */
	public void switchSwitcherBall(TDropListItem1 selectItem);

	/**
	 * default no-op object.
	 */
	@SuppressWarnings("rawtypes")
	public static final IView2ModelAdapter NULL_OBJECT = new IView2ModelAdapter() {
		public void update(Graphics g) {
		}

		public void clearBalls() {
		}

		public Object addUpdateStrategy(String classname) {
			return null;
		}

		public void makeBall(Object selectedItem1, Object selectedItem2, Container canvas) {
		}

		public Object combineUpdateStrategies(Object selectedItem1,
				Object selectedItem2) {
			return null;
		}

		public void makeSwitcherBall(Object selectedItem, Container canvas) {
		}

		public void switchSwitcherBall(Object selectItem) {
		}

		public Object addPaintStrategy(String classname) {
			return null;
		}
	};
}
