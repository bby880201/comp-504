/**
 * 
 */
package client.model.taskfactory;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.rmi.RemoteException;

import provided.compute.ILocalTaskViewAdapter;
import provided.compute.ITask;
import client.model.exception.ParameterInvalid;

/**
 * @author Boyang Bai
 *
 */
public class GetInfoFac implements ITaskFac {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5893674661048049047L;

	/* (non-Javadoc)
	 * @see client.model.taskfactory.ITaskFac#make(java.lang.String)
	 */
	@Override
	public ITask<?> make(final String para) throws ParameterInvalid {
		
		return new ITask<String>(){
			/**
			 * 
			 */
			private static final long serialVersionUID = 6467762572738042739L;

			/**
		     * Adapter to the local view.  Marked "transient" so that it is not serialized
		     * and instead is reattached at the destination (the server).  
		     */
			private transient ILocalTaskViewAdapter taskView = ILocalTaskViewAdapter.DEFAULT_ADAPTER;

			/**
			 * Input string given to the constructor
			 */
			private String input = "";

			/**
			 * An array carried along to prove that internal data is transmitted too.
			 */
			private double[] dArray = new double[] { 1.2, 2.3, 4.5 };

			/**
			 * Constructor for the class.
			 * 
			 * @param input  A string to carry around.
			 */
			{
				this.input = para;
			}

			/**
			 * Display the internal string on the server's console.
			 * Get the server's system properties and calculate the sum of the internal data.  
			 * @return the server's system properties and the sum of the internal array as a string.
			 */
			@Override
			public String execute() throws RemoteException {
				taskView.append("GetInfo task called with input = " + input + "\n");
				double sum = 0.0;
				for (double x : dArray)
					sum += x;
				return System.getProperties().toString() + "\n Sum = " + sum;
			}

			/**
			 * Reinitializes transient fields upon deserialization. See the <a href=
			 * "http://download.oracle.com/javase/6/docs/api/java/io/Serializable.html">
			 * Serializable</a> marker interface docs.
			 * taskView is set to a default value for now (ILocalTaskViewAdapter.DEFAULT_ADAPTER).
			 * 
			 * @param stream
			 *            The object stream with the serialized data
			 */
			private void readObject(ObjectInputStream stream) throws IOException,
					ClassNotFoundException {
				stream.defaultReadObject(); // Deserialize the non-transient data
				taskView = ILocalTaskViewAdapter.DEFAULT_ADAPTER; // re-initialize the
																	// transient field
			}

			/**
			 * Sets the task view adapter to a new value. Tests connection by sending a
			 * string representation of the dArray structure.  Called by the server to 
			 * attach the task to its view.
			 * 
			 * @param viewAdapter
			 *            the adapter to the view.
			 */
			@Override
			public void setTaskViewAdapter(ILocalTaskViewAdapter viewAdapter) {
				this.taskView = viewAdapter;
				viewAdapter.append("GetInfo installed!\n");
				String s = "";
				for (double x : dArray)
					s += x + " ";
				System.out.println("GetInfo.setTaskViewAdapter called.\ndArray = " + s
						+ "\n");
			}
			
			public String toString(){
				return "GetInfo task with parameter " + para;
			}
		};		
	}

	/* (non-Javadoc)
	 * @see client.model.taskfactory.ITaskFac#getTaskType()
	 */
	@Override
	public String getTaskType() {
		return "GetInfo";
	}

}
