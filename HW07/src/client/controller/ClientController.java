package client.controller;

import client.model.*;
import client.view.*;

/**
 * Controller for the Client MVC system.
 * @author swong, Boyang Bai
 *
 */
public class ClientController {

	/**
	 * The view of the MVC
	 */
	private ClientGUI view;

	/**
	 * The model of the MVC
	 */
	private ClientModel model;

	/**
	 * Constructor of the class.   Instantiates and connects the model and the view.
	 */
	public ClientController() {

		model = new ClientModel(new IViewAdapter() {
			/**
			 * Send the string to the view
			 * @param s The string to display in the view
			 */
			@Override
			public void append(String s) {
				view.append(s);
			}

			/**
			 * Sets the displayed remote host on the view
			 * @param hostAddress The address of the host to display
			 */
			@Override
			public void setRemoteHost(String hostAddress) {
				view.setRemoteHost(hostAddress);
			}
		});

		view = new ClientGUI(new IModelAdapter() {
			/**
			 * Connect to the given remote host
			 * @param the remote host to connect to.
			 */
			@Override
			public String connectTo(String remoteHost) {
				return model.connectTo(remoteHost);
			}

			/**
			 * Quits the current connection and closes the application.   
			 * Causes the model to stop and thus end the application. 
			 */
			@Override
			public void quit() {
				model.stop();
			}

			/**
			 * Let model load corresponding task factory
			 * @param the task type
			 */
			@Override
			public void loadTask(String text) {
				model.loadTask(text);
			}

			/**
			 * Pass the parameters to model's task factory to generate a task 
			 * and then execute it
			 * @param parameters that task needed to execute
			 */
			@Override
			public String execute(String pars) {
				return model.execute(pars);
			}
		});

	}

	/**
	 * Starts the view then the model.  The view needs to be started first so that it can display 
	 * the model status updates as it starts.
	 */
	public void start() {
		view.start();
		model.start();
	}

	/**
	 * Main() method of the client application. Instantiates and then starts the controller.
	 * @param args ignored
	 */
	public static void main(String[] args) {
		(new ClientController()).start();
	}
}
