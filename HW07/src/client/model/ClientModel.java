package client.model;

import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import client.model.exception.ParameterInvalid;
import client.model.taskfactory.ITaskFac;
import provided.compute.*;
import provided.rmiUtils.*;
import provided.util.IVoidLambda;

/**
 * The model of the client system.
 * 
 * @author swong, Boyang Bai
 * 
 */
public class ClientModel {

	/**
	 * output command used to put multiple strings up onto the view.
	 */
	private IVoidLambda<String> outputCmd = new IVoidLambda<String>() {

		@Override
		public void apply(String... params) {
			for (String s : params) {
				view.append(s + "\n");
			}
		}
	};

	/**
	 * Factory for the Registry and other uses.
	 */
	IRMIUtils rmiUtils = new RMIUtils(outputCmd);

	/**
	 * Adapter to the view
	 */
	private IViewAdapter view;

	/**
	 * A reference to the proxy stub of the remote ICompute object.
	 */
	private ICompute comp;

	/**
	 * The adapter that connects all the way back to the remote system's view
	 * enabling this client to append messages to the server's view.
	 */
	private IRemoteTaskViewAdapter serverTA;

	/**
	 * The view adapter that the server can use to append messages to this
	 * client's view.
	 */
	private IRemoteTaskViewAdapter clientTA = new IRemoteTaskViewAdapter() {
		public void append(String s) {
			view.append("[Server] " + s);
		}
	};

	/**
	 * RMI stub for clientTA.  null=> clientTA needs to be exported, do not re-export otherwise. 
	 */
	private IRemoteTaskViewAdapter clientTAstub = null;

	private ITaskFac taskFac = ITaskFac.NULL_OBJECT;

	/**
	 * Constructor for the class
	 * 
	 * @param view
	 *            The adapter to the view.
	 */
	public ClientModel(IViewAdapter view) {
		this.view = view;
	}

	/**
	 * Starts the model by setting all the required RMI system properties,
	 * starts up the class server and installs the security manager.
	 */
	public void start() {
		rmiUtils.startRMI(IRMI_Defs.CLASS_SERVER_PORT_CLIENT);

		try {
			view.setRemoteHost(rmiUtils.getLocalAddress()); //TODO Is this stored somewhere?
		} catch (Exception e) {
			System.err.println("Error getting local address: " + e);
		}
	}

	/**
	 * Stops the client by shutting down the class server.
	 */
	public void stop() {
		System.out.println("ClientModel.stop(): client is quitting.");
		try {
			rmiUtils.stopRMI();

		} catch (Exception e) {
			System.err
					.println("ClientModel.stop(): Error stopping class server: "
							+ e);
		}
		System.exit(0);
	}

	/**
	 * Connects to the given remote host and retrieves the stub to the ICompute object bound 
	 * to the ICompute.BOUND_NAME name in the remote Registry on port 
	 * IRMI_Defs.REGISTRY_PORT.  
	 * 
	 * @param remoteHost The IP address or host name of the remote server.
	 * @return  A status string on the connection.
	 */
	public String connectTo(String remoteHost) {
		try {
			view.append("Locating registry at " + remoteHost + "...\n");
			//Registry registry = registryFac.getRemoteRegistry(remoteHost);
			Registry registry = rmiUtils.getRemoteRegistry(remoteHost);
			view.append("Found registry: " + registry + "\n");
			comp = (ICompute) registry.lookup(ICompute.BOUND_NAME);
			view.append("Found remote Compute object: " + comp + "\n");
			if (null == clientTAstub) { // Don't re-export clientTA if already done.
				clientTAstub = (IRemoteTaskViewAdapter) UnicastRemoteObject
						.exportObject(clientTA,
								IRemoteTaskViewAdapter.BOUND_PORT_CLIENT);
			}
			serverTA = comp.setTextAdapter(clientTAstub);
			view.append("Got text adapter: " + serverTA + "\n");
			serverTA.append("Hello from the client!\n");
		} catch (Exception e) {
			view.append("Exception connecting to " + remoteHost + ": " + e
					+ "\n");
			e.printStackTrace();
			return "No connection established!";
		}
		return "Connection to " + remoteHost + " established!";
	}

	/**
	 * Model will dynamically load the task factory by given an input task type name
	 * @param className the name of task type
	 */
	public void loadTask(String className) {
		try {
			Object[] args = new Object[] {}; // YOUR CONSTRUCTOR MAY BE DIFFERENT!!   The supplied values here may be fields, input parameters, random values, etc.
			java.lang.reflect.Constructor<?> cs[] = Class.forName(
					"client.model.taskfactory." + className + "Fac")
					.getConstructors(); // get all the constructors
			java.lang.reflect.Constructor<?> c = null;
			for (int i = 0; i < cs.length; i++) { // find the first constructor with the right number of input parameters
				if (args.length == (cs[i]).getParameterTypes().length) {
					c = cs[i];
					break;
				}
			}
			taskFac = (ITaskFac) c.newInstance(args); // Call the constructor.   Will throw a null ptr exception if no constructor with the right number of input parameters was found.
			view.append(taskFac.getTaskType() + " task loading successful!\n");
		} catch (Exception ex) {
			view.append("task loading failed!\n");
			System.err.println("client.model.task." + className
					+ " failed to load. \n" + ex);
		}
	}

	/**
	 * Call the loaded task factory to make a task that take the input
	 * parameter string passed from view side, and then execute it, 
	 * return the represent string of the task result.  
	 * 
	 * @param para The input parameters that delimited by white space
	 * @return the String that represent the task result
	 */
	public String execute(String para) {
		String result = "";
		if (null == comp)
			return "No ICompute object yet!";

		try {
			ITask<?> task = taskFac.make(para);
			view.append("Executing " + task + "\n");
			result = comp.executeTask(task).toString() + "\n";
		} catch (ParameterInvalid e) {
			view.append(e.getMessage() + "\n");
			System.err.println("Parameter exception: " + e + "\n");
		} catch (Exception e) {
			System.err.println("Task executing exception: " + e + "\n");
			e.printStackTrace();
		}
		return result;
	}
}
