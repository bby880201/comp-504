/**
 * 
 */
package client.model.exception;

/**
 * An exception that should be thrown when the input parameter is invalid
 * @author Boyang Bai
 *
 */
public class ParameterInvalid extends Exception {

	/**
	 * SerialversionUID for well-defined serialization.
	 */
	private static final long serialVersionUID = 4331805807832417512L;

	/**
	 *Constructs a new exception with the specified detail message.  The
	 * cause is not initialized, and may subsequently be initialized by
	 * a call to {@link #initCause}.
	 *
	 * @param   message   the detail message. The detail message is saved for
	 *          later retrieval by the {@link #getMessage()} method. 
	 */
	public ParameterInvalid(String message) {
		super(message);
	}

	/**
	 * Constructs a new ParameterInvalid with the specified detail message and
	 * cause.  <p>Note that the detail message associated with
	 * {@code cause} is <i>not</i> automatically incorporated in
	 * this exception's detail message.
	 *
	 * @param  message the detail message (which is saved for later retrieval
	 *         by the {@link #getMessage()} method).
	 * @param  throwable the cause (which is saved for later retrieval by the
	 *         {@link #getCause()} method).  (A <tt>null</tt> value is
	 *         permitted, and indicates that the cause is nonexistent or
	 *         unknown.)
	 */
	public ParameterInvalid(String message, Throwable throwable) {
		super(message, throwable);
	}

}
