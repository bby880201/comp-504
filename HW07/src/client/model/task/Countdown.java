package client.model.task;

import java.rmi.RemoteException;

import provided.compute.ILocalTaskViewAdapter;
import provided.compute.ITask;

public class Countdown implements ITask<String> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4412576192228322175L;
	
	private transient ILocalTaskViewAdapter taskView = ILocalTaskViewAdapter.DEFAULT_ADAPTER;

	private final int seconds;
	
	public Countdown(int seconds) {
		this.seconds = seconds;
		taskView.append("Counting down... ");
		// TODO Auto-generated constructor stub
	}

	@Override
	public String execute() throws RemoteException {
		int countdown = this.seconds;
		while(countdown > 0 )
		{
			taskView.append(((Integer)countdown).toString() + "...\n");
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return "Countdown interrupted";
			}
			countdown--;
			
		}
		taskView.append("Countdown complete!");
		return "Countdown complete";
	}

	@Override
	public void setTaskViewAdapter(ILocalTaskViewAdapter viewAdapter) {
		taskView = viewAdapter;
		// TODO Auto-generated method stub
		
	}

}
