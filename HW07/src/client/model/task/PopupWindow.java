package client.model.task;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.rmi.RemoteException;
import java.util.concurrent.ThreadLocalRandom;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.WindowConstants;

import provided.compute.ILocalTaskViewAdapter;
import provided.compute.ITask;

/**
 * This task creates popup windows on the server that cannot be closed until the 
 * server closes. This may be slightly evil
 * @author dms5
 *
 */
public class PopupWindow implements ITask<String> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6917524360802147876L;
	

    /**
     * Adapter to the local view.  Marked "transient" so that it is not serialized
     * and instead is reattached at the destination (the server).  
     */
	private transient ILocalTaskViewAdapter taskView = ILocalTaskViewAdapter.DEFAULT_ADAPTER;

	public PopupWindow() {
	}

	/**
	 * Create a popup window on the server at a random location, set it to always be on top,
	 * and override the close behavior to do nothing
	 */
	@Override
	public String execute() throws RemoteException {
		// TODO Auto-generated method stub
		taskView.append("Creating popup window");
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int width = ((Double)screenSize.getWidth()).intValue();
		int height = ((Double)screenSize.getHeight()).intValue();
		
		final JFrame popup = new JFrame();
		popup.setSize(400, 400);
		popup.setAlwaysOnTop(true);
		popup.setLocation(ThreadLocalRandom.current().nextInt(width - 200), ThreadLocalRandom.current().nextInt(height - 200));
		
        JButton button = new JButton();

        button.setText("Close Window");
        popup.add(button);
        popup.pack();
        popup.setVisible(true);
        popup.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

        button.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	String[] responses = {"NOPE","NICE TRY","NO WAY","HA HA!","NEXT TIME","FINE"};
            	
            	int randomNum = ThreadLocalRandom.current().nextInt(5);
            	if(randomNum == 5){
                    popup.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
                    button.setText(responses[randomNum]);
                    button.removeActionListener(this);
                    button.addActionListener(new java.awt.event.ActionListener() {
                    	@Override
                    	public void actionPerformed(java.awt.event.ActionEvent evt){
                    		popup.dispose();
                    	}
                    });
            	}
            	else{
            		button.setText(responses[randomNum]);
            	}
            	
            }
        });
		return "Popup created";
	}

	@Override
	public void setTaskViewAdapter(ILocalTaskViewAdapter viewAdapter) {
		this.taskView = viewAdapter;
		
	}

}
