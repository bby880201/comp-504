/**
 * 
 */
package client.model.taskfactory;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collections;

import provided.compute.ILocalTaskViewAdapter;
import provided.compute.ITask;
import client.model.exception.ParameterInvalid;

/**
 * @author Boyang Bai
 *
 */
public class ArraySortFac implements ITaskFac {

	/**
	 * SerialversionUID for well-defined serialization.
	 */
	private static final long serialVersionUID = 3291824393837293388L;

	/* (non-Javadoc)
	 * @see client.model.taskfactory.ITaskFac#make(java.lang.String)
	 */
	/**
	 * {@inheritDoc}
	 */
	@Override
	public ITask<?> make(final String para) throws ParameterInvalid {
		ArrayList<Float> sortingArray = new ArrayList<Float>();
		try {
			for (String s : para.split("\\s+")) {
				sortingArray.add(Float.valueOf(s));
			}
		} catch (Exception e) {
			throw new ParameterInvalid("Parameter invalid. All input "
					+ "values should be float type and delimited by space!");
		}

		return new ITask<ArrayList<Float>>() {
			/**
			 * SerialversionUID for well-defined serialization.
			 */
			private static final long serialVersionUID = -7822142889451883053L;

			/**
			 * The input array that needs to be sorted
			 */
			private final ArrayList<Float> arr;

			/**
			 * Adapter to the local view.  Marked "transient" so that it is not serialized
			 * and instead is reattached at the destination (the server).  
			 */
			private transient ILocalTaskViewAdapter taskView = ILocalTaskViewAdapter.DEFAULT_ADAPTER;

			{
				this.arr = sortingArray;
				taskView.append("Sorting array... ");
			}

			/**
			 * Sort array and retrun the sorted array
			 */
			@Override
			public ArrayList<Float> execute() throws RemoteException {
				Collections.sort(arr);
				System.out.println(arr);
				return arr;
			}

			@Override
			public void setTaskViewAdapter(ILocalTaskViewAdapter viewAdapter) {
				this.taskView = viewAdapter;
			}

			public String toString() {
				return "ArraySort task with parameter " + para;
			}
		};
	}

	/* (non-Javadoc)
	 * @see client.model.taskfactory.ITaskFac#getTaskType()
	 */
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getTaskType() {
		return "ArraySort";
	}

}
