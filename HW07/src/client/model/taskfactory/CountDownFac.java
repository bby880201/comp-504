/**
 * 
 */
package client.model.taskfactory;

import java.rmi.RemoteException;

import provided.compute.ILocalTaskViewAdapter;
import provided.compute.ITask;
import client.model.exception.ParameterInvalid;

/**
 * @author dmc5, Boyang Bai
 *
 */
public class CountDownFac implements ITaskFac {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4105983265667311029L;

	/* (non-Javadoc)
	 * @see client.model.taskfactory.ITaskFac#make(java.lang.String)
	 */
	/**
	 * {@inheritDoc}
	 */
	@Override
	public ITask<?> make(final String para) throws ParameterInvalid {
		String paras[];
		try {
			paras = para.split("\\s+");
			if (Integer.parseInt(paras[0]) < 0) {
				throw new ParameterInvalid(
						"Parameter n shoud be greater than 0!");
			}
		} catch (Exception e) {
			throw new ParameterInvalid(
					"Parameter invalid. A positive integer needed!");
		}

		return new ITask<String>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 920312482123405509L;

			private transient ILocalTaskViewAdapter taskView = ILocalTaskViewAdapter.DEFAULT_ADAPTER;

			private final int seconds;

			{
				this.seconds = Integer.parseInt(paras[0]);
				taskView.append("Counting down... \n");
			}

			/**
			 * Have the server "count down" from the number input, 
			 * waiting a full second between each count
			 */
			@Override
			public String execute() throws RemoteException {
				int countdown = this.seconds;
				while (countdown > 0) {
					taskView.append(((Integer) countdown).toString() + "...\n");
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
						return "Countdown interrupted";
					}
					countdown--;

				}
				taskView.append("Countdown complete!\n");
				return "Countdown complete";
			}

			@Override
			public void setTaskViewAdapter(ILocalTaskViewAdapter viewAdapter) {
				taskView = viewAdapter;
			}

			public String toString() {
				return "CountDown task with parameter " + para;
			}
		};
	}

	/* (non-Javadoc)
	 * @see client.model.taskfactory.ITaskFac#getTaskType()
	 */
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getTaskType() {
		return "CountDown";
	}

}
