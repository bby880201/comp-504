/**
 * 
 */
package client.model.taskfactory;

import java.io.Serializable;

import client.model.exception.ParameterInvalid;
import provided.compute.ITask;

/**
 * The top level task factory interface. The concrete classes of 
 * this interface represent the factories of different types of 
 * task. The factory can make corresponding task with input
 * parameters.
 *  
 * @author Boyang Bai
 *
 */
public interface ITaskFac extends Serializable {

	/**
	 * The null object of this class.
	 */
	public final static ITaskFac NULL_OBJECT = new ITaskFac() {

		/**
		 * SerialversionUID for well-defined serialization.
		 */
		private static final long serialVersionUID = -86186014591802038L;

		@Override
		public ITask<?> make(String para) {
			System.err.println(this);
			return null;
		}

		@Override
		public String toString() {
			return "task type is invalid or not specified";
		}

		@Override
		public String getTaskType() {
			return "No Specific Task";
		};

	};

	/**
	 * Generate a task that takes some specific parameters
	 *  
	 * @param para input parameters to task
	 * @return an ITask object of this type of task
	 * @throws ParameterInvalid if input parameters are invalid, this exception will be raised
	 */
	public ITask<?> make(final String para) throws ParameterInvalid;

	/**
	 * Return the task type 
	 * @return task type
	 */
	public String getTaskType();
}
