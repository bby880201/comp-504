package client.view;

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;

/**
 * The view of the client MVC system.
 * 
 * This code was edited or generated using CloudGarden's Jigloo
 * SWT/Swing GUI Builder, which is free for non-commercial
 * use. If Jigloo is being used commercially (ie, by a corporation,
 * company or business for any purpose whatever) then you
 * should purchase a license for each developer using Jigloo.
 * Please visit www.cloudgarden.com for details.
 * Use of Jigloo implies acceptance of these licensing terms.
 * A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
 * THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
 * LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
 * 
 *  @author Boyang Bai
 */

public class ClientGUI extends JFrame {

	/**
	 * SerialVersionUId for the class.
	 */
	private static final long serialVersionUID = -199099598475124566L;

	/**
	 * The default remote host reference.
	 */
	private static final String DEFAULT_REMOTE_HOST = "localhost";

	/**
	 * The adapter to the model.
	 */
	private IModelAdapter model;

	/**
	 * The top control panel
	 */
	private JPanel controlPnl;

	/**
	 * The status output text area
	 */
	private JTextArea outputTA;

	/**
	 * The connect button
	 */
	private JButton connectBtn;

	/**
	 * The quit button
	 */
	private JButton quitBtn;

	/**
	 * The remote server's IP address info input text field.
	 */
	private JTextField remoteHostTF;
	private JLabel taskTypeLbl;
	private JButton btnLoadTask;
	private JButton btnExecuteTask;
	private JTextField paraTF;
	private JTextField taskTypeTF;

	/**
	 * Constructor of the class
	 * @param ma the ModelAdapter 
	 */
	public ClientGUI(IModelAdapter ma) {
		super("Client GUI");
		model = ma;
		initGUI();
	}

	/**
	 * Initializes the view and its components.
	 */
	protected void initGUI() {
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		this.addWindowListener(new WindowAdapter() {
			/**
			 * tell the model to quit if the window is closing, even if very slowly.
			 */
			public void windowClosing(WindowEvent evt) {
				System.out.println("this.windowClosing, event=" + evt);
				model.quit();
			}
		});
		setSize(800, 600);
		controlPnl = new JPanel();
		outputTA = new JTextArea();
		outputTA.setEditable(false);
		JScrollPane scroll = new JScrollPane(outputTA);
		Container contentPane = getContentPane();
		contentPane.add(controlPnl, BorderLayout.NORTH);
		GridBagLayout gbl_controlPnl = new GridBagLayout();
		gbl_controlPnl.columnWidths = new int[] { 14, 88, 100, 100, 17, 76,
				100, 0, 71, 100, 17, 0 };
		gbl_controlPnl.rowHeights = new int[] { 30, 0, 0 };
		gbl_controlPnl.columnWeights = new double[] { 0.0, 0.0, 1.0, 0.0, 0.0,
				0.0, 1.0, 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE };
		gbl_controlPnl.rowWeights = new double[] { 0.0, 0.0, Double.MIN_VALUE };
		controlPnl.setLayout(gbl_controlPnl);

		JLabel remoteHostLbl = new JLabel("Remote Host:");
		GridBagConstraints gbc_remoteHostLbl = new GridBagConstraints();
		gbc_remoteHostLbl.fill = GridBagConstraints.HORIZONTAL;
		gbc_remoteHostLbl.insets = new Insets(0, 0, 5, 5);
		gbc_remoteHostLbl.gridx = 1;
		gbc_remoteHostLbl.gridy = 0;
		controlPnl.add(remoteHostLbl, gbc_remoteHostLbl);

		remoteHostTF = new JTextField(DEFAULT_REMOTE_HOST);
		remoteHostTF.setToolTipText("Enter server's ip address here");
		remoteHostTF.setPreferredSize(new Dimension(100, 25));
		GridBagConstraints gbc_remoteHostTF = new GridBagConstraints();
		gbc_remoteHostTF.gridwidth = 2;
		gbc_remoteHostTF.fill = GridBagConstraints.HORIZONTAL;
		gbc_remoteHostTF.insets = new Insets(0, 0, 5, 5);
		gbc_remoteHostTF.gridx = 2;
		gbc_remoteHostTF.gridy = 0;
		controlPnl.add(remoteHostTF, gbc_remoteHostTF);
		remoteHostTF.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				connect();
			}
		});

		taskTypeLbl = new JLabel("Task Type:");
		GridBagConstraints gbc_taskTypeLbl = new GridBagConstraints();
		gbc_taskTypeLbl.anchor = GridBagConstraints.EAST;
		gbc_taskTypeLbl.insets = new Insets(0, 0, 5, 5);
		gbc_taskTypeLbl.gridx = 5;
		gbc_taskTypeLbl.gridy = 0;
		controlPnl.add(taskTypeLbl, gbc_taskTypeLbl);

		taskTypeTF = new JTextField();
		taskTypeTF.setToolTipText("Enter the type of task here");
		taskTypeTF.setText("GetInfo");
		GridBagConstraints gbc_taskTypeTF = new GridBagConstraints();
		gbc_taskTypeTF.insets = new Insets(0, 0, 5, 5);
		gbc_taskTypeTF.fill = GridBagConstraints.HORIZONTAL;
		gbc_taskTypeTF.gridx = 6;
		gbc_taskTypeTF.gridy = 0;
		controlPnl.add(taskTypeTF, gbc_taskTypeTF);
		taskTypeTF.setColumns(10);

		JLabel paramsLbl = new JLabel("Parameters:");
		GridBagConstraints gbc_paramsLbl = new GridBagConstraints();
		gbc_paramsLbl.fill = GridBagConstraints.HORIZONTAL;
		gbc_paramsLbl.insets = new Insets(0, 0, 5, 5);
		gbc_paramsLbl.gridx = 8;
		gbc_paramsLbl.gridy = 0;
		controlPnl.add(paramsLbl, gbc_paramsLbl);

		paraTF = new JTextField();
		paraTF.setToolTipText("Enter parameters here, using space as delimiter");
		GridBagConstraints gbc_paraTF = new GridBagConstraints();
		gbc_paraTF.insets = new Insets(0, 0, 5, 5);
		gbc_paraTF.fill = GridBagConstraints.HORIZONTAL;
		gbc_paraTF.gridx = 9;
		gbc_paraTF.gridy = 0;
		controlPnl.add(paraTF, gbc_paraTF);
		paraTF.setColumns(10);

		connectBtn = new JButton();
		connectBtn.setToolTipText("Connect to server");
		GridBagConstraints gbc_connectBtn = new GridBagConstraints();
		gbc_connectBtn.insets = new Insets(0, 0, 0, 5);
		gbc_connectBtn.gridx = 2;
		gbc_connectBtn.gridy = 1;
		controlPnl.add(connectBtn, gbc_connectBtn);
		connectBtn.setText("Connect");
		connectBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				System.out.println("connectBtn.actionPerformed, event=" + evt);
				connect();
			}
		});

		quitBtn = new JButton();
		quitBtn.setToolTipText("Quit this client application");
		GridBagConstraints gbc_quitBtn = new GridBagConstraints();
		gbc_quitBtn.insets = new Insets(0, 0, 0, 5);
		gbc_quitBtn.gridx = 3;
		gbc_quitBtn.gridy = 1;
		controlPnl.add(quitBtn, gbc_quitBtn);
		quitBtn.setText("Quit");

		btnLoadTask = new JButton("Load Task");
		btnLoadTask.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				model.loadTask(taskTypeTF.getText());
			}
		});

		btnLoadTask.setToolTipText("Load a selected task");
		GridBagConstraints gbc_btnLoadTask = new GridBagConstraints();
		gbc_btnLoadTask.insets = new Insets(0, 0, 0, 5);
		gbc_btnLoadTask.gridx = 6;
		gbc_btnLoadTask.gridy = 1;
		controlPnl.add(btnLoadTask, gbc_btnLoadTask);

		btnExecuteTask = new JButton("Execute Task");
		btnExecuteTask.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					append("Start executing task\n");
					append(model.execute(paraTF.getText()));
				} catch (Exception excpt) {
					append("Task exception: " + excpt + "\n");
				}
			}
		});
		btnExecuteTask.setToolTipText("Execute selected task with parameters");
		GridBagConstraints gbc_btnExecuteTask = new GridBagConstraints();
		gbc_btnExecuteTask.insets = new Insets(0, 0, 0, 5);
		gbc_btnExecuteTask.gridx = 9;
		gbc_btnExecuteTask.gridy = 1;
		controlPnl.add(btnExecuteTask, gbc_btnExecuteTask);
		quitBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				System.out.println("quitBtn.actionPerformed, event=" + evt);
				model.quit();
			}
		});

		contentPane.add(scroll, BorderLayout.CENTER);
	}

	/**
	 * Have the model connect to the remote server.
	 */
	private void connect() {
		append("Connecting...\n");
		append(model.connectTo(remoteHostTF.getText()) + "\n");
	}

	/**
	 * Set the displayed remote host text field to the actual remote system's IP address or host name 
	 * @param host The name of the remote host 
	 */
	public void setRemoteHost(String host) {
		remoteHostTF.setText(host);
	}

	/**
	 * Append the given string(s) to the view's output text adapter.  
	 * @param s the string to display.
	 */
	public void append(String s) {
		outputTA.append(s);
		//Force the JScrollPane to go to scroll down to the new text
		outputTA.setCaretPosition(outputTA.getText().length());
	}

	/**
	 * Starts the view by making it visible.
	 */
	public void start() {
		setVisible(true);
	}
}