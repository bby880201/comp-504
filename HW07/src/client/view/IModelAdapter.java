package client.view;

/**
 * Adapter the view uses to communicate to the model
 */
public interface IModelAdapter {
	/**
	 * Requests that model connect to the RMI Registry at the given remote host
	 * 
	 * @param remoteHost
	 *            The remote host to connect to.
	 * @return A status string regarding the connection result
	 */
	public String connectTo(String remoteHost);

	/**
	 * Quits the applications and gracefully shuts down the RMI-related resources.
	 */
	public void quit();

	public void loadTask(String text);

	public String execute(String string);

}