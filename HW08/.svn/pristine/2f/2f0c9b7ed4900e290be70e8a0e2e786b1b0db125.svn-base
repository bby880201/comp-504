package controller;

import java.awt.Container;
import java.awt.EventQueue;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;
import common.IChatroom;
import common.IUser;
import view.IView2ModelAdapter;
import view.MainGUI;
import view.chatwindow.ChattingWindow;
import view.chatwindow.IChatWindow2Model;
import model.ChatAppMainModel;
import model.IModel2ViewAdapter;
import model.chatroom.ChatroomWithAdapter;
import model.chatroom.IChatRoom2View;
import provided.datapacket.DataPacket;

/**
 * 
 * @author bb26, xc7
 */
public class ChatAppController {
	
	private MainGUI<IChatroom, IUser> view;
	
	private ChatAppMainModel model;
	

	public ChatAppController() {
		
		view = new MainGUI<IChatroom,IUser> (new IView2ModelAdapter<IChatroom, IUser>(){
			/**
			 * Quits the current connection and closes the application.   
			 * Causes the model to stop and thus end the application. 
			 */
			@Override
			public void quit() {
				model.stop();
			}
			
			/**
			 * Requests that model connect to the RMI Registry at the given remote host
			 * 
			 * @param remoteHost The remote host to connect to.
			 * @return A status string regarding the connection result
			 */
			@Override
			public void chatWith(String ip) {
				model.chatWith(ip);
			}

			@Override
			public HashSet<IChatroom> getListRooms(String ip) {
				return model.getFriendChatrooms(ip);
			
//				HashSet<IChatroom> lstRooms = new HashSet<IChatroom>();
//				try {
//					lstRooms = model.getFriend().getConnect().getChatrooms();
//				} catch (RemoteException e) {
//					e.printStackTrace();
//				}
//				return lstRooms;
//			}
			}
			@Override
			public void joinChatroom(IChatroom rm) {
				model.joinRoom(rm);
			}
		});
		
		model = new ChatAppMainModel(new IModel2ViewAdapter<IUser>() {
			/**
			 * pass the data needed to be received through the adapter
			 * 
			 * @param roomID
			 * @param me
			 * @param data
			 */
			public <T> void receive(String roomID, IUser me, DataPacket<T> data) {
				
			}

			@Override
			public IChatRoom2View<IUser> makeChatRoom(ChatroomWithAdapter chatRoom) {
				ChattingWindow<IUser> cw = view.makeChatRoom(new IChatWindow2Model<IUser>() {
					
					@Override
					public String getName() {
						return chatRoom.getName();
					}

					@Override
					public void quit() {
						chatRoom.quit();
					}

					@Override
					public void sendMsg(String text) {
						chatRoom.sendMsg(text);
					}

					@Override
					public void invite(String text) {
						IUser friend = model.connectTo(text);
						if (null != friend) chatRoom.invite(friend);
					}

					@SuppressWarnings({ "rawtypes", "unchecked" })
					@Override
					public void deleteWindow(ChattingWindow cw) {
						view.deleteChatWindow(cw);
					}

					@Override
					public void speakTo(IUser user) {
						model.createNewRoom(user);
					}
				});
				
				return new IChatRoom2View<IUser>() {

					@Override
					public void append(String data) {
						cw.append(data);
					}
					
					@Override
					public void refreshList(List<IUser> users) {
						cw.refreshList(users);						
					}

					@Override
					public void deleteWindow() {
						cw.deleteWindow();
					}

					@Override
					public void deleteModel(UUID id) {
						model.deleteChatroom(id);
					}

					@Override
					public Container Scrollable() {
						return cw.Scrollable();
					}
				};
			}
		});
	}
	
	private void start() {
		model.start();
		view.start();
	}
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ChatAppController ctl = new ChatAppController();
					ctl.start();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
