package view.chatwindow;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.border.BevelBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.text.DefaultCaret;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

public class ChattingWindow<User> extends JSplitPane {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8432378916828183998L;
	
	private IChatWindow2Model toChatroomAdapt;
	
	private ChattingWindow<User> thisWindow = this;
	
	private JEditorPane edMsg;
	
	private JTextArea taDisplay;
		
	public ChattingWindow(IChatWindow2Model mv2mmAdapt) {
		super();
		
		toChatroomAdapt = mv2mmAdapt;
		
		this.setResizeWeight(0.7);
		this.setOrientation(JSplitPane.VERTICAL_SPLIT);
		
		JPanel panel_2 = new JPanel();
		this.setLeftComponent(panel_2);
		GridBagLayout gbl_panel_2 = new GridBagLayout();
		gbl_panel_2.columnWidths = new int[]{0, 0, 0};
		gbl_panel_2.rowHeights = new int[]{0, 0};
		gbl_panel_2.columnWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
		gbl_panel_2.rowWeights = new double[]{1.0, Double.MIN_VALUE};
		panel_2.setLayout(gbl_panel_2);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		GridBagConstraints gbc_scrollPane_1 = new GridBagConstraints();
		gbc_scrollPane_1.insets = new Insets(0, 0, 0, 5);
		gbc_scrollPane_1.fill = GridBagConstraints.BOTH;
		gbc_scrollPane_1.gridx = 0;
		gbc_scrollPane_1.gridy = 0;
		panel_2.add(scrollPane_1, gbc_scrollPane_1);
		
		taDisplay = new JTextArea();
		taDisplay.setLineWrap(true);
		taDisplay.setWrapStyleWord(true);
		taDisplay.setEditable(false);
		DefaultCaret caret = (DefaultCaret) taDisplay.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		scrollPane_1.setViewportView(taDisplay);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		GridBagConstraints gbc_panel_4 = new GridBagConstraints();
		gbc_panel_4.fill = GridBagConstraints.BOTH;
		gbc_panel_4.gridx = 1;
		gbc_panel_4.gridy = 0;
		panel_2.add(panel_4, gbc_panel_4);
		GridBagLayout gbl_panel_4 = new GridBagLayout();
		gbl_panel_4.columnWidths = new int[]{0, 0};
		gbl_panel_4.rowHeights = new int[]{0, 150, 0, 0, 0, 20, 0, 0};
		gbl_panel_4.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_panel_4.rowWeights = new double[]{0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel_4.setLayout(gbl_panel_4);
		
		JLabel lblMemberList = new JLabel("Member List:");
		GridBagConstraints gbc_lblMemberList = new GridBagConstraints();
		gbc_lblMemberList.fill = GridBagConstraints.BOTH;
		gbc_lblMemberList.insets = new Insets(0, 0, 5, 0);
		gbc_lblMemberList.gridx = 0;
		gbc_lblMemberList.gridy = 0;
		panel_4.add(lblMemberList, gbc_lblMemberList);
		
		JPanel panel_6 = new JPanel();
		GridBagConstraints gbc_panel_6 = new GridBagConstraints();
		gbc_panel_6.insets = new Insets(0, 0, 5, 0);
		gbc_panel_6.fill = GridBagConstraints.BOTH;
		gbc_panel_6.gridx = 0;
		gbc_panel_6.gridy = 1;
		panel_4.add(panel_6, gbc_panel_6);
		panel_6.setLayout(new BorderLayout(0, 0));
		
		JList<User> lsMember = new JList<User>();
		lsMember.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		lsMember.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		panel_6.add(lsMember, BorderLayout.CENTER);
		
		JButton btnSpeakTo = new JButton("Speak To");
		GridBagConstraints gbc_btnSpeakTo = new GridBagConstraints();
		gbc_btnSpeakTo.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnSpeakTo.insets = new Insets(0, 0, 5, 0);
		gbc_btnSpeakTo.gridx = 0;
		gbc_btnSpeakTo.gridy = 2;
		panel_4.add(btnSpeakTo, gbc_btnSpeakTo);
		
		JButton btnSendFile = new JButton("Send File");
		GridBagConstraints gbc_btnSendFile = new GridBagConstraints();
		gbc_btnSendFile.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnSendFile.insets = new Insets(0, 0, 5, 0);
		gbc_btnSendFile.gridx = 0;
		gbc_btnSendFile.gridy = 3;
		panel_4.add(btnSendFile, gbc_btnSendFile);
		
		JButton btnLeave = new JButton("Leave");
		btnLeave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				toChatroomAdapt.quit(thisWindow);
			}
		});
		
		JButton btnGetRooms = new JButton("Get Rooms");
		GridBagConstraints gbc_btnGetRooms = new GridBagConstraints();
		gbc_btnGetRooms.insets = new Insets(0, 0, 5, 0);
		gbc_btnGetRooms.gridx = 0;
		gbc_btnGetRooms.gridy = 4;
		panel_4.add(btnGetRooms, gbc_btnGetRooms);
		
		GridBagConstraints gbc_btnLeave = new GridBagConstraints();
		gbc_btnLeave.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnLeave.gridx = 0;
		gbc_btnLeave.gridy = 6;
		panel_4.add(btnLeave, gbc_btnLeave);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		this.setRightComponent(panel_3);
		
		GridBagLayout gbl_panel_3 = new GridBagLayout();
		gbl_panel_3.columnWidths = new int[]{717, -62, 0};
		gbl_panel_3.rowHeights = new int[]{0, 0};
		gbl_panel_3.columnWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
		gbl_panel_3.rowWeights = new double[]{1.0, Double.MIN_VALUE};
		panel_3.setLayout(gbl_panel_3);
		
		JButton btnSend = new JButton("Send");
		btnSend.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				String str = edMsg.getText();
				taDisplay.append("YOU says:\n" + str + "\n");
				toChatroomAdapt.sendMsg(str);
				edMsg.setText("");
			}
		});
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.insets = new Insets(0, 0, 0, 5);
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 0;
		panel_3.add(scrollPane, gbc_scrollPane);
		
		edMsg = new JEditorPane();
		scrollPane.setViewportView(edMsg);
		GridBagConstraints gbc_btnSend = new GridBagConstraints();
		gbc_btnSend.fill = GridBagConstraints.BOTH;
		gbc_btnSend.gridx = 1;
		gbc_btnSend.gridy = 0;
		panel_3.add(btnSend, gbc_btnSend);
		
	}

	public void append(String data) {
		taDisplay.append(data + "\n");
	}

}
