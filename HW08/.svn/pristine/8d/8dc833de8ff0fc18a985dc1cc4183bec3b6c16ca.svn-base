package model;

import java.awt.Container;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.HashSet;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import model.chatroom.ChatroomWithAdapter;
import model.messages.StringMsgTest;
import model.user.Connect;
import model.user.IConnectToWorldAdapter;
import model.user.User;
import common.IChatroom;
import common.ICmd2ModelAdapter;
import common.IConnect;
import common.IUser;
import common.messages.AddMe;
import common.messages.InviteToChatroom;
import common.messages.RemoveMe;
import common.messages.RequestForAlgo;
import provided.datapacket.ADataPacket;
import provided.datapacket.ADataPacketAlgoCmd;
import provided.datapacket.DataPacket;
import provided.datapacket.DataPacketAlgo;
import provided.mixedData.MixedDataDictionary;
import provided.rmiUtils.IRMIUtils;
import provided.rmiUtils.IRMI_Defs;
import provided.rmiUtils.RMIUtils;
import provided.util.IVoidLambda;

/**
 * 
 * @author bb26, xc7
 */
public class ChatAppMainModel {
	
	/**
	 * The RMI Registry
	 */
	private Registry registry;
	
	/**
	 * Utility object used to get the Registry
	 */
	private IRMIUtils rmiUtils;
	
	private IUser me = null;
		
	private HashMap<UUID, IChatroom> rooms;
		
	private IModel2ViewAdapter<IUser> toView;
	
	private DataPacketAlgo<String, Object> msgAlgo;
	
	private String userName = "xc7";

	private Class<?> newCmdClass;
	// 1-element blocking queue to guarantee unknown command is installed before execute the cmd
	private BlockingQueue<Integer> bq = new ArrayBlockingQueue<Integer>(1);
	
	@SuppressWarnings("rawtypes")
	public ChatAppMainModel(IModel2ViewAdapter<IUser> toViewAdapter) {
		toView = toViewAdapter;
		rooms = new HashMap<UUID, IChatroom>();
		msgAlgo = new DataPacketAlgo<String,Object>(new ADataPacketAlgoCmd<String, Object, Object>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = -4329950671092819917L;

			@Override

			/**
			 * default cmd to handle unknown command type
			 */
			public String apply(Class<?> index, DataPacket<Object> host, Object... params) {
				IUser remote = (IUser) params[0];
				
				newCmdClass = host.getData().getClass();
				
				RequestForAlgo pac = new RequestForAlgo(newCmdClass);
				
				try {
					remote.getConnect().sendReceive(me, new DataPacket<RequestForAlgo>(RequestForAlgo.class, pac));
				} catch (RemoteException e) {
					e.printStackTrace();
				}
				// block here until unknown command is installed
				try {
					bq.take();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				msgAlgo.getCmd(newCmdClass).apply(index, host, params);
				
				return null;
			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
				
			}
		});
		
				
		msgAlgo.setCmd(InviteToChatroom.class, new ADataPacketAlgoCmd<String, InviteToChatroom, Object>(){

			/**
			 * 
			 */
			private static final long serialVersionUID = 6397860207466953790L;

			@Override
			public String apply(Class<?> index, DataPacket<InviteToChatroom> host, Object... params) {
				
				try {
					
					IChatroom remoteRoom = host.getData().chatroom;
					
					ChatroomWithAdapter room = new ChatroomWithAdapter(userName, remoteRoom.id());
					room.setChatWindowAdapter(toView.makeChatRoom(room));
					
					IUser[] remoteUsers = remoteRoom.getUsers().toArray(new IUser[remoteRoom.getUsers().size()]);
					
					for (int i = 0; i < remoteUsers.length; i ++) {
						room.addUser(remoteUsers[i]);
					}
					
					room.addMe();
					
					rooms.put(room.id(), (IChatroom) room);
					
				} catch (Exception e) {
					System.out.println("create room failed: " + e + "\n");
					e.printStackTrace();
				}

				return null;
//				IChatroom rm = host.getData().chatroom;
//				IUser remote = (IUser) params[0];
//				
//				ChatroomWithAdapter chatRoom = new ChatroomWithAdapter(me,rm.id());
//				chatRoom.setChatWindowAdapter(toView.makeChatRoom(chatRoom));
//				
//				for (IUser user: rm.getUsers()) {
//					if (!user.equals(me)) chatRoom.addUser(user);
//				}
//				
//				chatRoom.addUser(remote);
//				rooms.put(rm.id(),chatRoom);
//				return null;
			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
				
			}
		});
		
		msgAlgo.setCmd(RemoveMe.class, new ADataPacketAlgoCmd<String, RemoveMe, Object>(){

			/**
			 * 
			 */
			private static final long serialVersionUID = -3221582487358802589L;

			@Override
			public String apply(Class<?> index, DataPacket<RemoveMe> host, Object... params) {
				
				IChatroom room = rooms.get(host.getData().chatroomID);
				room.removeUser(host.getData().me);
				return null;
				
			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
				// TODO Auto-generated method stub
				
			}
		});
		
		msgAlgo.setCmd(AddMe.class, new ADataPacketAlgoCmd<String, AddMe, Object>(){

			/**
			 * 
			 */
			private static final long serialVersionUID = 6397860207466953790L;

			@Override
			public String apply(Class<?> index, DataPacket<AddMe> host, Object... params) {

				return null;
			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
				
			}
		});
		
		msgAlgo.setCmd(RequestForAlgo.class, new ADataPacketAlgoCmd<String, RequestForAlgo, Object>(){

			/**
			 * 
			 */
			private static final long serialVersionUID = 6397860207466953790L;

			@Override
			public String apply(Class<?> index, DataPacket<RequestForAlgo> host, Object... params) {
				IUser friend = (IUser) params[0];
				
				Class<?> algoType = host.getData().unknownType();
				// downcast the return value of getCmd to AdataPacketAlgoCmd
				ADataPacketAlgoCmd<String, Class<?>, Object> myCmd = 
						(ADataPacketAlgoCmd<String, Class<?>, Object>) msgAlgo.getCmd(algoType);
				
				try {
					friend.getConnect().sendReceive(me, 
							new DataPacket<ADataPacketAlgoCmd>(ADataPacketAlgoCmd.class, myCmd));
				} catch (RemoteException e) {
					e.printStackTrace();
				}
				
				return null;
			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
				
			}
		});
		
		msgAlgo.setCmd(ADataPacketAlgoCmd.class, new ADataPacketAlgoCmd<String, ADataPacketAlgoCmd, Object>(){

			/**
			 * 
			 */
			private static final long serialVersionUID = 6397860207466953790L;

			@SuppressWarnings("unchecked")
			@Override
			public String apply(Class<?> index, DataPacket<ADataPacketAlgoCmd> host, Object... params) {
				
				msgAlgo.setCmd(newCmdClass, host.getData());
				
				bq.offer(1);
				
				return null;
			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
				
			}
		});
		
		// for testing the ability of handle unknown data type
		msgAlgo.setCmd(StringMsgTest.class, new ADataPacketAlgoCmd<String, StringMsgTest, Object>(){

			/**
			 * 
			 */
			private static final long serialVersionUID = -7694775719535304909L;
			
			private transient ICmd2ModelAdapter toModelAdapter;
			{
				this.toModelAdapter = new ICmd2ModelAdapter(){

					@Override
					public Container scrollable() {
						// TODO Auto-generated method stub
						return null;
					}

					@Override
					public Container updateable() {
						// TODO Auto-generated method stub
						return null;
					}

					@Override
					public ADataPacketAlgoCmd<String, ?, IUser> other() {
						// TODO Auto-generated method stub
						return null;
					}

					@Override
					public MixedDataDictionary dictionary() {
						// TODO Auto-generated method stub
						return null;
					}
					
				};
			}
			
			@Override
			public String apply(Class<?> index, DataPacket<StringMsgTest> host, Object... params) {
				
				ChatroomWithAdapter room = (ChatroomWithAdapter) rooms.get(host.getData().getId());
				IUser remote = (IUser) params[0];
				room.display(remote.getName() +" says:\n"+host.getData().getMsg() + "\n");
				return null;
			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
				this.toModelAdapter = cmd2ModelAdpt;
			}
		});
		
	}
	
	public void start() {
		
		rmiUtils = new RMIUtils(new IVoidLambda<String>(){

			@Override
			public void apply(String... params) {
				for (String s:params) {
					System.out.println(s);
				}
			}
			
		});
		
		rmiUtils.startRMI(IRMI_Defs.CLASS_SERVER_PORT_SERVER);
		try {
			Connect connect = new Connect(new IConnectToWorldAdapter(){

				@Override	
				public HashSet<IChatroom> getChatrooms() {
					return new HashSet<IChatroom>(rooms.values());
				}

				@Override
				public <T> ADataPacketAlgoCmd<String, ?, IUser> getCommand(
						RequestForAlgo request) {
					// TODO Auto-generated method stub
					return null;
				}

				// TODO change back id
				@Override
				public IUser getUser(IConnect stub) {
					return me;
				}

				@Override
				public <T> void receive(IUser remote, ADataPacket data) {
					data.execute(msgAlgo, remote);					
				}
			});
			
			IConnect stub =(IConnect) UnicastRemoteObject.exportObject(connect, IConnect.BOUND_PORT);

			registry = rmiUtils.getLocalRegistry();
			registry.rebind(IConnect.BOUND_NAME, stub);
			
			System.out.println("Waiting..."+"\n");

			// me = new User(connect, id, InetAddress.getLocalHost());
			
			me = new User(connect, userName, InetAddress.getLocalHost());

		} catch (Exception e) {
			System.err.println("Connect exception:"+"\n");
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
	
	public IUser connectTo(final String ip) {
		
		IUser friend = null;
		try{
			Registry registry = rmiUtils.getRemoteRegistry(ip);
			System.out.println("Found registry: " + registry + "\n");
			IConnect connect = (IConnect) registry.lookup(IConnect.BOUND_NAME);
			System.out.println("Found remote Connect object: " + connect + "\n");
			friend = (IUser)(connect.getUser(connect));
		}catch (Exception e){
			System.out.println("Establish connect failed!\n Exception connecting to " + ip + ": " + e + "\n");
		}
	
		return friend;
	}
	
	public void stop()
	{
		try {
			registry.unbind(IConnect.BOUND_NAME);
			System.out.println("Chat App Model Registry: " + IConnect.BOUND_NAME
					+ " has been unbound.");
			
			rmiUtils.stopRMI();
			
			System.exit(0);
		} catch (Exception e) {
			System.err.println("Chat App Model Registry: Error unbinding "
					+ IConnect.BOUND_NAME + ":\n" + e);
			System.exit(-1);
		}
	}

	public void deleteChatroom(UUID id2) {
		rooms.remove(id2);
	}

	
	public void chatWith(final String ip) {
	
		IUser friend = connectTo(ip);
		
		if (null != friend) { 
			createNewRoom(friend);
		}
	}
	
	public void createNewRoom(IUser friend) {
		(new Thread(){
			@Override
			public void run(){
				if (null!=friend){
					try{
						ChatroomWithAdapter chatRoom = new ChatroomWithAdapter(userName);
						chatRoom.setChatWindowAdapter(toView.makeChatRoom(chatRoom));
						
						InviteToChatroom invite = new InviteToChatroom(chatRoom);
						friend.getConnect().sendReceive(chatRoom.getMe(), new DataPacket<InviteToChatroom>(InviteToChatroom.class, invite));
						
						rooms.put(chatRoom.id(), (IChatroom) chatRoom);
						
					} catch(Exception e){
						System.out.println("Create room failed: " + e + "\n");
						e.printStackTrace();
					}
				}
			}
		}).start();
	}
		
	public void joinRoom(IChatroom rm) {
		// create a local chatroom with all the existing members in the room to join
		ChatroomWithAdapter chatRoom;
		try {
			chatRoom = new ChatroomWithAdapter(userName);
		
			chatRoom.setChatWindowAdapter(toView.makeChatRoom(chatRoom));
			
			for (IUser user: rm.getUsers()) {
				chatRoom.addUser(user);
			}
			rooms.put(rm.id(), chatRoom);
			// broadcast to all the members in the chatroom
			AddMe pac = new AddMe(me, rm.id());
			rm.send(me, new DataPacket<AddMe>(AddMe.class, pac));
		} catch (UnknownHostException | RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public HashSet<IChatroom> getFriendChatrooms(String ip) {
		IUser friend = connectTo(ip);
		
		try {
			if (null != friend)
			return friend.getConnect().getChatrooms();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return null;
	}
}
