package model.chatroom;

import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.UUID;

import model.messages.StringMessage;
import provided.datapacket.ADataPacket;
import provided.datapacket.DataPacket;
import common.IChatroom;
import common.IUser;
import common.messages.InviteToChatroom;
import common.messages.RemoveMe;

/**
 * 
 * @author bb26, xc7
 */
public class ChatroomWithAdapter implements IChatroom {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4342241825756246744L;
	
	private transient IChatRoom2View<IUser> chatWindowAdapter;
	
	private IUser me;
	
	private UUID id;
	
	private String displayName;
		
	private HashSet<IUser> users;
	
	private IChatroom thisRoom = this;

	public ChatroomWithAdapter(IUser me) {
		setMe(me);
		this.id = UUID.randomUUID();
		users = new HashSet<IUser>();
	}
	
	public ChatroomWithAdapter(IUser me, UUID id){
		this(me);
		this.id = id;
		this.setName(id.toString());
	}
	
	public ChatroomWithAdapter(IChatRoom2View<IUser> adapter, IUser me) {
		this(me);
		setChatWindowAdapter(adapter);
	}

	public IChatRoom2View<IUser> getChatWindowAdapter() {
		return chatWindowAdapter;
	}

	public void setChatWindowAdapter(IChatRoom2View<IUser> chatWindowAdapter) {
		this.chatWindowAdapter = chatWindowAdapter;
	}

	public void quit() {
		
		RemoveMe remove = new RemoveMe(me,id);
		send(me, new DataPacket<RemoveMe>(RemoveMe.class,remove));
		chatWindowAdapter.deleteWindow();
		
	}

	public IUser getMe() {
		return me;
	}

	public void setMe(IUser me) {
		this.me = me;
	}
	
	public void invite(IUser friend) {
		(new Thread(){
			@Override
			public void run(){
				for (IUser user: users) {
					try {
						if (user.equals(friend)){
							InviteToChatroom invite = new InviteToChatroom(thisRoom);
							user.getConnect().sendReceive(me, new DataPacket<InviteToChatroom>(InviteToChatroom.class, invite));
						}
					}catch (RemoteException e) {
						System.out.println("Invite user failed!\nRemote exception invite " + ": " + user.getIP() + e + "\n");
						e.printStackTrace();
					}
				}
				
			}
		}).start();
	}


	@Override
	public UUID id() {
		return id;
	}

	@Override
	public HashSet<IUser> getUsers() {
		return users;
	}

	@Override
	public boolean addUser(IUser user) {
		boolean added = users.add(user);
		refreshList();
		return added;
				
	}

	@Override
	public boolean removeUser(IUser user) {
		
		boolean removed = users.remove(user);
		if (users.size() == 1) {
			chatWindowAdapter.deleteWindow();
			chatWindowAdapter.deleteModel(id);
		}
		refreshList();
		return removed;
	}

	@Override
	public void send(IUser me, ADataPacket message) {
		(new Thread(){
			@Override
			public void run(){
				users.iterator().forEachRemaining((user) ->{ 
					try{
						user.getConnect().sendReceive(me, message);
					}
					catch(RemoteException e){
						e.printStackTrace();
					}
				});
			}
		}).start();
	}

	@Override
	public String getName() {
		if (users.size()>0){
			IUser[] lstUser = users.toArray(new IUser[users.size()]);
			displayName = "Chatroom with members: " + lstUser[0].getName() + " et, al.";
		}
		else{
			displayName = "Chatroom with members: " + me.getName() + " et, al.";
		}
		return displayName;
	}

	@Override
	public void setName(String name) {
		displayName = name;
	}

	public void sendMsg(String text) {
		StringMessage pac = new StringMessage(this,text);
		send(me, new DataPacket<StringMessage>(StringMessage.class, pac));
	}
	
	public void display(String data) {
		chatWindowAdapter.append(data);
	}
	
	private void refreshList() {
		chatWindowAdapter.refreshList(users);
	}

	
	public String toString() {
		return displayName;
	}
}
