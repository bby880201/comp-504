package model;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.HashSet;
import java.util.UUID;

import model.chatroom.ChatroomWithAdapter;
import model.user.Connect;
import model.user.IConnectToWorldAdapter;
import model.user.User;
import common.IChatroom;
import common.ICmd2ModelAdapter;
import common.IConnect;
import common.IUser;
import common.messages.AddMe;
import common.messages.InviteToChatroom;
import common.messages.RemoveMe;
import common.messages.RequestForAlgo;
import provided.datapacket.ADataPacket;
import provided.datapacket.ADataPacketAlgoCmd;
import provided.datapacket.DataPacket;
import provided.datapacket.DataPacketAlgo;
import provided.rmiUtils.IRMIUtils;
import provided.rmiUtils.IRMI_Defs;
import provided.rmiUtils.RMIUtils;
import provided.util.IVoidLambda;

/**
 * 
 * @author bb26, xc7
 */
public class ChatAppMainModel {
	
	/**
	 * The RMI Registry
	 */
	private Registry registry;
	
	/**
	 * Utility object used to get the Registry
	 */
	private IRMIUtils rmiUtils;
	
	private IUser me = null;
		
	private HashMap<UUID, IChatroom> rooms;
		
	private IModel2ViewAdapter<IUser> toView;
	
	private DataPacketAlgo<String, Object> msgAlgo;
	
	private String userName = "xc7_bb26";
	
	public ChatAppMainModel(IModel2ViewAdapter<IUser> toViewAdapter) {
		toView = toViewAdapter;
		rooms = new HashMap<UUID, IChatroom>();
		msgAlgo = new DataPacketAlgo<String,Object>(new ADataPacketAlgoCmd<String, Object, Object>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = -4329950671092819917L;

			@Override
			public String apply(Class<?> index, DataPacket<Object> host,
					Object... params) {
				System.out.println("I should not be here!");
				return null;
			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
				// TODO Auto-generated method stub
				
			}

		});
		
				
		msgAlgo.setCmd(InviteToChatroom.class,  new ADataPacketAlgoCmd<String, InviteToChatroom, Object>(){

			/**
			 * 
			 */
			private static final long serialVersionUID = 6397860207466953790L;

			@Override
			public String apply(Class<?> index, DataPacket<InviteToChatroom> host, Object... params) {
				
				try {
					
					IChatroom remoteRoom = host.getData().chatroom;
					ChatroomWithAdapter room = new ChatroomWithAdapter(userName, remoteRoom.id());
					room.setChatWindowAdapter(toView.makeChatRoom(room));
					room.addMe();
					
				} catch (Exception e) {
					System.out.println("create room failed: " + e + "\n");
					e.printStackTrace();
				}

				return null;
//				IChatroom rm = host.getData().chatroom;
//				IUser remote = (IUser) params[0];
//				
//				ChatroomWithAdapter chatRoom = new ChatroomWithAdapter(me,rm.id());
//				chatRoom.setChatWindowAdapter(toView.makeChatRoom(chatRoom));
//				
//				for (IUser user: rm.getUsers()) {
//					if (!user.equals(me)) chatRoom.addUser(user);
//				}
//				
//				chatRoom.addUser(remote);
//				rooms.put(rm.id(),chatRoom);
//				return null;
			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
				
			}
		});
		
		msgAlgo.setCmd(RemoveMe.class,  new ADataPacketAlgoCmd<String, RemoveMe, Object>(){

			/**
			 * 
			 */
			private static final long serialVersionUID = -3221582487358802589L;

			@Override
			public String apply(Class<?> index, DataPacket<RemoveMe> host, Object... params) {
				
				IChatroom room = rooms.get(host.getData().chatroomID);
				room.removeUser(host.getData().me);
				return null;
				
			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
				// TODO Auto-generated method stub
				
			}

			
		});
		
		msgAlgo.setCmd(AddMe.class, new ADataPacketAlgoCmd<String, AddMe, Object>(){

			/**
			 * 
			 */
			private static final long serialVersionUID = 6397860207466953790L;

			@Override
			public String apply(Class<?> index, DataPacket<AddMe> host, Object... params) {

				return null;
			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
				
			}
			
		});
	}
	
	public void start() {
		
		rmiUtils = new RMIUtils(new IVoidLambda<String>(){

			@Override
			public void apply(String... params) {
				for (String s:params) {
					System.out.println(s);
				}
			}
			
		});
		
		rmiUtils.startRMI(IRMI_Defs.CLASS_SERVER_PORT_SERVER);
		try {
			Connect connect = new Connect(new IConnectToWorldAdapter(){

				@Override	
				public HashSet<IChatroom> getChatrooms() {
					return new HashSet<IChatroom>(rooms.values());
				}

				@Override
				public <T> ADataPacketAlgoCmd<String, ?, IUser> getCommand(
						RequestForAlgo request) {
					// TODO Auto-generated method stub
					return null;
				}

				// TODO change back id
				@Override
				public IUser getUser(IConnect stub) {
					return me;
				}

				@Override
				public <T> void receive(IUser remote, ADataPacket data) {
					data.execute(msgAlgo, remote);					
				}
			});
			
			IConnect stub =(IConnect) UnicastRemoteObject.exportObject(connect, IConnect.BOUND_PORT);

			registry = rmiUtils.getLocalRegistry();
			registry.rebind(IConnect.BOUND_NAME, stub);
			
			System.out.println("Waiting..."+"\n");
			me = new User(connect,userName, InetAddress.getLocalHost());
		}catch (Exception e) {
			System.err.println("Connect exception:"+"\n");
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
	
	public void chatWith(final String ip) {
	
		IUser friend = connectTo(ip);	
		if (null!=friend) createNewRoom(friend);
	}
	
	public IUser connectTo(final String ip) {
		
		IUser friend = null;
		try{
			Registry registry = rmiUtils.getRemoteRegistry(ip);
			System.out.println("Found registry: " + registry + "\n");
			IConnect connect = (IConnect) registry.lookup(IConnect.BOUND_NAME);
			System.out.println("Found remote Connect object: " + connect + "\n");
			friend = (IUser)(connect.getUser(connect));
		}catch (Exception e){
			System.out.println("Establish connect failed!\n Exception connecting to " + ip + ": " + e + "\n");
		}
	
		return friend;
	}
	
	public void stop()
	{
		try {
			registry.unbind(IConnect.BOUND_NAME);
			System.out.println("Chat App Model Registry: " + IConnect.BOUND_NAME
					+ " has been unbound.");
			
			rmiUtils.stopRMI();
			
			System.exit(0);
		} catch (Exception e) {
			System.err.println("Chat App Model Registry: Error unbinding "
					+ IConnect.BOUND_NAME + ":\n" + e);
			System.exit(-1);
		}
	}

	public void deleteChatroom(UUID id2) {
		rooms.remove(id2);
	}

	public void createNewRoom(IUser friend) {
		(new Thread(){
			@Override
			public void run(){
				if (null!=friend){
					try{
						ChatroomWithAdapter chatRoom = new ChatroomWithAdapter(userName);
						chatRoom.setChatWindowAdapter(toView.makeChatRoom(chatRoom));
						
						InviteToChatroom invite = new InviteToChatroom(chatRoom);
						friend.getConnect().sendReceive(chatRoom.getMe(), new DataPacket<InviteToChatroom>(InviteToChatroom.class, invite));
						
						rooms.put(chatRoom.id(), (IChatroom) chatRoom);
					} catch(Exception e){
						System.out.println("create room failed: " + e + "\n");
						e.printStackTrace();
					}
				}
			}
		}).start();
	}
		
	public void joinRoom(IChatroom rm) {
		// create a local chatroom with all the existing members in the room to join
		ChatroomWithAdapter chatRoom;
		try {
			chatRoom = new ChatroomWithAdapter(userName);
		
			chatRoom.setChatWindowAdapter(toView.makeChatRoom(chatRoom));
			
			for (IUser user: rm.getUsers()) {
				chatRoom.addUser(user);
			}
			rooms.put(rm.id(), chatRoom);
			// broadcast to all the members in the chatroom
			AddMe pac = new AddMe(me, rm.id());
			rm.send(me, new DataPacket<AddMe>(AddMe.class, pac));
		} catch (UnknownHostException | RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public HashSet<IChatroom> getFriendChatrooms(String ip) {
		IUser friend = connectTo(ip);
		
		try {
			if (null != friend)
			return friend.getConnect().getChatrooms();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return null;
	}
}
