package model.chatroom;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.UUID;
import model.user.Connect;
import model.user.IConnectToWorldAdapter;
import model.user.User;
import provided.datapacket.ADataPacket;
import provided.datapacket.ADataPacketAlgoCmd;
import provided.datapacket.DataPacket;
import provided.datapacket.DataPacketAlgo;
import common.IChatroom;
import common.ICmd2ModelAdapter;
import common.IConnect;
import common.IUser;
import common.messages.AddMe;
import common.messages.RemoveMe;
import common.messages.RequestForAlgo;

/**
 * 
 * @author bb26, xc7
 */
public class ChatroomWithAdapter implements IChatroom {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4342241825756246744L;
	
	private transient IChatRoom2View<IUser> chatWindowAdapter;
	
	private IUser me;
	
	private UUID id;
	
	private String displayName;
		
	private ArrayList<IUser> users;
		
	private DataPacketAlgo<String, Object> msgAlgo;
	
	public ChatroomWithAdapter(String name) throws UnknownHostException, RemoteException {
		
		initAlgo();
		
		Connect connect = new Connect(new IConnectToWorldAdapter(){
			
			@Override	
			public HashSet<IChatroom> getChatrooms() {
				return null;
			}

			@Override
			public <T> ADataPacketAlgoCmd<String, ?, IUser> getCommand(
					RequestForAlgo request) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public IUser getUser(IConnect stub) {
				return me;
			}

			@Override
			public <T> void receive(IUser remote, ADataPacket data) {
				data.execute(msgAlgo, remote);					
			}
		});
		
		IConnect stub =(IConnect) UnicastRemoteObject.exportObject(connect, IConnect.BOUND_PORT);
		me = new User(stub, name, InetAddress.getLocalHost());
		
		id = UUID.randomUUID();
		users = new ArrayList<IUser>();
		users.add(me);
	}
	
	public ChatroomWithAdapter(String name, UUID uuid) throws UnknownHostException, RemoteException {
		this(name);
		id = uuid;
	}
	
//	public ChatroomWithAdapter(IUser me) {
//		setMe(me);
//		this.id = UUID.randomUUID();
//		users = new HashSet<IUser>();
//	}
//	
//	public ChatroomWithAdapter(IUser me, UUID id){
//		this(me);
//		this.id = id;
//		this.setName(id.toString());
//	}
//	
//	public ChatroomWithAdapter(IChatRoom2View<IUser> adapter, IUser me) {
//		this(me);
//		setChatWindowAdapter(adapter);
//	}

	public IChatRoom2View<IUser> getChatWindowAdapter() {
		return chatWindowAdapter;
	}

	public void setChatWindowAdapter(IChatRoom2View<IUser> chatWindowAdapter) {
		this.chatWindowAdapter = chatWindowAdapter;
		refreshList();
	}

	public void quit() {
		
		RemoveMe remove = new RemoveMe(me, id);
		send(me, new DataPacket<RemoveMe>(RemoveMe.class, remove));
//		chatWindowAdapter.deleteWindow();
	}

	public IUser getMe() {
		return me;
	}

	public void setMe(IUser me) {
		this.me = me;
	}
	
	public void invite(IUser friend) {
		
	}


	@Override
	public UUID id() {
		return id;
	}

	@Override
	public HashSet<IUser> getUsers() {
		return new HashSet<IUser>(users);

	}

	@Override
	public boolean addUser(IUser user) {
		boolean added = users.add(user);
		refreshList();
		return added;
				
	}

	@Override
	public boolean removeUser(IUser user) {
		
		boolean removed = users.remove(user);
		if (users.size() == 0) {
			chatWindowAdapter.deleteWindow();
			chatWindowAdapter.deleteModel(id);
		}
		else{
			refreshList();
		}
		return removed;
	}

	@Override
	public void send(IUser me, ADataPacket message) {
		(new Thread(){
			@Override
			public void run(){
				for (IUser user: users){
					if (user != me) {
						try{
							user.getConnect().sendReceive(me, message);
						}
						catch(RemoteException e){
							e.printStackTrace();
						}
					}
				}
				
			}
		}).start();
	}

	@Override
	public String getName() {
		if (users.size() > 0){
			IUser[] lstUser = users.toArray(new IUser[users.size()]);
			displayName = "Chatroom with members: " + lstUser[0].getName() + " et, al.";
		}
		else{
			displayName = "Chatroom with members: " + me.getName() + " et, al.";
		}
		return displayName;
	}

	@Override
	public void setName(String name) {
		displayName = name;
	}

	public void sendMsg(String text) {
		send(me, new DataPacket<String>(String.class, text));
	}
	
	public void display(String data) {
		chatWindowAdapter.append(data);
	}
	
	private void refreshList() {
		chatWindowAdapter.refreshList(users);
	}
	
	public String toString() {
		return displayName;
	}
	
	private void initAlgo() {
		msgAlgo = new DataPacketAlgo<String,Object>(new ADataPacketAlgoCmd<String, Object, Object>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = -1139989943264094599L;

			@Override
			public String apply(Class<?> index, DataPacket<Object> host, Object... params) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
				// TODO Auto-generated method stub
			}

		});
		
		msgAlgo.setCmd(String.class, new ADataPacketAlgoCmd<String, String, Object>(){

			
			/**
			 * 
			 */
			private static final long serialVersionUID = 2210559989023917346L;
			
			@Override
			public String apply(Class<?> index, DataPacket<String> host, Object... params) {
				IUser remote = (IUser) params[0];
				chatWindowAdapter.append(remote.getName() + " says:");
				chatWindowAdapter.append(host.getData());
				return null;
			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
				
			}
		});
		
		msgAlgo.setCmd(AddMe.class, new ADataPacketAlgoCmd<String, AddMe, Object>(){

			/**
			 * 
			 */
			private static final long serialVersionUID = -189336880905492572L;

			@Override
			public String apply(Class<?> index, DataPacket<AddMe> host,
					Object... params) {

				addUser(host.getData().me);
				return null;
			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
	}

	public void addMe() {
		
		(new Thread(){
			@Override
			public void run(){
				for (IUser user: users) {
					try {
						if (!user.equals(me)){
							AddMe addMe = new AddMe(me, id);
							user.getConnect().sendReceive(me, new DataPacket<AddMe>(AddMe.class, addMe));
						};
					}catch (RemoteException e) {
						System.out.println("Broadcast to add user failed!\nRemote exception invite " + ": " + user.getIP() + e + "\n");
						e.printStackTrace();
					}
				}
				
			}
		}).start();
	}
}