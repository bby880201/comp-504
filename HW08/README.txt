Comp504 HW08
Authors: Xinyi Cen (xc7); Boyang Bai (bb26)

This assignment implemented a flexible, extensible chat program using RMI.
Our ChapApp uses a peer-to-peer architecture, and bases on a message-passing 
architecture. 

This program is able to:
	 1. Connect to and chat with self (single user) on local chatroom
	 2. Support two-way text chatting with
	 	a. Multiple, simultaneous single individuals in a chat room
	 	b. Multiple, simultaneous independent chat rooms.
	 3. Support the ability to handle new kinds of data packets at run-time.
	 4. Support the ability to send back return status

How to run program:
Run controller.ChatAppController.java, a GUI window should pop up.

Enter the IP address of the remote user you want to chat with into the 
"Connect to" test field. 
Click on "Chat with" button, a chat window should show up in the main GUI panel.
Click on "Get Chatrooms" will return a list of chatrooms the remote user (as 
specified by the IP address) is current in. 
You could choose a chatroom from the list of chatrooms to join. 

You could invide a remote friend to join a chatroom by entering the remote user's
IP address into the "Invite friend" test field; then click on the "Invite" button. 

You could chat with a specific memebr of a chatroom by choosing that member from the 
"Member list" and click on "Speak to".
